package com.gps.hubble;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.w3c.dom.Document;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.framework.service.ServiceInvokerManager;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.core.YCPContext;
import com.yantra.yfc.dom.YFCDocument;

public class TestBase {
	private YCPContext ctx;
	private YIFApi api;
	private ServiceInvoker serviceInvoker;
	
	
	
	public ServiceInvoker getServiceInvoker() {
		return serviceInvoker;
	}

	public void setServiceInvoker(ServiceInvoker serviceInvoker) {
		this.serviceInvoker = serviceInvoker;
	}

	@Before
	public void setup(){
		try{
			ctx = 	createContext();
			ctx.setRollbackOnly(true);
			api = YIFClientFactory.getInstance().getApi();
			serviceInvoker = ServiceInvokerManager.getInstance().getDefaultServiceInvoker();
			serviceInvoker.setNewYFSEnvironment(ctx);
			//YCPCacheSetup.disableCacheSetup(true);
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	private YCPContext createContext() throws Exception{
		try{
			YCPContext ctx = new YCPContext("admin","TestUploadExcels");
			ctx.setSystemCall(true);
			ctx.setInternalApiCall(true);
			return ctx;
		}catch(Exception e){
			throw e;
		}
	}
	
	@After
	public void afterCompletion(){
		if(ctx != null){
			try{
				ctx.rollback();
			}catch(SQLException sqlException){
				Assert.fail(sqlException.getMessage());
			}
			ctx.close();
		}
	}
	
	public YFCDocument invokeYantraService(String serviceName, YFCDocument input){
		try{
			Document output = api.executeFlow(ctx, serviceName, input.getDocument());
			return YFCDocument.getDocumentFor(output);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public YFCDocument invokeYantraApi(String apiName, YFCDocument input){
		try{
			Document output = api.invoke(ctx, apiName, input.getDocument());
			return YFCDocument.getDocumentFor(output);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public YFCDocument invokeYantraApi(String apiName, YFCDocument input, YFCDocument template){
		ctx.setApiTemplate(apiName, template.getDocument());
		return invokeYantraApi(apiName, input);
	}
	
	
}
