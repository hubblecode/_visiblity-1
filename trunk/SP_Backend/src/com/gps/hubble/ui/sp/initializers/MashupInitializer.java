package com.gps.hubble.ui.sp.initializers;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.mashup.MashupManager;

/**
 * Servlet implementation class MashupInitializer
 */
@WebServlet(urlPatterns = {"/init/mashupinitializer"}, loadOnStartup = 50)
public final class MashupInitializer extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MashupInitializer() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("Mashup Init called");
		MashupManager.registerMashupFile("/resources/sp_mashups.xml");
		MashupManager.registerMashupFile("/resources/vendor_mashups.xml");
		//MashupManager.registerMashupFile("/resources/alerts_mashups.xml");
		MashupManager.loadMashups();
	}

}
