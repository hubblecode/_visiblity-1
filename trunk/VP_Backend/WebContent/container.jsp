<%@page import="com.gps.hubble.ui.utils.Utils"%>
<%@page import="com.gps.hubble.ui.AppInfo"%>
<%@page import="com.gps.hubble.ui.utils.UIContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Vendor portal</title>
    <style>[v-cloak] {
  display: none;
}
    </style>
</head>
<body>
  <%
  	UIContext context = UIContext.getUIContext(request, response);
  	String csrfToken = context.getCsrfToken();
  	String loginid = context.getLoginid();
  	boolean isUsingWebpack = Utils.getBooleanValue(request.getHeader("bridgewebpack"));
  %>
  <script type="text/javascript">
	if (!window.AppInfo) {
	  	window.AppInfo = {}
	}
  	window.AppInfo.contextroot = "<%=request.getContextPath()%>";
  	window.AppInfo.bridgecsrftoken = "<%=csrfToken%>";
  	window.AppInfo.loginpage = "<%=AppInfo.getLoginpage()%>";
  	window.AppInfo.loginid = "<%=loginid%>";
  </script>
 	<div id="app"  v-cloak>
		<div>
	   <header class=" top-bar">
    <div class="navbar-fixed-top">
      <div class="row top-bar-border no-gutters nav-down">
          <div class="container-fluid">
              <div class="row">
        <div class="col-sm-5 col-xs-5">
            <div class="navbar-brand p-l-0"><a href="index.html"><img src="images/ibm-logo.png" class="pull-left" alt=""></a></div>
        </div>
        <div class="col-md-7 col-xs-7 ">
          <div class="clearfix">

            <div class="pull-right">
              <ul class="nav navbar-nav navbar-nav-dropdown clearfix">
                   <li> 
                      <div id="custom-search-input">
              <div class="input-group">
      <input type="text" class="form-control" placeholder="Search Orders">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
      </span>
    </div><!-- /input-group -->
                      </div></li>
                  <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  type="button"><i class="glyphicon glyphicon-user"></i></a>
          <ul class="dropdown-menu">
               <li><a href="../vendor_portal/index.html"><b>Vendor UI</b></a></li>
              <li><a href="../sterling_portal/index.html">Sterling UI</a></li>
               <li><a href="../customer_portal/index.html">Telstra UI</a></li>
                <div class="divider"></div>
            <li><a href="#">My Account</a></li>
                <li><a href="#">Sign Out</a></li>
            
              
          </ul>
        </li>
              </ul>
    
            </div>
          </div>
        </div>
              </div>
          </div>
      </div>
        <nav class="navbar navbar-default cusBottom">
            <div class="row-border-bottom no-gutters">
  <div class="container-fluid">
    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand" href="index.html"><h4>Vendor Portal</h4></div>
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
        <li ><a v-link="{ path:'/orderlist'}" > Orders</a></li>
            <li ><a href="shipments.html">Shipments</a></li>
               <li ><a href="administration.html">Administration</a></li>
    </ul>
            </div>
  </div>
            </div>
</nav>
             </div>
          
    </header>
		</div>
	  <!-- route outlet -->
	   <router-view></router-view>
	</div>
<% if(isUsingWebpack) { %>
	  <script src="/dist/vendor.js"></script>
	  <script src="/dist/app.js"></script>
<%} else {%>
	  <script src="./dist/vendor.js"></script>
	  <script src="./dist/app.js"></script>
<% } %>

	      <footer class="footer LoginFooter">
        <div class="container">
            <div class="text-center">© Copyright IBM Corporation 2016</div>
        </div>
    </footer>
</body>
</html>