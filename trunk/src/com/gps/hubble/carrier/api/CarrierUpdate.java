package com.gps.hubble.carrier.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * Custom class to update Carrier Information for an Order
 * 
 * 
 * @author Murali
 * @version 1.0 
 */
public class CarrierUpdate extends AbstractCustomApi {

  private static YFCLogCategory logger = YFCLogCategory.instance(CarrierUpdate.class);
  /**
   * Base Method execution starts here
   * 
   * 
   */
  @Override
  public YFCDocument invoke(YFCDocument inXml) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    try{
      YFCDocument getShipListOutXml = getShipmentList(inXml.getDocumentElement());
      validateShipment(getShipListOutXml);
      YFCElement shipmentEle = getShipListOutXml.getDocumentElement()
        .getChildElement(TelstraConstants.SHIPMENT);
      updateCarrierStatus(inXml,shipmentEle);
    }
    catch (Exception exp) {
      LoggerUtil.verboseLog("Carrier Update Failure ", logger, exp);
      throw exp;
    }
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    return inXml;
  }
  
  
  /**
   * Method to invoke getShipmentList to get input 
   * shipment information
   * 
   * @param inEle
   * @return
   */
  private YFCDocument getShipmentList(YFCElement inEle) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getShipmentStatus", inEle);
    YFCDocument getShipListXml = YFCDocument.getDocumentFor("<Shipment />");
    YFCElement shipListEle = getShipListXml.getDocumentElement();
    shipListEle.setAttribute(TelstraConstants.ORDER_NO, inEle.getAttribute(TelstraConstants.ORDER_NO));
    shipListEle.setAttribute(TelstraConstants.FROM_STATUS, getProperty(TelstraConstants.FROM_STATUS,true));
    shipListEle.setAttribute(TelstraConstants.TO_STATUS, getProperty(TelstraConstants.TO_STATUS,true));
    shipListEle.setAttribute(TelstraConstants.SHIPMENT_NO, 
        inEle.getAttribute(TelstraConstants.SHIPMENT_NO));
    shipListEle.setAttribute(TelstraConstants.TRACKING_NO, inEle.getAttribute(TelstraConstants.TRACKING_NO));
    shipListEle.createChild(TelstraConstants.SHIPMENT_LINES).createChild(TelstraConstants.SHIPMENT_LINE)
    .createChild(TelstraConstants.ORDER).setAttribute(TelstraConstants.ORDER_NAME,
        inEle.getAttribute(TelstraConstants.ORDER_NAME));
    shipListEle.setAttribute("StatusQryType", "BETWEEN");
    YFCDocument tempXml = YFCDocument.getDocumentFor("<Shipments TotalNumberOfRecords = ''>"
        + "<Shipment DocumentType='' ShipmentNo='' ShipmentKey='' Status='' /></Shipments>");
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getShipmentStatus", "Get Shipment List Success");
     return invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListXml, tempXml);
  }

  
  /**
   * Method to change Shipment status based on the Carrier Information
   * 
   * If Carrier Status is Consignment Picked Up then Shipment status is 
   * In transit.
   * If Carrier Status is Unsuccessful then Shipment status is Futile Delivery
   * @param shipmentKey
   * @param transactionId
   * @param baseDropStatus
   */
  private void changeShipmentStatus(String shipmentKey,String transactionId, String baseDropStatus) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeShipmentStatus", "ShipmentKey :"+shipmentKey+
        "BaseDropStatus :"+baseDropStatus);
    YFCDocument changeShipStatXml = YFCDocument.getDocumentFor("<Shipment />");
    changeShipStatXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_KEY, shipmentKey);
    changeShipStatXml.getDocumentElement().setAttribute("TransactionId", transactionId);
    changeShipStatXml.getDocumentElement().setAttribute("BaseDropStatus", baseDropStatus);
    invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT_STATUS_API, changeShipStatXml);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeShipmentStatus", "Change Shipment Status Success");
  }
  
  /**
   * 
   * Method to confirm / un confirm / deliver Status based
   * on carrier status and shipment status
   * @param shipmentStatus
   * @param shipmentKey
   * @param api
   */
  private void changeShipmentTranStatus(String shipmentKey,String api) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateConfirmShipment", "ShipmentKey :"+shipmentKey+
        "API :"+api);
      YFCDocument shipConfirmXml = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+shipmentKey+"' />");
      invokeYantraApi(api, shipConfirmXml);
      LoggerUtil.endComponentLog(logger, this.getClass().getName(), "validateConfirmShipment", "API Success");
  }
  
  
  /**
   * Method to add the carrier update information 
   * in HangOff Table for Shipment.
   * 
   * @param inXml
   * @param shipmentKey
   */
  private void modifyShipment(YFCDocument inXml,String shipmentKey) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "modifyShipment", inXml);
    YFCElement extnEle = inXml.getDocumentElement().getChildElement(TelstraConstants.EXTN);
    YFCDocument modifyShipXml = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+shipmentKey+"' />");
    YFCElement modifyShipEle = modifyShipXml.getDocumentElement();
    modifyShipEle.importNode(extnEle);
    invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT, modifyShipXml);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "modifyShipment", "Shipment Modification Success");
  }
  
  
  
  /**
   * This method validates the different status and
   * changes the shipment status accordingly. Also
   * updates the HangOff table.
   *
   * @param inXml
   */
  private void updateCarrierStatus(YFCDocument inXml,YFCElement shipmentEle ) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "updateCarrierStatus", shipmentEle);
    if(!XmlUtils.isVoid(shipmentEle)) {
      String inTransit = getProperty("GPS_IN_TRANSIT",true);
      String futileDle = getProperty("GPS_FUTILE_DEL",true);
	  String carrierInTransit =  getProperty("INTRANSIT_STATUS",true);
	  String carrierDelivered = getProperty("DELIVERED_STATUS",true);
	  String carrierFailed = getProperty("FAILED_STATUS",true);
      Double shipmentStatus = shipmentEle.getDoubleAttribute(TelstraConstants.STATUS);
	  String documentType = shipmentEle.getAttribute(TelstraConstants.DOCUMENT_TYPE);
      String carrierStatus = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatus");
      String shipmentKey = shipmentEle.getAttribute(TelstraConstants.SHIPMENT_KEY);
      if(shipmentStatus < 1400) {
        changeShipmentTranStatus(shipmentKey,
            TelstraConstants.CONFIRM_SHIPMENT_API);
			 shipmentStatus = (double) 1400;
      }
      if(carrierInTransit.equals(carrierStatus)) {
        changeShipmentStatus(shipmentKey,"GPS_IN_TRANSIT."+documentType+".ex",inTransit);
      } else if(carrierFailed.equals(carrierStatus)) {
        changeShipmentTranStatus(shipmentKey,
            TelstraConstants.UN_CONFIRM_SHIPMENT);
        changeShipmentStatus(shipmentKey, "GPS_FUTILE_DEL."+documentType+".ex", futileDle);
      } else if(carrierDelivered.equals(carrierStatus)) {
        if(shipmentStatus < 1400.10000) {
          changeShipmentStatus(shipmentKey,"GPS_IN_TRANSIT."+documentType+".ex",inTransit);
        }
        changeShipmentTranStatus(shipmentKey,
            TelstraConstants.DELIVER_SHIPMENT_API);
      }
      modifyShipment(inXml,shipmentKey);
    }
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "updateCarrierStatus", "Carrier Update Success");
  }
  
  
  /**
   * Method validates shipment list for multi Shipment 
   * Throws exception if more than one Shipment on In transit 
   * @param shipListXml
   */
  
  private void validateShipment(YFCDocument shipListXml) {
    String shipmentCount = shipListXml.getDocumentElement().getAttribute("TotalNumberOfRecords");
    if(!"1".equals(shipmentCount)) {
      String strErrorCode = getProperty("CarrierFailureCode", true); 
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());     
    }
  }
}