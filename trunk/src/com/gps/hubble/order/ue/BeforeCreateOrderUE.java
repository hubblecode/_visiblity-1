/***********************************************************************************************
 * File	Name		: BeforeCreateOrderUE.java
 *
 * Description		: This class is called from YFSBeforeCreateOrder UE as a service. 
 * 						It do Cost Center, Zip Code and Item Round Validation. 
 * 						It also updates BillToId if BillToKey is present.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 04,2016	  	Tushar Dhaka 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.order.ue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCDateUtils;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfc.util.YFCLocale;
import com.yantra.yfs.japi.YFSException;


/**
 * Custom call to do order validations
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 * Extends AbstractCustomApi class
 */
public class BeforeCreateOrderUE extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(BeforeCreateOrderUE.class);
	
	/*
	 * For validation and order date calcuation
	 * (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		
		//get Common Codes associated with Order Validation
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, YFCDocument.getDocumentFor("<CommonCode CodeType=\"ORDER_VALIDATION\"/>"));
		
		//validate Cost Center
		costCenterValidation(inXml, docGetCommonCodeListOutput);
		
		//validate zip code
		zipCodeValidation(inXml, docGetCommonCodeListOutput);
		
		//validate Item Round
		itemRoundValidation(inXml, docGetCommonCodeListOutput);
		
		//update BillToId if BillToKey is passed in input
		updateBillToIdOrKey(inXml);
		
		//Order date calculations
		if(requireOrderDateCalculations(inXml)){
			docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, YFCDocument.getDocumentFor("<CommonCode/>"));
			if(isIntegralOrder(inXml)){
				calculateDatesForIntegralOrder(inXml,docGetCommonCodeListOutput);
			}else if(isVectorOrder(inXml)){
				calculateDatesForVectorOrder(inXml, docGetCommonCodeListOutput);
			}
		}
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		return inXml;
	}
	
	private void updateBillToIdOrKey(YFCDocument docOrderXml) {
		String sBillToID = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.BILL_TO_ID,"");
		if(YFCObject.isNull(sBillToID)) {
			String sBillToKey = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.BILL_TO_KEY,"");
			if(!YFCObject.isNull(sBillToKey)) {
				//BillToId is blank and BillToKey exist, will call getCustomerList and get CustomerID and update in input xml
				YFCDocument docInput = YFCDocument.getDocumentFor("<Customer CustomerKey ='"+sBillToKey+"'/>");
				YFCDocument docTemplate = YFCDocument.getDocumentFor("<CustomerList><Customer CustomerKey=\"\" CustomerID=\"\"/></CustomerList>");
				YFCDocument docOutput = invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST, docInput, docTemplate);
				if(!YFCObject.isVoid(docOutput.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER))) {
					String sCustomerID = docOutput.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER).getAttribute(TelstraConstants.CUSTOMER_ID,"");
					if(!YFCObject.isNull(sCustomerID)) {
						docOrderXml.getDocumentElement().setAttribute(TelstraConstants.BILL_TO_ID, sCustomerID);
						docOrderXml.getDocumentElement().removeAttribute(TelstraConstants.BILL_TO_KEY);
					}
				}
			}
		}
		
	}
	
	private void itemRoundValidation(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput) {
		//Check Item Round Validation settings from common code
		if(isValidationOn(docGetCommonCodeListOutput,"ITEM_ROUND")) {
			String sOrganizationCode = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTERPRISE_CODE);
			//iterate through all the order lines
			for(YFCElement eleOrderLine : docOrderXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
				String sShipNode = eleOrderLine.getAttribute(TelstraConstants.SHIP_NODE,"");
				double dOrderLineQty = eleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY,0.0);
				double dNewOrderLineQty = dOrderLineQty;
				
				YFCElement eleItem = eleOrderLine.getChildElement(TelstraConstants.ITEM);
				String sItemID = eleItem.getAttribute(TelstraConstants.ITEM_ID);
				String sUnitOfMeasure = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE,"");
				
				//call getItemNodeDefnList with input <ItemNodeDefn UnitOfMeasure="" OrganizationCode="" Node="" ItemID=""/>
				YFCDocument docInput = YFCDocument.getDocumentFor("<ItemNodeDefn UnitOfMeasure='"+sUnitOfMeasure+"' OrganizationCode='"+sOrganizationCode+"' Node='"+sShipNode+"' ItemID='"+sItemID+"'/>");
				YFCDocument docTemplate = YFCDocument.getDocumentFor("<ItemNodeDefnList><ItemNodeDefn><Extn/></ItemNodeDefn></ItemNodeDefnList>");
				YFCDocument docOutput = invokeYantraApi(TelstraConstants.API_GET_ITEM_NODE_DEFN_LIST, docInput, docTemplate);
				
				YFCElement eleItemNodeDefn = docOutput.getElementsByTagName(TelstraConstants.ITEM_NODE_DEFN).item(0);
				if(!YFCObject.isVoid(eleItemNodeDefn)) {
					YFCElement eleExtn = eleItemNodeDefn.getElementsByTagName(TelstraConstants.EXTN).item(0);
					if(!YFCObject.isVoid(eleExtn)) {
						double dRunQuantity = eleExtn.getDoubleAttribute(TelstraConstants.RUN_QUANTITY,0.0);
						if(dRunQuantity>0) {
							if(dOrderLineQty<=dRunQuantity) {
								dNewOrderLineQty = dRunQuantity;
							}
							else {
								dNewOrderLineQty = (Math.ceil(dOrderLineQty/dRunQuantity))*dRunQuantity;
							}
						}
					}
				}
				eleOrderLine.setDoubleAttribute(TelstraConstants.ORDERED_QTY, dNewOrderLineQty);
			}
		}
	}
	
	private void zipCodeValidation(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput) {
		//Check Zip Code Validation settings from common code
		if(isValidationOn(docGetCommonCodeListOutput,"ZIP_CODE")) {
			//get BillToID or BillToKey from Order Xml
			String sBillToID = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.BILL_TO_ID,"");
			String sBillToKey = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.BILL_TO_KEY,"");

			if(YFCObject.isNull(sBillToID) && YFCObject.isNull(sBillToKey)){
				// Fetch city, state, country and zip code from the bill to address 
				YFCElement eleBillTo = docOrderXml.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);
				String sCity = eleBillTo.getAttribute(TelstraConstants.CITY,"NULL"), sState = eleBillTo.getAttribute(TelstraConstants.STATE,"NULL"), 
						sCountry = eleBillTo.getAttribute(TelstraConstants.COUNTRY,"NULL"), sZipCode = eleBillTo.getAttribute(TelstraConstants.ZIP_CODE,"NULL");
				
				// call GpsGetZipCodeLocationList to get ZipCode
				YFCDocument docInput = YFCDocument.getDocumentFor("<ZipCodeLocation City='"+sCity+"' State='"+sState+"' Country='"+sCountry+"'/>");
				String sZipCodeLocationListService = getProperty("Service.GetZipCodeLocationList", true);
				YFCDocument docOutput = invokeYantraService(sZipCodeLocationListService, docInput);
				if(!YFCObject.isVoid(docOutput.getDocumentElement().getChildElement(TelstraConstants.ZIP_CODE_LOCATION))) {
					String sTableZipCode = docOutput.getDocumentElement().getChildElement(TelstraConstants.ZIP_CODE_LOCATION).getAttribute(TelstraConstants.ZIP_CODE,"");
					if(!sTableZipCode.equals(sZipCode)) {
						//update order xml with zip code from DB
						eleBillTo.setAttribute(TelstraConstants.ZIP_CODE, sTableZipCode);
					}
				}	
				else {
					// call GpsGetZipCodeLocationList to get City, country and state
					YFCDocument docGpsGetZipCodeLocationListOutput = invokeYantraService(sZipCodeLocationListService, YFCDocument.getDocumentFor("<ZipCodeLocation ZipCode='"+sZipCode+"'/>"));
					YFCElement eleZipCodeList = docGpsGetZipCodeLocationListOutput.getDocumentElement();
					if(eleZipCodeList.getElementsByTagName("ZipCodeLocation").getLength()>1) {
						//throw exception
						LoggerUtil.verboseLog("Cannot Determine Unique zipcode ", logger, " throwing exception");
						String strErrorCode = getProperty("ZipCodeDeterminationFailureCode", true);
						YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
						throw new YFSException(erroDoc.toString());	
					}
					if(!YFCObject.isVoid(eleZipCodeList.getChildElement("ZipCodeLocation"))) {
						String sTableCity = eleZipCodeList.getChildElement(TelstraConstants.ZIP_CODE_LOCATION).getAttribute(TelstraConstants.CITY,"");
						String sTableState = eleZipCodeList.getChildElement(TelstraConstants.ZIP_CODE_LOCATION).getAttribute(TelstraConstants.STATE,"");
						String sTableCountry = eleZipCodeList.getChildElement(TelstraConstants.ZIP_CODE_LOCATION).getAttribute(TelstraConstants.COUNTRY,"");
						
						//update order xml with data from DB
						eleBillTo.setAttribute(TelstraConstants.CITY, sTableCity);
						eleBillTo.setAttribute(TelstraConstants.STATE, sTableState);
						eleBillTo.setAttribute(TelstraConstants.COUNTRY, sTableCountry);
					}
					else {
						//throw exception
						LoggerUtil.verboseLog("Zip Code Validation Failure ", logger, " throwing exception");
						String strErrorCode = getProperty("ZipCodeValidationFailureCode", true);
						YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
						throw new YFSException(erroDoc.toString());	
					}
				}
			}
		}
	}
	
	private void costCenterValidation(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput) {
		//Check Cost Centre Validation settings
		if(isValidationOn(docGetCommonCodeListOutput, "COST_CENTER")) {
			//get Division code from order xml
			String sDivision = docOrderXml.getDocumentElement().getAttribute("Division","");
			if(YFCObject.isNull(sDivision)){
				//throw exception
				LoggerUtil.verboseLog("Cost Center Validation Failure ", logger, " throwing exception");
				String strErrorCode = getProperty("CostCenterValidationFailureCode", true);
				YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
				throw new YFSException(erroDoc.toString());	
			}
			else {
				//call GpsGetCostCenterList with input <CostCenter OrgCode="Division" ActiveFlag="Y"/>
				String sCostCenterListService = getProperty("Service.GetCostCenterList", true);
				YFCDocument docOutput = invokeYantraService(sCostCenterListService, YFCDocument.getDocumentFor("<CostCenter OrgCode='"+sDivision+"' ActiveFlag='Y'/>"));
				if(YFCObject.isVoid(docOutput.getDocumentElement().getChildElement(TelstraConstants.COST_CENTER))) {
					//throw exception
					LoggerUtil.verboseLog("Cost Center Validation Failure ", logger, " throwing exception");
					String strErrorCode = getProperty("CostCenterValidationFailureCode", true);
					YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
					throw new YFSException(erroDoc.toString());	
					
				}
			}			
		}		
	}
	
	private boolean isValidationOn(YFCDocument docGetCommonCodeListOutput, String sValidationType) {
		boolean bValidation = false;
		for(YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			if(eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE).equals(sValidationType) && eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION).equals(TelstraConstants.YES)) {
				bValidation = true;
				break;
			}
		}		
		return bValidation;
	}	
	
	private void calculateDatesForIntegralOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){		
		Priority priority = getOrderPriority(docOrderXml);
		if(priority == Priority.URGENT){
			handleForUrgentOrder(docOrderXml, docGetCommonCodeListOutput);
		}else if(priority == Priority.STANDARD_HOME_DELIVERY){
			handleForStandardHomeDeliveryOrder(docOrderXml,docGetCommonCodeListOutput);
		}else{
			handleForStandardOrder(docOrderXml,docGetCommonCodeListOutput);
		}
	}
	
	private void calculateDatesForVectorOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		Priority priority = getOrderPriority(docOrderXml);
		if(priority == Priority.MEDICAL){
			handleForVectorMedicalOrder(docOrderXml, docGetCommonCodeListOutput);
		}else if(priority == Priority.URGENT){
			handleForUrgentVectorOrder(docOrderXml, docGetCommonCodeListOutput);
		}else{
			handleForLowPriorityVectorOrder(docOrderXml, docGetCommonCodeListOutput);
		}
	}
	

	private void handleForVectorMedicalOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		if(shipDate == null){
			shipDate = new YDate(false);
			orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}
		YDate deliveryDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(deliveryDate != null){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		String materialClass = getMaterialClassFromDepartmentCode(docOrderXml, docGetCommonCodeListOutput);
		if(YFCObject.isVoid(materialClass)){
			return;
		}
		YFCElement transportMatrix = getTransportMatrix(materialClass, TelstraConstants.ORDER_PRIORITY_MEDICAL, docOrderXml,docGetCommonCodeListOutput);
		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		deliveryDate = calculateDeliveryDate(calendarDoc,orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),transitTime);
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	private void handleForLowPriorityVectorOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		boolean isShipDatePassed = true;
		if(shipDate == null){
			isShipDatePassed = false;
			shipDate = new YDate(false);
		}
		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		if(!isShipDatePassed){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		YDate deliveryDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(deliveryDate != null){
			return;
		}
		calculateDeliveryDateForLowPriorityVectorOrder(docOrderXml, docGetCommonCodeListOutput, docShipNodeOrgDetails, calendarDoc);
	}
	
	private void calculateDeliveryDateForLowPriorityVectorOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput, YFCDocument docShipNodeOrgDetails,YFCDocument calendarDoc){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		YDate shipDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		String priority = orderElem.getAttribute(TelstraConstants.PRIORITY);
		String materialClass = getMaterialClassFromDepartmentCode(docOrderXml, docGetCommonCodeListOutput);
		if(YFCObject.isVoid(priority) || YFCObject.isVoid(materialClass)){
			return;
		}
		YFCElement transportMatrix = getTransportMatrix(materialClass, priority, docOrderXml,docGetCommonCodeListOutput);
		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		int totalTransitTime = transitTime;
		boolean isHazmat = isHazmatOrder(orderElem);
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(calendarDoc,orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime);
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	
	private void handleForUrgentVectorOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = docOrderXml.getDocumentElement().getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		boolean isShipDatePassed = true;
		if(shipDate == null){
			isShipDatePassed = false;
			shipDate = new YDate(false);
			orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}
		
		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		boolean isHazmat = isHazmatOrder(orderElem);
		if(!isShipDatePassed && !isBeforeCutOff(shipNode, docShipNodeOrgDetails, docGetCommonCodeListOutput)){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);
			orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}
		YDate reqDeliveryDate = docOrderXml.getDocumentElement().getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(reqDeliveryDate != null){
			return;
		}
		String materialClass = getMaterialClassFromDepartmentCode(docOrderXml, docGetCommonCodeListOutput);
		YFCElement transportMatrix = getTransportMatrix(materialClass, "Urgent", docOrderXml,docGetCommonCodeListOutput);
		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		int totalTransitTime = transitTime;
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(calendarDoc,orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime);
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	
	private void handleForUrgentOrder(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = new YDate(false);
		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		boolean isHazmat = isHazmatOrder(orderElem);
		if(!isBeforeCutOff(shipNode, docShipNodeOrgDetails, docGetCommonCodeListOutput)){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Urgent", docOrderXml,docGetCommonCodeListOutput);
		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		int totalTransitTime = transitTime;
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(calendarDoc,orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime);
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	
	private void handleForStandardOrder(YFCDocument docOrderXml,YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		YDate shipDate = new YDate(false);
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		boolean isHazmat = isHazmatOrder(orderElem);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		YFCElement dacSchedule = getDACSchedule(orderElem);
		YFCElement milkRunSchedule = null;
		YDate expShipDate;
		YDate dateToWham = OrderDateHelper.getOrderDate(orderElem, TelstraConstants.DATE_TO_WHAM);
		YFCDocument weekNumbers = getWeekNumbers();
		if(dacSchedule != null){
			String routeId = dacSchedule.getAttribute(TelstraConstants.ROUTE_ID);
			if(!YFCObject.isVoid(routeId)){
				milkRunSchedule = getMilkRunSchedule(routeId,dateToWham);
			}
			String schedule = dacSchedule.getAttribute(TelstraConstants.SCHEDULE);
			expShipDate = getNextBusinessDayBasedOnSchedule(dateToWham, schedule, weekNumbers, exceptionDates, allDayShift);
			
		}else{
			expShipDate = OrderDateHelper.getNextBusinessDay(dateToWham, allDayShift, exceptionDates);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, expShipDate);
		YDate deliveryDate;
		if(milkRunSchedule != null){
			deliveryDate = getMilkRunScheduledDate(expShipDate,milkRunSchedule, exceptionDates, allDayShift);
		}else{
			YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Standard", docOrderXml,docGetCommonCodeListOutput);
			if(transportMatrix == null){
				return;
			}
			int transitTime = OrderDateHelper.getTransitTime(expShipDate,transportMatrix);
			int totalTransitTime = transitTime;
			if(isHazmat){
				totalTransitTime = transitTime + 1;
			}
			deliveryDate = calculateDeliveryDate(calendarDoc,expShipDate,totalTransitTime);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	private void handleForStandardHomeDeliveryOrder(YFCDocument docOrderXml,YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		YDate shipDate = new YDate(false);
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		boolean isHazmat = isHazmatOrder(orderElem);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		YFCElement dacSchedule = getDACSchedule(orderElem);
		YFCElement milkRunSchedule = null;
		YDate expShipDate;
		YDate dateToWham = OrderDateHelper.getOrderDate(orderElem, TelstraConstants.DATE_TO_WHAM);
		YFCDocument weekNumbers = getWeekNumbers();
		if(dacSchedule != null){
			String routeId = dacSchedule.getAttribute(TelstraConstants.ROUTE_ID);
			if(!YFCObject.isVoid(routeId)){
				milkRunSchedule = getMilkRunSchedule(routeId,dateToWham);
			}
			String schedule = dacSchedule.getAttribute(TelstraConstants.SCHEDULE);
			expShipDate = getNextBusinessDayBasedOnSchedule(dateToWham, schedule, weekNumbers, exceptionDates, allDayShift);
			
		}else{
			expShipDate = OrderDateHelper.getNextBusinessDay(dateToWham, allDayShift, exceptionDates);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, expShipDate);
		YDate deliveryDate;
		if(milkRunSchedule != null){
			deliveryDate = getMilkRunScheduledDate(expShipDate,milkRunSchedule, exceptionDates, allDayShift);
		}else{
			YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Home Delivery", docOrderXml,docGetCommonCodeListOutput);
			if(transportMatrix == null){
				return;
			}
			int transitTime = OrderDateHelper.getTransitTime(expShipDate,transportMatrix);
			int totalTransitTime = transitTime;
			if(isHazmat){
				totalTransitTime = transitTime + 1;
			}
			deliveryDate = calculateDeliveryDate(calendarDoc,expShipDate,totalTransitTime);
		}
		orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
	}
	
	
	private YFCElement getTransportMatrix(String materialClass, String priority, YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		YFCElement orderElem = docOrderXml.getDocumentElement();
		YFCDocument materialToTransportMapInput =  YFCDocument.getDocumentFor("<MaterialToTransportMap MaterialClass='" + materialClass + "' Priority='"+ priority +"' />");
		YFCDocument materialToTransportMapOutput = invokeYantraService(TelstraConstants.API_GET_MATERIAL_TO_TRANSPORT_MAP, materialToTransportMapInput);
		if(materialToTransportMapOutput == null){
			return null;
		}
		String matrixName = materialToTransportMapOutput.getDocumentElement().getAttribute(TelstraConstants.MATRIX_NAME);
		if(YFCObject.isVoid(matrixName)){
			return null;
		}
		YFCElement personInfoShipToElem = orderElem.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		if(personInfoShipToElem == null){
			return null;
		}
		String toPostCode = personInfoShipToElem.getAttribute(TelstraConstants.ZIP_CODE);
		if(YFCObject.isVoid(toPostCode)){
			return null;
		}
		String suburb = personInfoShipToElem.getAttribute(TelstraConstants.CITY);
		String shipNode = getShipNode(orderElem);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		YFCDocument transportMatrixListInput = YFCDocument.getDocumentFor("<TransportMatrix MatrixName='"+matrixName+"' "
				+ "FromState='"+shipNodeState+"' ToPostcode ='"+toPostCode+"' ToSuburb ='"+suburb+"'/>");
		YFCDocument transportMatrixListOutput = invokeYantraService(TelstraConstants.API_GET_TRANSPORT_MATRIX_LIST, transportMatrixListInput);
		YDate reqShipDate = docOrderXml.getDocumentElement().getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		if(reqShipDate == null){
			reqShipDate = new YDate(true);
		}
		return filterTransportMatrixListOutput(transportMatrixListOutput,reqShipDate);
	}
	
	
	private YDate calculateDeliveryDate(YFCDocument calendarDoc, YDate shipDate, int transitTime){
		if(transitTime == 0){
			return shipDate;
		}
		LinkedList<YDate> transitDates = new LinkedList<>();
		//First get list of transit dates
		for(int i=1; i<=transitTime;i++){
			YDate transitDate = new YDate(shipDate.getString(TelstraConstants.DATE_TIME_FORMAT), TelstraConstants.DATE_TIME_FORMAT, false);
			YFCDateUtils.addHours(transitDate, 24*i);
			transitDates.add(transitDate);
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		//Now remove and exception days
		OrderDateHelper.removeExceptionAndWeekends(transitDates, allDayShift,exceptionDates);
		return transitDates.getLast();
	}
	
	
	
	private YDate getMilkRunScheduledDate(YDate date, YFCElement milkRun, List<String> exceptionDates, YFCElement allDayShift){
		String frequency = milkRun.getAttribute(TelstraConstants.FREQUENCY);
		YDate effectiveFrom = milkRun.getYDateAttribute("EffectiveFrom");
		YFCDocument weekNumbers = null;
		String schedule;
		YDate shipDateNextDate = new YDate(date.getString(TelstraConstants.TELSTRA_DATE_FORMAT),TelstraConstants.TELSTRA_DATE_FORMAT,true);
		YFCDateUtils.addHours(shipDateNextDate, 24);
		if(frequency.equalsIgnoreCase(TelstraConstants.FORTNIGHTLY)){
			schedule = "2" + milkRun.getAttribute(TelstraConstants.WEEKDAY).toUpperCase();
			weekNumbers = OrderDateHelper.createWeekNumberDocForFortnightlyFrequency(effectiveFrom,shipDateNextDate);
		}else{
			schedule = "0" + milkRun.getAttribute(TelstraConstants.WEEKDAY).toUpperCase();
		}
		return getNextBusinessDayBasedOnSchedule(shipDateNextDate, schedule, weekNumbers, exceptionDates, allDayShift);
	}
	
	private YFCElement filterTransportMatrixListOutput(YFCDocument transportMatrixListOutput, YDate shipDate){
		if(transportMatrixListOutput == null || transportMatrixListOutput.getDocumentElement() == null){
			return null;
		}
		for(Iterator<YFCElement> itr = transportMatrixListOutput.getDocumentElement().getChildren().iterator();itr.hasNext();){
			YFCElement elem = itr.next();
			YDate fromDate = elem.getYDateAttribute("FromDate");
			YDate toDate = elem.getYDateAttribute("ToDate");
			if(fromDate == null || toDate == null){
				continue;
			}
			if(shipDate.after(fromDate) && shipDate.before(toDate)){
				return elem;
			}
		}
		return null;
	}
	
	private boolean isBeforeCutOff(String shipNode, YFCDocument docShipNodeOrgDetails, YFCDocument docGetCommonCodeListOutput){
		try{
			String shipNodeLocaleCode = docShipNodeOrgDetails.getDocumentElement().getAttribute("LocaleCode");
			YFCLocale yfcLocale = YFCLocale.getYFCLocale(shipNodeLocaleCode);
			String shipNodeTimeZoneString = yfcLocale.getTimezone();
			java.util.TimeZone shipNodeTimeZone = java.util.TimeZone.getTimeZone(shipNodeTimeZoneString);
			
			//Currently calculating based on current time
			YDate now = new YDate(false);
			SimpleDateFormat hostFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
			SimpleDateFormat nodeFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
			
			nodeFormatter.setTimeZone(shipNodeTimeZone);
			String convertedDateString = nodeFormatter.format(now);
			Date convrtedJavaDate = hostFormatter.parse(convertedDateString); 
			YDate convertedDate = new YDate(convrtedJavaDate.getTime(), false);	
			YDate cutoffDate = getCutOffTimeForShipNode(shipNode, docGetCommonCodeListOutput);
			return cutoffDate == null ? false : convertedDate.before(cutoffDate);
		}catch(ParseException pe){
			throw new YFCException(pe);
		}
	}
	
	private YDate getCutOffTimeForShipNode(String shipNode, YFCDocument docGetCommonCodeListOutput){
		YFCElement elemCutOffTime = getCommonCodeFor(TelstraConstants.CUT_OFF_TIME, shipNode, docGetCommonCodeListOutput);
		if(elemCutOffTime == null){
			elemCutOffTime = getCommonCodeFor(TelstraConstants.CUT_OFF_TIME, "DEFAULT", docGetCommonCodeListOutput);
			if(elemCutOffTime == null){
				return null;
			}
		}
		String timeString = elemCutOffTime.getAttribute("CodeShortDescription");
		int hours = Integer.parseInt(timeString.substring(0, 2));
		int minutes = Integer.parseInt(timeString.substring(2));
		YDate today = new YDate(false);
		today.setHours(hours);
		today.setMinutes(minutes);
		today.setSeconds(0);
		return today;
	}
	
	private YFCElement getCommonCodeFor(String codeType, String codeValue, YFCDocument docGetCommonCodeListOutput){
		if(!isValidCommonCodeList(docGetCommonCodeListOutput)){
			return null;
		}
		for(Iterator<YFCElement> itr = docGetCommonCodeListOutput.getDocumentElement().getChildren().iterator();itr.hasNext();){
			YFCElement elem = itr.next();
			String elmCodeType = elem.getAttribute("CodeType");
			String elmCodeValue = elem.getAttribute(TelstraConstants.CODE_VALUE);
			if(YFCObject.isVoid(elmCodeType) || YFCObject.isVoid(elmCodeValue)){
				continue;
			}
			if(elmCodeType.equalsIgnoreCase(codeType) && elmCodeValue.equalsIgnoreCase(codeValue)){
				return elem;
			}
		}
		return null;
	}
	
	private boolean isValidCommonCodeList(YFCDocument docGetCommonCodeListOutput){
		if(docGetCommonCodeListOutput == null || docGetCommonCodeListOutput.getDocumentElement() == null || docGetCommonCodeListOutput.getDocumentElement().getChildren() == null){
			return false;
		}
		return true;
	}
	
	protected String getShipNode(YFCElement orderElem){
		YFCElement elemOrderLines = orderElem.getChildElement("OrderLines");
		if(elemOrderLines == null){
			return null;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			return elemOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
		}
		return null;
	}
	
		
	private String getStateForShipNode(String shipNode,YFCDocument docGetCommonCodeListOutput){
		if(YFCObject.isVoid(shipNode)){
			return null;
		}
		YFCElement elemCommonCode =  getCommonCodeFor("DC_STATE_MAP", shipNode, docGetCommonCodeListOutput);
		if(elemCommonCode == null){
			return null;
		}
		return elemCommonCode.getAttribute("CodeShortDescription");
	}
	
	private Priority getOrderPriority(YFCDocument docOrderXml){
		String priority = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.PRIORITY);
		if(isUrgentPriority(priority)){
			return Priority.URGENT;
		}
		if(!YFCObject.isVoid(priority) && priority.equalsIgnoreCase(TelstraConstants.ORDER_PRIORITY_MEDICAL)){
			return Priority.MEDICAL;
		}
		String shipToId = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.SHIP_TO_ID);
		if(isHomeAddress(shipToId)){
			return Priority.STANDARD_HOME_DELIVERY;
		}
		return Priority.STANDARD;
	}
	
	private boolean isUrgentPriority(String priority){
		if(YFCObject.isVoid(priority)){
			return false;
		}
		if("Y".equalsIgnoreCase(priority) || "1".equalsIgnoreCase(priority) || "2".equalsIgnoreCase(priority) || "3".equalsIgnoreCase(priority)){
			return true;
		}
		return false;
	}
	
	private YFCElement getDACSchedule(YFCElement orderElem){
		String dac = orderElem.getAttribute(TelstraConstants.SHIP_TO_ID);
		String shipNode = getShipNode(orderElem);
		YFCDocument getDACSchduleInput = YFCDocument.getDocumentFor("<DacSchedule Node='"+shipNode+"' DeliveryAddressCode='"+dac+"'/>");
		YFCDocument dacScheduleOutput = invokeYantraService(TelstraConstants.API_GET_DAC_SCHEDULE, getDACSchduleInput);
		if(dacScheduleOutput == null || dacScheduleOutput.getDocumentElement() == null || dacScheduleOutput.getAttributes() == null){
			return null;
		}
		return dacScheduleOutput.getDocumentElement();
	}
	
	private YFCElement getMilkRunSchedule(String routeId,YDate dateToWham){
		YFCDocument milkRunSchedule = YFCDocument.getDocumentFor("<MilkRunSchedule RouteId='"+routeId+"' />");
		YFCDocument milkRunScheduleOutput = invokeYantraService(TelstraConstants.API_GET_MILK_RUN_SCHEDULE, milkRunSchedule);
		if(milkRunScheduleOutput == null || milkRunScheduleOutput.getDocumentElement() == null || milkRunScheduleOutput.getDocumentElement().getAttributes() == null){
			return null;
		}
		YDate effectiveFrom = milkRunScheduleOutput.getDocumentElement().getYDateAttribute(TelstraConstants.EFFECTIVE_FROM);
		YDate effectiveTo = milkRunScheduleOutput.getDocumentElement().getYDateAttribute(TelstraConstants.EFFECTIVE_TO);
		if((effectiveFrom == null || dateToWham.after(effectiveFrom)) && (effectiveTo == null || dateToWham.before(effectiveTo))){
			return milkRunScheduleOutput.getDocumentElement(); 
		}
		return null;
	}
	
	private YDate getNextBusinessDayBasedOnSchedule(YDate shipDate,String schedule, YFCDocument weekNumbers, List<String> exceptionDates, YFCElement allDayShift){
		Schedule scheduleObj = new Schedule(schedule);
		YDate newShipDate = new YDate(shipDate.getString(TelstraConstants.TELSTRA_DATE_FORMAT), TelstraConstants.TELSTRA_DATE_FORMAT, true);
		YFCDateUtils.addHours(newShipDate,24);
		int maxCount = 100000;
		int count = 0;
		
		while(true){
			count++;
			if(OrderDateHelper.isNonWorkingDay(newShipDate, allDayShift)){
				YFCDateUtils.addHours(newShipDate, 24);
			}else{
				boolean isValid;
				if(weekNumbers != null){
					int weekNumber = OrderDateHelper.getWeekNumber(weekNumbers, newShipDate);
					isValid = scheduleObj.isValidDate(weekNumber, newShipDate);
				}else{
					isValid = scheduleObj.isValidDay(newShipDate);
				}			
				if(isValid || count > maxCount){
					if(OrderDateHelper.isExceptionDay(newShipDate, exceptionDates)){
						newShipDate = OrderDateHelper.getNextBusinessDay(newShipDate, allDayShift, exceptionDates);
					}
					break;
				}
				YFCDateUtils.addHours(newShipDate, 24);
			}
		}
		if(count > maxCount){
			return null;
		}
		return newShipDate;
	}
	
	private boolean isHomeAddress(String shipToId){
		YFCDocument docGetCustomer = YFCDocument.getDocumentFor("<Customer CustomerID='"+shipToId+"' />");
		YFCDocument docGetCustomerOutput = invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST, docGetCustomer);
		if(docGetCustomerOutput != null && docGetCustomerOutput.getDocumentElement() != null){
			YFCElement elemCustomer = docGetCustomerOutput.getDocumentElement().getFirstChildElement();
			if(elemCustomer == null){
				return false;
			}
			String customerClassificationCode = elemCustomer.getAttribute(TelstraConstants.CUSTOMER_CLASSIFICATION_CODE);
			if(!YFCObject.isVoid(customerClassificationCode) && customerClassificationCode.equalsIgnoreCase(TelstraConstants.HOME_ADDRESS)){
				return true;
			}
		}
		return false;
	}
	
	private YFCDocument getWeekNumbers(){
		YFCDocument input = YFCDocument.getDocumentFor("<WeekNumber />");
		return invokeYantraService(TelstraConstants.API_GET_WEEK_NUMBER_LIST, input);
	}
	
	private YFCDocument getOrganizationHeirarchy(String node) {
		String orgXmlStr = "<Organization OrganizationCode='"+node+"' />";
		YFCDocument docOrgInput = YFCDocument.getDocumentFor(orgXmlStr);
		String orgTemplate = "<Organization LocaleCode=\"\" OrganizationCode=\"\"><CorporatePersonInfo State=\"\" City=\"\" /></Organization>";
		YFCDocument docOrgTemplate = YFCDocument.getDocumentFor(orgTemplate);
		return invokeYantraApi(TelstraConstants.API_GET_ORGANIZATION_HEIRARCHY, docOrgInput, docOrgTemplate);
	}
	
	private boolean requireOrderDateCalculations(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		if(entryType.equalsIgnoreCase(TelstraConstants.INTEGRAL_PLUS) || entryType.equalsIgnoreCase(TelstraConstants.VECTOR)){
			return true;
		}
		return false;
	}
	
	private boolean isIntegralOrder(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		if(entryType.equalsIgnoreCase(TelstraConstants.INTEGRAL_PLUS)){
			return true;
		}
		return false;
	}
	
	private boolean isVectorOrder(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		if(entryType.equalsIgnoreCase(TelstraConstants.VECTOR)){
			return true;
		}
		return false;
	}
	
	private String getMaterialClassFromDepartmentCode(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput){
		String departmentCode = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.DEPARTMENT_CODE);
		if(YFCObject.isVoid(departmentCode)){
			return null;
		}
		YFCElement elemCommonCode = getCommonCodeFor(TelstraConstants.DEPARTMENT_MATERIAL_CLASS_MAP,departmentCode, docGetCommonCodeListOutput);
		if(elemCommonCode == null){
			return null;
		}
		return elemCommonCode.getAttribute("CodeShortDescription");
	}
	
	
	private boolean isHazmatOrder(YFCElement orderElem){
		YFCElement elemOrderLines = orderElem.getChildElement("OrderLines");
		if(elemOrderLines == null){
			return false;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			YFCElement elemItem = elemOrderLine.getChildElement(TelstraConstants.ITEM);
			if(elemItem == null){
				continue;
			}
			String itemId = elemItem.getAttribute(TelstraConstants.ITEM_ID);
			if(YFCObject.isVoid(itemId)){
				continue;
			}
			if(isHazmatItem(itemId)){
				return true;
			}
		}
		return false;
	}
	
	private boolean isHazmatItem(String itemId){
		YFCDocument docItemDetails = getItemDetails(itemId);
		if(docItemDetails == null || docItemDetails.getDocumentElement() == null){
			return false;
		}
		YFCElement elemItemPrimaryInfo = docItemDetails.getDocumentElement().getChildElement("PrimaryInformation");
		if(elemItemPrimaryInfo == null){
			return false;
		}
		return elemItemPrimaryInfo.getBooleanAttribute(TelstraConstants.IS_HAZMAT);
	}
	
	private YFCDocument getItemDetails(String itemId){
		YFCDocument docGetItemDetails = YFCDocument.getDocumentFor("<Item ItemID = '"+itemId+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' />");
		return invokeYantraApi(TelstraConstants.API_GET_ITEM_DETAILS, docGetItemDetails);
	}
	
	private YFCDocument getCalendarDocument(String shipNodeState){
		YFCDocument docGetCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+shipNodeState+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"'/>");
		YFCDocument calendarDoc =  getCalendarDocument(docGetCalendar);
		if((calendarDoc == null || calendarDoc.getDocumentElement() == null) && !TelstraConstants.DEFAULT_CALENDAR_STATE.equalsIgnoreCase(shipNodeState)){
			shipNodeState = TelstraConstants.DEFAULT_CALENDAR_STATE;
			docGetCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+shipNodeState+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"'/>");
			return getCalendarDocument(docGetCalendar);
		}
		return calendarDoc; 
	}
		
	private YFCDocument getCalendarDocument(YFCDocument docGetCalendar){
		try{
			return invokeYantraApi(TelstraConstants.API_GET_CALENDAR_DETAILS, docGetCalendar);
		}catch(YFSException ex){
			return null;
		}
	}
	
	private class Schedule{
		private String scheduleString;
		private int scheduleWeekNumber;
		private List<Integer> validWeekNumbers;
		private List<Integer> workingDays;

		Schedule(String scheduleString){
			this.scheduleString = scheduleString;
			String scheduleWeekNumberString = scheduleString.substring(0, 1);
			this.scheduleWeekNumber = Integer.parseInt(scheduleWeekNumberString);
			populateValidWeekNumbers();
			populateWeekDays();
		}
		
		private void populateValidWeekNumbers(){
			validWeekNumbers = new ArrayList<>();
			switch(scheduleWeekNumber){
				case 0:
					validWeekNumbers.add(1);
					validWeekNumbers.add(2);
					validWeekNumbers.add(3);
					validWeekNumbers.add(4);
					validWeekNumbers.add(5);
					validWeekNumbers.add(6);
					break;
				case 1:
					validWeekNumbers.add(1);
					validWeekNumbers.add(3);
					validWeekNumbers.add(5);
					break;
				case 2:
					validWeekNumbers.add(2);
					validWeekNumbers.add(4);
					validWeekNumbers.add(6);
					break;
				case 3:
					validWeekNumbers.add(3);
					break;
				case 4:
					validWeekNumbers.add(4);
					break;
				case 5:
					validWeekNumbers.add(5);
					break;
				default:
					validWeekNumbers.add(6);					
			}
		}
		
		private void populateWeekDays(){
			workingDays = new ArrayList<>();
			String scheduleWeekDayString = scheduleString.substring(1);
			for(int i=0;i<scheduleWeekDayString.length();i++){
				char weekDayChar = scheduleWeekDayString.charAt(i);
				switch(weekDayChar){
					case 'A' :
						workingDays.add(2);
						break;
					case 'B' :
						workingDays.add(3);
						break;
					case 'C' :
						workingDays.add(4);
						break;
					case 'D' :
						workingDays.add(5);
						break;
					case 'E' :
						workingDays.add(6);
						break;
					default:
			            break;
				}
			}
		}
		
		boolean isValidDate(int weekNumber,YDate date){
			if(!validWeekNumbers.contains(weekNumber)){
				return false;
			}
            return isValidDay(date);
		}
		
		boolean isValidDay(YDate date){
			int dayOfWeek = date.getDayOfWeek();
			if(workingDays.contains(dayOfWeek)){
				return true;
			}
			return false;
		}
	}
	
	private enum Priority{
		URGENT,STANDARD,STANDARD_HOME_DELIVERY,MEDICAL,LOW
	}
}
