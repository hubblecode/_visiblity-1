package com.gps.hubble.order.ue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCDateUtils;

public final class OrderDateHelper {
	
	private OrderDateHelper(){
		
	}
	
	public static YDate getOrderDate(YFCElement orderElem,String dateTypeId){
		YFCElement orderDates = orderElem.getChildElement("OrderDates");
		for(Iterator<YFCElement> itr = orderDates.getChildren(TelstraConstants.ORDER_DATE).iterator();itr.hasNext();){
			YFCElement orderDate = itr.next();
			if(orderDate.getAttribute(TelstraConstants.DATE_TYPE_ID).equalsIgnoreCase(dateTypeId)){
				return orderDate.getYDateAttribute(TelstraConstants.REQUESTED_DATE);
			}
		}
		return null;
	}
	
	public static List<String> getExceptionDates(YFCDocument calendarDoc){
		YFCElement calendar = calendarDoc.getDocumentElement();
		List<String> exceptionDates = new ArrayList<>();
		YFCElement calendarDayExceptions = calendar.getChildElement(TelstraConstants.CALENDAR_DAY_EXCEPTIONS);
		for(Iterator<YFCElement> itr = calendarDayExceptions.getChildren(TelstraConstants.CALENDAR_DAY_EXCEPTION).iterator();itr.hasNext();){
			YFCElement calendarDayException = itr.next();
			YDate date = calendarDayException.getYDateAttribute("Date");
			if(date != null){
				exceptionDates.add(date.getString("yyyy-MM-dd"));
			}
		}
		return exceptionDates;
	}
	
	public static YFCElement getAllDayShiftElement(YFCDocument calendarDoc, YDate shipDate){
		YFCElement allDayShift = null;
		YFCElement calendarElem = calendarDoc.getDocumentElement();
		YFCElement effectivePeriods = calendarElem.getChildElement(TelstraConstants.EFFECTIVE_PERIODS);
		if(effectivePeriods == null || effectivePeriods.getChildren(TelstraConstants.EFFECTIVE_PERIOD) == null){
			return null;
		}
		for(Iterator<YFCElement> itr = effectivePeriods.getChildren(TelstraConstants.EFFECTIVE_PERIOD).iterator();itr.hasNext();){
			YFCElement effectivePeriod = itr.next();
			YDate effectiveFromDate = effectivePeriod.getYDateAttribute(TelstraConstants.EFFECTIVE_FROM_DATE);
			YDate effectiveToDate = effectivePeriod.getYDateAttribute(TelstraConstants.EFFECTIVE_TO_DATE);
			if(!shipDate.after(effectiveFromDate) || !shipDate.before(effectiveToDate)){
				continue;
			}
			YFCElement shiftsElem = effectivePeriod.getChildElement(TelstraConstants.SHIFTS);
			for(Iterator<YFCElement> itr2 = shiftsElem.getChildren(TelstraConstants.SHIFT).iterator();itr2.hasNext();){
				YFCElement shiftElem = itr2.next();
				if(shiftElem.getAttribute(TelstraConstants.SHIFT_NAME) != null && shiftElem.getAttribute(TelstraConstants.SHIFT_NAME).equalsIgnoreCase(TelstraConstants.SHIFT_NAME_ALL_DAY)){
					allDayShift = shiftElem;
					break;
				}
			}
			
		}
		return allDayShift;
	}
	
	public static YFCDocument createWeekNumberDocForFortnightlyFrequency(YDate effectiveFrom,YDate shipDateNextDate){
		YFCDocument weekNumbers = YFCDocument.createDocument(TelstraConstants.WEEK_NUMBER_LIST);
		boolean shipDateReached = false;
		int afterShipDateWeekCount = 0; //We will keep it 12
		int maxAfterShipDateWeekCount = 12;
		boolean evenWeek = true;
		while(true){
			YDate tmpEffectiveFrom = null;
			if(shipDateReached){
				if(afterShipDateWeekCount > maxAfterShipDateWeekCount){
					break;
				}
				afterShipDateWeekCount++;
				createWeekNumberElement(weekNumbers, effectiveFrom, evenWeek);
			}else{
				tmpEffectiveFrom = new YDate(effectiveFrom.getTime(),true);
				YFCDateUtils.addHours(tmpEffectiveFrom, 24*7);
			}
			if(tmpEffectiveFrom != null && tmpEffectiveFrom.after(shipDateNextDate)){
				shipDateReached = true;
			}else{
				evenWeek = !evenWeek;
				YFCDateUtils.addHours(effectiveFrom, 24*7);
			}
		}
		return weekNumbers;
	}
	
	public static void createWeekNumberElement(YFCDocument weekNumbers, YDate date, boolean evenWeek){
		YFCElement weekNumber = weekNumbers.getDocumentElement().createChild(TelstraConstants.WEEK_NUMBER);
		YDate newDate = new YDate(date.getString(TelstraConstants.TELSTRA_DATE_FORMAT),TelstraConstants.TELSTRA_DATE_FORMAT,true);
		YDate fromDate = new YDate(newDate.getTime(),true);
		YFCDateUtils.addHours(newDate, 24*6);
		YDate toDate = new YDate(newDate.getTime(),true); 
		weekNumber.setAttribute(TelstraConstants.FROM_DATE, fromDate);
		YFCDateUtils.addHours(newDate, 24*6);
		weekNumber.setAttribute(TelstraConstants.TO_DATE, toDate);
		if(evenWeek){
			weekNumber.setAttribute(TelstraConstants.WEEK_NUMBER, 2);
		}else{
			weekNumber.setAttribute(TelstraConstants.WEEK_NUMBER, 1);
		}
	}
	
	public static int getTransitTime(YDate shipDate,YFCElement transportMatrix){
		int dayOfWeek = shipDate.getDayOfWeek();
	    int transitTime;
	    switch(dayOfWeek){
	    	case 2:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.MONDAY_TRANSIT_TIME);
	    		break;
	    	case 3:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.TUESDAY_TRANSIT_TIME);
	    		break; 
	    	case 4:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.WEDNESDAY_TRANSIT_TIME);
	    		break;
	    	case 5:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.THURSDAY_TRANSIT_TIME);
	    		break;
	    	default:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.FRIDAY_TRANSIT_TIME);
	    		break;
	    }
	    return transitTime;
	}
	
	public static int getWeekNumber(YFCDocument weekNumbers, YDate date){
		int weekNumber = 0;
		for(Iterator<YFCElement> itr = weekNumbers.getDocumentElement().getChildren();itr.hasNext();){
			YFCElement weekNumberElem = itr.next();
			YDate fromDate = weekNumberElem.getYDateAttribute(TelstraConstants.FROM_DATE);
			YDate toDate = weekNumberElem.getYDateAttribute(TelstraConstants.TO_DATE);
			if((fromDate.before(date) || fromDate.getString().equals(date.getString())) && toDate.after(date)){
				weekNumber = weekNumberElem.getIntAttribute(TelstraConstants.WEEK_NUMBER);
			}
		}
		return weekNumber;
	}
	
	public static YDate getNextBusinessDay(YDate date, YFCElement allDayShift, List<String> exceptionDates){
		long time = date.getTime();
		YDate newYDate = new YDate(time, false);
		YFCDateUtils.addHours(newYDate, 24);
		LinkedList<YDate> nextDates = new LinkedList<>();
		nextDates.add(newYDate);
		removeExceptionAndWeekends(nextDates, allDayShift, exceptionDates);
		return nextDates.getLast();
	}
	
	public static void removeExceptionAndWeekends(LinkedList<YDate> transitDates, YFCElement allDayShift,List<String> exceptionDates){
		YDate toRemove = null;
		for(YDate transitDate : transitDates){
			if(isNonWorkingDay(transitDate, allDayShift) || isExceptionDay(transitDate,exceptionDates)){
				toRemove = transitDate;
				break;
			}
		}
		if(toRemove != null){
			//Add a new date
			YDate lastTransitDate = transitDates.peekLast();
			YDate newTransitDate = new YDate(lastTransitDate.getString(TelstraConstants.DATE_TIME_FORMAT), TelstraConstants.DATE_TIME_FORMAT, false);
			YFCDateUtils.addHours(newTransitDate, 24);
			transitDates.remove(toRemove);
			transitDates.add(newTransitDate);
			removeExceptionAndWeekends(transitDates, allDayShift,exceptionDates);
		}
		
	}
	
	public static boolean isNonWorkingDay(YDate date, YFCElement allDayShift){
		if(allDayShift == null){
			return false;
		}
		int dayOfWeek = date.getDayOfWeek();
		boolean isNonWorking = false;
		switch(dayOfWeek){
			case 1:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.SUNDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 2:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.MONDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 3:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.TUESDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 4:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.WEDNESDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 5:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.THURSDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 6:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.FRIDAY_VALID)){
					isNonWorking = true;
				}
				break;
			case 7:
				if(!allDayShift.getBooleanAttribute(TelstraConstants.SATURDAY_VALID)){
					isNonWorking = true;
				}
				break;
			default:
				isNonWorking = false;
		}
		return isNonWorking;
	}
	
	public static boolean isExceptionDay(YDate date, List<String> exceptionDates){
		String dateString = date.getString("yyyy-MM-dd");
		return exceptionDates.contains(dateString);
	}
}
