/***********************************************************************************************
 * File	Name		: ManageOrder.java
 *
 * Description		: This class is called from General as a service. 
 * 						It mainly Creates or Updates Order related Information. 
 *                      It changes Shipment, Order and OrderLine Statuses.
 * 						It Validates BackOrder Cancel and Deliver.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 10,2016	  	Keerthi Yadav 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to create Hold for Duplicate Order
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 */

public class ManageOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory
			.instance(ManageOrder.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),
				"invoke", inXml);
		String sOrderName = inXml.getDocumentElement().getAttribute(
				TelstraConstants.ORDER_NAME, "");
		if (!YFCObject.isVoid(sOrderName)) {
			YFCDocument docGetOrderListOutXml = getOrderList(sOrderName);
			YFCElement eleOrderOut = docGetOrderListOutXml
					.getElementsByTagName(TelstraConstants.ORDER).item(0);
			if (!YFCObject.isVoid(eleOrderOut)) {
				for (YFCElement eleInputOrderLine : inXml
						.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					String sPrimeLineNo = eleInputOrderLine.getAttribute(
							TelstraConstants.PRIME_LINE_NO, "");
					YFCElement eleTempOrderLine = null;
					for (YFCElement eleOrderListLine : eleOrderOut
							.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
						String sOrderListPrimeLineNo = eleOrderListLine
								.getAttribute(TelstraConstants.PRIME_LINE_NO,
										"");
						if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
							eleTempOrderLine = eleOrderListLine;
							break;
						}
					}
					if (YFCObject.isNull(eleTempOrderLine)) {
						// changeorder with orderline action=CREATE
						changeOrderToCreateNewLine(eleInputOrderLine,
								docGetOrderListOutXml);
					} else {
						String sOrderLineStatus = eleInputOrderLine
								.getAttribute(TelstraConstants.CHANGE_STATUS,
										"");
						if (YFCObject.isNull(sOrderLineStatus)) {
							// call change order with order line action modify
							// and pass inXml order line element and add
							// OrderLineKey
							changeOrderToUpdateOrderLine(eleInputOrderLine,
									eleTempOrderLine, docGetOrderListOutXml);
						} else {
							processOrderLine(eleTempOrderLine,
									docGetOrderListOutXml, sOrderLineStatus);
						}
					}
				}
			} else {
				createOrder(inXml);
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",
				inXml);
		return inXml;
	}

	/**
	 * 
	 * @param eleTempOrderLine
	 * @param docGetOrderListOutXml
	 * @param sOrderLineStatus
	 */
	private void processOrderLine(YFCElement eleTempOrderLine,
			YFCDocument docGetOrderListOutXml, String sOrderLineStatus) {
		// Configure in service arguments
		String sDeliverstatus = getProperty("ChangeStatusDeliver", true);// "C"
		String sBackorderstatus = getProperty("ChangeStatusBackorder", true); // "B"
		String sCancelstatus = getProperty("ChangeStatusCancel", true); // "D"

		if (sOrderLineStatus.equalsIgnoreCase(sDeliverstatus)) {
			deliverOrderLine(eleTempOrderLine, docGetOrderListOutXml);
		} else if (sOrderLineStatus.equalsIgnoreCase(sCancelstatus)) {
			cancelOrderLine(eleTempOrderLine, docGetOrderListOutXml);
		} else if (sOrderLineStatus.equalsIgnoreCase(sBackorderstatus)) {
			backOrderLine(eleTempOrderLine);
		}
	}

	/**
	 * 
	 * @param eleInputOrderLine
	 * @param eleTempOrderLine
	 * @param docGetOrderListOut
	 */
	private void changeOrderToUpdateOrderLine(YFCElement eleInputOrderLine,
			YFCElement eleTempOrderLine, YFCDocument docGetOrderListOut) {
		// call changeOrder with input xml = <Order OrderHeaderKey=""
		// Action="MODIFY"><OrderLines><OrderLine
		// Action="MODIFY"></OrderLines></Order>
		YFCDocument tempDoc = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey=''/>");
		YFCElement eleOrder = docGetOrderListOut.getElementsByTagName(
				TelstraConstants.ORDER).item(0);
		String date = getDateStamp();
		YFCDocument inDoc = YFCDocument
				.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
						+ eleOrder
								.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
						+ "' Action='MODIFY' ModificationReasonText='DELIVERED at "
						+ date + "'><OrderLines></OrderLines></Order>");
		eleInputOrderLine.setAttribute("Action", "MODIFY");
		eleInputOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY,
				eleTempOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY));
		inDoc.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0)
				.importNode(eleInputOrderLine.cloneNode(true));
		invokeYantraApi("changeOrder", inDoc, tempDoc);
	}

	/**
	 * 
	 * @param eleOrderLine
	 * @param docGetOrderListOut
	 */
	private void changeOrderToCreateNewLine(YFCElement eleOrderLine,
			YFCDocument docGetOrderListOut) {
		// call changeOrder with input xml = <Order OrderHeaderKey=""
		// Action="MODIFY"><OrderLines><OrderLine
		// Action="CREATE"></OrderLines></Order>
		YFCDocument tempDoc = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey=''/>");
		YFCElement eleOrder = docGetOrderListOut.getElementsByTagName(
				TelstraConstants.ORDER).item(0);
		String date = getDateStamp();
		YFCDocument inDoc = YFCDocument
				.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
						+ eleOrder
								.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
						+ "' Action='MODIFY' ModificationReasonText='DELIVERED at "
						+ date + "'><OrderLines></OrderLines></Order>");
		eleOrderLine.setAttribute("Action", "CREATE");
		inDoc.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0)
				.importNode(eleOrderLine.cloneNode(true));
		invokeYantraApi("changeOrder", inDoc, tempDoc);
	}

	/***** Method for getting OrderList *****/
	private YFCDocument getOrderList(String sOrderName) throws YFCException {
		YFCDocument docOrderListinXml = YFCDocument
				.getDocumentFor("<Order OrderName='"
						+ sOrderName
						+ "' ><OrderLine ChainedFromOrderHeaderKeyQryType='ISNULL'/></Order>");
		YFCDocument doctempgetOrderListinXml = YFCDocument
				.getDocumentFor("<OrderList><Order EnterpriseCode='' DocumentType='' OrderHeaderKey=''>"
						+ "<OrderLines><OrderLine OrderLineKey='' PrimeLineNo='' OrderHeaderKey='' Status=''><OrderStatuses><OrderStatus Status='' OrderReleaseKey=''/>"
						+ "</OrderStatuses></OrderLine></OrderLines></Order></OrderList>");
		return invokeYantraApi("getOrderList", docOrderListinXml,
				doctempgetOrderListinXml);
	}

	/*****
	 * Method for DeliverOrder
	 * 
	 * @param sOrderHeaderKey
	 * @param sDocumentType
	 * @param sEnterpriseCode
	 *****/
	private void deliverOrderLine(YFCElement eleOrderLine,
			YFCDocument docGetOrderListOut) throws YFCException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),
				"deliverOrderLine", "");
		String sSetOrderLineKey = null;
		YFCElement eleOrderFromOrderList = docGetOrderListOut
				.getElementsByTagName(TelstraConstants.ORDER).item(0);
		YFCDocument docChainOrderListInXml = YFCDocument
				.getDocumentFor("<Order StatusQryType='LT' Status='3700.7777'><OrderLine ChainedFromOrderHeaderKey='"
						+ eleOrderFromOrderList
								.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
						+ "'/></Order>");
		YFCDocument doctempChaingetOrderListinXml = YFCDocument
				.getDocumentFor("<OrderList><Order EnterpriseCode='' DocumentType='' OrderHeaderKey=''><OrderLines><OrderLine "
						+ "OrderLineKey='' ChainedFromOrderLineKey=''/></OrderLines></Order></OrderList>");
		YFCDocument docChainOrderoutXml = invokeYantraApi("getOrderList",
				docChainOrderListInXml, doctempChaingetOrderListinXml);
		YFCElement eleChainedOrder = docChainOrderoutXml.getElementsByTagName(
				TelstraConstants.ORDER).item(0);
		String strShipFromStatus = "1400";
		String strShipToStatus = "1499";
		String sOrderLineKey = eleOrderLine
				.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		if (!YFCObject.isVoid(eleChainedOrder)){
		for (YFCElement eleOrderListLine : eleChainedOrder
				.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sChainedfromOrderLineKey = eleOrderListLine
					.getAttribute("ChainedFromOrderLineKey");
			if (YFCObject.equals(sOrderLineKey, sChainedfromOrderLineKey)) {
				sSetOrderLineKey =eleOrderListLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);			
				break;
			}
		}
	}
		if (!YFCObject.isVoid(eleChainedOrder)
				&& !YFCObject.isVoid(sSetOrderLineKey)) {
			
			deliverChainedOrderShipment(eleChainedOrder,strShipFromStatus,strShipToStatus,sSetOrderLineKey);
			
		} else {
			// get shipment associated with this Sales OrderLine
			
			deliverOrderShipment(strShipFromStatus,strShipToStatus,eleOrderFromOrderList,eleOrderLine);
			
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(),
				"deliverOrderLine", "");
	}
	
	/*
	 * Delivering the shipment of Chained Order If any
	 */
	
	
	private void deliverChainedOrderShipment(YFCElement eleChainedOrder,String strShipFromStatus,String strShipToStatus, String sSetOrderLineKey){
	YFCDocument docGetShipmentForChainedOrderInput = YFCDocument
			.getDocumentFor("<Shipment EnterpriseCode='"
					+ eleChainedOrder.getAttribute("EnterpriseCode")
					+ "'  DocumentType='"
					+ eleChainedOrder.getAttribute("DocumentType")
					+ "' StatusQryType='BETWEEN' FromStatus='"
					+ strShipFromStatus
					+ "' ToStatus='"
					+ strShipToStatus
					+ "'><ShipmentLines><ShipmentLine OrderLineKey='"
					+ sSetOrderLineKey
					+ "' OrderHeaderKey='"
					+ eleChainedOrder
							.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
					+ "'/></ShipmentLines></Shipment>");
	YFCDocument docGetShipmentListTemplate = YFCDocument
			.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status='' /></Shipments>");
	YFCDocument docGetShipmentForChainedOrderOutput = invokeYantraApi(
			"getShipmentList", docGetShipmentForChainedOrderInput,
			docGetShipmentListTemplate);
	if (docGetShipmentForChainedOrderOutput.getElementsByTagName(
			"Shipment").getLength() > 0) {
		for (YFCElement eleShipment : docGetShipmentForChainedOrderOutput
				.getElementsByTagName(TelstraConstants.SHIPMENT)) {
			if (!YFCObject.isVoid(eleShipment)) {
				// Chained Order has shipment - call deliverShipment API
				String sShipmentKey = eleShipment.getAttribute(
						TelstraConstants.SHIPMENT_KEY, "");
				invokeYantraApi(
						"deliverShipment",
						YFCDocument
								.getDocumentFor("<Shipment ShipmentKey='"
										+ sShipmentKey + "'/>"));
			}
		}
	}
}
	
	private void deliverOrderShipment(String strShipFromStatus,String strShipToStatus, YFCElement eleOrderFromOrderList, YFCElement eleOrderLine){
	{
	// get shipment associated with this Sales OrderLine
	YFCDocument docGetShipmentForSOInput = YFCDocument
			.getDocumentFor("<Shipment StatusQryType='BETWEEN' FromStatus='"
					+ strShipFromStatus
					+ "' ToStatus='"
					+ strShipToStatus
					+ "' EnterpriseCode='"
					+ eleOrderFromOrderList
							.getAttribute("EnterpriseCode")
					+ "'  DocumentType='"
					+ eleOrderFromOrderList
							.getAttribute("DocumentType")
					+ "'><ShipmentLines><ShipmentLine OrderLineKey='"
					+ eleOrderLine.getAttribute("OrderLineKey")
					+ "' OrderHeaderKey='"
					+ eleOrderLine.getAttribute("OrderHeaderKey")
					+ "' /></ShipmentLines></Shipment>");
	YFCDocument docGetShipmentListTemplate = YFCDocument
			.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''/></Shipments>");
	YFCDocument docGetShipmentForSOOutput = invokeYantraApi(
			"getShipmentList", docGetShipmentForSOInput,
			docGetShipmentListTemplate);
	YFCElement eleShipmentcheck = docGetShipmentForSOOutput
			.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
	if (!YFCObject.isVoid(eleShipmentcheck)) {
		for (YFCElement eleShipment : docGetShipmentForSOOutput
				.getElementsByTagName(TelstraConstants.SHIPMENT)) {
			// Chained Order has shipment - call deliverShipment API
			String sShipmentKey = eleShipment.getAttribute(
					TelstraConstants.SHIPMENT_KEY, "");
			invokeYantraApi("deliverShipment",
					YFCDocument
							.getDocumentFor("<Shipment ShipmentKey='"
									+ sShipmentKey + "'/>"));
		}
	}
	/*
	 * else {
	 * 
	 * String date = getDateStamp(); YFCDocument docchangeOrderinXml =
	 * YFCDocument
	 * .getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY +
	 * eleOrderFromOrderList.getAttribute(
	 * TelstraConstants.ORDER_HEADER_KEY, "") +
	 * "' TransactionId='GPS_OUTBOUND_SHIP_LIST.0001.ex' ModificationReasonText='Order Line# "
	 * + eleOrderLine.getAttribute("PrimeLineNo", "") + " DELIVERED at "
	 * + date +
	 * "'><OrderLines><OrderLine BaseDropStatus='3700.7777' ChangeForAllAvailableQty='Y' OrderLineKey='"
	 * + eleOrderLine.getAttribute( TelstraConstants.ORDER_LINE_KEY, "")
	 * + "'/></OrderLines></Order>");
	 * invokeYantraApi("changeOrderStatus", docchangeOrderinXml); }
	 */
}
	
	
	}
	
	
	/*****
	 * Method for CancelOrder
	 * 
	 * @param sOrderHeaderKey
	 * @param sDocumentType
	 * @param sEnterpriseCode
	 *****/
	private void cancelOrderLine(YFCElement eleOrderLine,
			YFCDocument docGetOrderListOut) throws YFCException {

		// cancel Sales Order Line
		String sOrderLineKey = eleOrderLine.getAttribute(
				TelstraConstants.ORDER_LINE_KEY, "");
		String sOrderLineStatus = eleOrderLine.getAttribute("Status", "");
		if (sOrderLineStatus.equals("Shipped")
				|| sOrderLineStatus.equals("Intransit")) {
			// Shipment is in Shipped status we need to unconfirmShipment and
			// then call cancelOrder
			// get shipment associated with this Sales OrderLine
			String strShipFromStatus = "1400";
			String strShipToStatus = "1499";
			YFCElement eleOrderFromOrderList = docGetOrderListOut
					.getDocumentElement();
			YFCDocument docGetShipmentForSOInput = YFCDocument
					.getDocumentFor("<Shipment StatusQryType='BETWEEN' FromStatus='"
							+ strShipFromStatus
							+ "' ToStatus='"
							+ strShipToStatus
							+ "' EnterpriseCode='"
							+ eleOrderFromOrderList.getChildElement(
									TelstraConstants.ORDER).getAttribute(
									"EnterpriseCode")
							+ "'  DocumentType='"
							+ eleOrderFromOrderList.getChildElement(
									TelstraConstants.ORDER).getAttribute(
									"DocumentType")
							+ "'><ShipmentLines><ShipmentLine OrderLineKey='"
							+ eleOrderLine.getAttribute("OrderLineKey")
							+ "' OrderHeaderKey='"
							+ eleOrderLine.getAttribute("OrderHeaderKey")
							+ "' /></ShipmentLines></Shipment>");
			YFCDocument docGetShipmentListTemplate = YFCDocument
					.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''/></Shipments>");
			YFCDocument docGetShipmentForSOOutput = invokeYantraApi(
					"getShipmentList", docGetShipmentForSOInput,
					docGetShipmentListTemplate);
			YFCElement eleShipmentcheck = docGetShipmentForSOOutput
					.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
			if (!YFCObject.isVoid(eleShipmentcheck)) {
				for (YFCElement eleShipment : docGetShipmentForSOOutput
						.getElementsByTagName(TelstraConstants.SHIPMENT)) {
					String sShipmentKey = eleShipment.getAttribute(
							TelstraConstants.SHIPMENT_KEY, "");
					invokeYantraApi("unconfirmShipment",
							YFCDocument
									.getDocumentFor("<Shipment ShipmentKey='"
											+ sShipmentKey + "'/>"));
				}
			}
		}

		String date = getDateStamp();
		String sOrderHeaderKey = docGetOrderListOut
				.getElementsByTagName(TelstraConstants.ORDER).item(0)
				.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "");
		YFCDocument docchangeOrderinXml = YFCDocument
				.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
						+ sOrderHeaderKey
						+ "' "
						+ "Action='MODIFY' ModificationReasonText='Order Line # "
						+ eleOrderLine.getAttribute("PrimeLineNo", "")
						+ " CANCELLED at "
						+ date
						+ "'><OrderLines><OrderLine Action='CANCEL' OrderLineKey='"
						+ sOrderLineKey + "'></OrderLine></OrderLines></Order>");
		invokeYantraApi("cancelOrder", docchangeOrderinXml);
	}

	/**
	 * BackOrder
	 * 
	 * @param eleOrderLine
	 * @throws YFCException
	 */

	private void backOrderLine(YFCElement eleOrderLine) throws YFCException {
		// Iterate through all the OrderStatus and get OrderReleaseKey for a
		// record for which Status=3200
		String sOrderReleaseKey = null;
		for (YFCElement eleOrderStatus : eleOrderLine
				.getElementsByTagName("OrderStatus")) {
			if ("3200".equals(eleOrderStatus.getAttribute(
					TelstraConstants.STATUS, ""))) {
				sOrderReleaseKey = eleOrderStatus.getAttribute(
						TelstraConstants.ORDER_RELEASE_KEY, "");
			}
		}
		if (!YFCObject.isNull(sOrderReleaseKey)) {
			String sOrderLineKey = eleOrderLine
					.getAttribute(TelstraConstants.ORDER_LINE_KEY);
			String date = getDateStamp();
			YFCDocument docOrderReleaseinXml = YFCDocument
					.getDocumentFor("<OrderRelease Action='MODIFY' ModificationReasonText='BACKORDERED at "
							+ date
							+ "' OrderReleaseKey='"
							+ sOrderReleaseKey
							+ "'><OrderLines><OrderLine Action='BACKORDER' OrderLineKey='"
							+ sOrderLineKey
							+ "' /></OrderLines></OrderRelease>");
			invokeYantraApi("changeRelease", docOrderReleaseinXml);
		} else {
			// cancel for created status orders
			String date = getDateStamp();
			YFCDocument docchangeOrderinXml = YFCDocument
					.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
							+ eleOrderLine.getAttribute(
									TelstraConstants.ORDER_HEADER_KEY, "")
							+ "' TransactionId='SCHEDULE.0001' ModificationReasonText='Order Line# "
							+ eleOrderLine.getAttribute("PrimeLineNo", "")
							+ " DELIVERED at "
							+ date
							+ "'><OrderLines><OrderLine BaseDropStatus='1300' ChangeForAllAvailableQty='Y' OrderLineKey='"
							+ eleOrderLine.getAttribute(
									TelstraConstants.ORDER_LINE_KEY, "")
							+ "'/></OrderLines></Order>");
			invokeYantraApi("changeOrderStatus", docchangeOrderinXml);
		}
	}

	/**
	 * 
	 * @param inxml
	 * @return
	 * @throws YFCException
	 */
	private String getDACNode(YFCDocument inxml) throws YFCException {
		String sExternalCustomerID = null;
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),
				"getDACNode", "");
		String sCustomerID = inxml.getDocumentElement().getAttribute(
				TelstraConstants.BILL_TO_ID, "");
		YFCDocument docCustomerListinXml = YFCDocument
				.getDocumentFor("<Customer CustomerID='" + sCustomerID + "'/> ");
		if (YFCObject.isNull(sCustomerID)) {
			String sCustomerkey = inxml.getDocumentElement().getAttribute(
					TelstraConstants.BILL_TO_KEY, "");
			docCustomerListinXml = YFCDocument
					.getDocumentFor("<Customer Customerkey='" + sCustomerkey
							+ "'/> ");
		}
		YFCDocument docCustomerListtempXml = YFCDocument
				.getDocumentFor("<CustomerList><Customer ExternalCustomerID='' CustomerID=''/></CustomerList>");
		YFCDocument docCustomerListoutXml = invokeYantraApi("getCustomerList",
				docCustomerListinXml, docCustomerListtempXml);
		YFCElement sCustomer = docCustomerListoutXml.getDocumentElement()
				.getChildElement(TelstraConstants.CUSTOMER);
		if (!YFCObject.isNull(sCustomer)) {
			sExternalCustomerID = docCustomerListoutXml.getDocumentElement()
					.getChildElement(TelstraConstants.CUSTOMER)
					.getAttribute(TelstraConstants.EXTERNAL_CUSTOMER_ID, "");
		} else {
			// check common code
			YFCDocument docGetCommonCodeListOutput = invokeYantraApi(
					TelstraConstants.API_GET_COMMON_CODE_LIST,
					YFCDocument
							.getDocumentFor("<CommonCode CodeType=\"ORDER_VALIDATION\"/>"));
			boolean bValidation = false;
			String sValidationType = getProperty("ValidationType", true);
			for (YFCElement eleCommonCode : docGetCommonCodeListOutput
					.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
				if (eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE)
						.equals(sValidationType)
						&& eleCommonCode.getAttribute(
								TelstraConstants.CODE_SHORT_DESCRIPTION)
								.equals("Y")) {
					bValidation = true;
					break;
				}
			}
			if (bValidation) {
				if (!YFCObject.isNull(sCustomerID)
						&& sCustomerID.startsWith("V")) {
					sExternalCustomerID = sCustomerID;
				}
				createcustomer(inxml, sCustomerID, sExternalCustomerID); // pass
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),
				"getDACNode", "");
		return sExternalCustomerID;
	}

	/**
	 * 
	 * @param inxml
	 * @param sCustomerID
	 * @throws YFCException
	 */
	// creating Consumer input xml after validation
	private void createcustomer(YFCDocument inxml, String sCustomerID,
			String sExternalCustomerID) throws YFCException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),
				"createcustomer", inxml);
		YFCDocument docCustomer = YFCDocument.createDocument("Customer");
		String sCustomerkey = inxml.getDocumentElement().getAttribute(
				TelstraConstants.BILL_TO_KEY);
		String sOrganizationCode = inxml.getDocumentElement().getAttribute(
				TelstraConstants.ENTERPRISE_CODE, "");
		docCustomer.getDocumentElement().setAttribute(
				TelstraConstants.CUSTOMER_ID, sCustomerID);
		docCustomer.getDocumentElement().setAttribute(
				TelstraConstants.CUSTOMER_KEY, sCustomerkey);
		docCustomer.getDocumentElement().setAttribute("CustomerType", "02");
		docCustomer.getDocumentElement().setAttribute(
				TelstraConstants.ORGANIZATION_CODE, sOrganizationCode);
		if (!YFCObject.isNull(sExternalCustomerID)) {
			docCustomer.getDocumentElement().setAttribute("ExternalCustomerID",
					sExternalCustomerID);
		}
		docCustomer.getDocumentElement().setAttribute(TelstraConstants.STATUS,
				"10");
		YFCElement docConsumer = docCustomer.getDocumentElement().createChild(
				"Consumer");
		YFCElement eleBillingPersonInfo = inxml.getDocumentElement()
				.getChildElement(TelstraConstants.PERSON_INFO_BILL_TO);
		docConsumer.appendChild(docCustomer.importNode(eleBillingPersonInfo,
				true));
		docCustomer.getDocument().renameNode(
				docConsumer.getChildElement(
						TelstraConstants.PERSON_INFO_BILL_TO).getDOMNode(),
				null, "BillingPersonInfo");
		String gpsManageDAC = getProperty("SERVICE_NAME_GpsManageDAC", true);
		logger.verbose("docCustomer:" + docCustomer);
		invokeYantraService(gpsManageDAC, docCustomer);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "createcustomer",
				inxml);
	}

	/**
	 * 
	 * @param sExternalCustomerID
	 * @return
	 */
	// Finding the NodeType for the Node
	private String getNodeType(String sExternalCustomerID) {
		LoggerUtil.verboseLog("ExternalCustomerID" + sExternalCustomerID,
				logger, "");
		String sNodeType = null;
		YFCDocument docNodeListinXml = YFCDocument
				.getDocumentFor("<Shipment ShipNode='" + sExternalCustomerID
						+ "' />");
		YFCDocument doctempgetNodeListinXml = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode NodeType=''/></ShipNodeList>");
		YFCDocument outXml = invokeYantraApi("getShipNodeList",
				docNodeListinXml, doctempgetNodeListinXml);
		YFCElement eleShipNode = outXml.getDocumentElement().getChildElement(
				"ShipNode");
		if (!YFCElement.isNull(eleShipNode)) {
			sNodeType = outXml.getDocumentElement().getChildElement("ShipNode")
					.getAttribute(TelstraConstants.NODE_TYPE, "");
		}
		return sNodeType;
	}

	/**
	 * 
	 * @param sBillToID
	 * @return
	 */
	private Boolean getGoNoDAC(String sBillToID) {
		LoggerUtil.verboseLog("BillToID" + sBillToID, logger, "");

		boolean bValidation = false;
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(
				TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument
						.getDocumentFor("<CommonCode CodeType=\"GODACLIST\"/>"));
		for (YFCElement eleCommonCode : docGetCommonCodeListOutput
				.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			if (!YFCObject.isNull(eleCommonCode)) {
				String sCodeValue = eleCommonCode
						.getAttribute(TelstraConstants.CODE_VALUE);
				if (YFCObject.equals(sBillToID, sCodeValue)) {
					bValidation = true;
					break;
				}
			}
		}

		return bValidation;
	}

	/**
	 * Creating order Setting Document Type node DepartmentCode
	 * 
	 * @param inxml
	 */

	private void createOrder(YFCDocument inxml) {
		LoggerUtil.verboseLog("OrderNo ", logger, "");

		String sOrderType = inxml.getDocumentElement().getAttribute(
				TelstraConstants.ORDER_TYPE, "");
		String sEntryType = inxml.getDocumentElement().getAttribute(
				TelstraConstants.ENTRY_TYPE, "");
		String sInterfaceNo = inxml.getDocumentElement().getAttribute(
				TelstraConstants.INTERFACE_NO, "");
		String sDepartmentCode = inxml.getDocumentElement().getAttribute(
				TelstraConstants.DEPARTMENT_CODE, "");
		String sBillToID = inxml.getDocumentElement().getAttribute(
				TelstraConstants.BILL_TO_ID, "");
		String sSellerOrganizationCode=inxml.getDocumentElement().getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE, "");	
		String sExternalCustomerID = getDACNode(inxml);
		
		
		
		boolean bGoNoDAC = getGoNoDAC(sBillToID); // to find common code list
		// for no dac
		String sReceivingNode = null;
		String sNodeType = null;
		String sDocumentType = null;

		if (!YFCObject.isNull(sExternalCustomerID)) {
			sNodeType = getNodeType(sExternalCustomerID);
		}

		if (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")) {
			if (YFCObject.equals(sEntryType, "INTEGRAL_PLUS")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sDocumentType = "0006";
					sReceivingNode = sExternalCustomerID;
				} else {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
				}
			}

			if (YFCObject.equals(sEntryType, "VECTOR")) {
				sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
			}
		}

		/*
		 * This logic is yet to be implemented - Begin
		 * 
		 * This Material Reservation can also be a PMC LRA. In which case, if
		 * the order comes with the need of releasing the Integral Plus PMC
		 * material reservations, such sales orders need to be identified (using
		 * the Search Criteria1 attribute as the PMC Project Number (Network (PM
		 * order) +Activity (PM Operations))) and the address of the Integral
		 * Plus sales orders need to be updated with the LRA address*
		 * 
		 * This logic is yet to be implemented - End
		 */

		if (YFCObject.equals(sOrderType, "TRANSPORT_ORDER")) {
			if (YFCObject.equals(sEntryType, "INTEGRAL_PLUS")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					if (YFCObject.equals(sDepartmentCode, "NBN")) {
						sDocumentType = "0005";
					} else {
						sDocumentType = "0006";
					}
					sReceivingNode = sExternalCustomerID;
				} else {
					// throw exception
					LoggerUtil.verboseLog("TRANSPORT ORDER is not valid", logger, " throwing exception");
					String strErrorCode = getProperty("TransportOrderFailureCode", true);
					YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
					throw new YFSException(erroDoc.toString());	
				}
			}
		}

		if (YFCObject.equals(sOrderType, "PURCHASE_ORDER")) {
			if (YFCObject.equals(sEntryType, "INTEGRAL_PLUS")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sDocumentType = "0005";
					sReceivingNode = sExternalCustomerID;
				} else {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
				}
			}
			if (YFCObject.equals(sEntryType, "MERIDIAN")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sDocumentType = "0005";
					sReceivingNode = sExternalCustomerID;
				} else {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
				}
			}
		}

		if (YFCObject.equals(sOrderType, "RECOVERY_ORDER")) {
			if (YFCObject.equals(sEntryType, "VECTOR")) {
				sDocumentType = "0003";
			}
		}

		if (YFCObject.equals(sInterfaceNo, "INT_ODR_4")) {
			if (YFCObject.isNull(sDepartmentCode)) {
				if (!YFCObject.isNull(sExternalCustomerID)) {
					sDepartmentCode = "INVENTORY_MATERIAL";
				} else {
					sDepartmentCode = "CISCO_INT";
				}
			}
		}

		if ((YFCObject.equals(sInterfaceNo, "INT_ODR_1"))
				|| (YFCObject.equals(sInterfaceNo, "INT_ODR_2"))) {
			if (bGoNoDAC) {
				sDepartmentCode = "PMC";
			}
			if (YFCObject.equals(sDocumentType, "0006")) {

				if ((YFCObject.equals(sNodeType, "DC"))
						|| (YFCObject.equals(sNodeType, "LS"))
						|| (YFCObject.equals(sNodeType, "LC"))
						|| (YFCObject.equals(sNodeType, "CT"))) {
					sDepartmentCode = "INVENTORY_MATERIAL";
				} else if (YFCObject.equals(sNodeType, "WLVS")) {
					sDepartmentCode = "WIRELESS";
				} else if (YFCObject.equals(sNodeType, "WBVS")) {
					sDepartmentCode = "WIDEBAND";
				}
			} else {
				if(YFCObject.isNull(sDepartmentCode)){
				sDepartmentCode = "ISGM";
			}
				}

		}

		inxml.getDocumentElement().setAttribute(
				TelstraConstants.DEPARTMENT_CODE, sDepartmentCode);
		inxml.getDocumentElement().setAttribute(TelstraConstants.DOCUMENT_TYPE,
				sDocumentType);
		inxml.getDocumentElement().setAttribute(
				TelstraConstants.RECEIVING_NODE, sReceivingNode);
		inxml.getDocumentElement().removeAttribute(
				TelstraConstants.INTERFACE_NO);
		
		if(YFCObject.equals(sDocumentType, TelstraConstants.DOCUMENT_TYPE_0001)){
			if(!YFCObject.equals(sSellerOrganizationCode,"TELSTRA") && !YFCObject.equals(sSellerOrganizationCode,"NBN")){
				inxml.getDocumentElement().setAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE,
						"TELSTRA");
				String sShipNode=sSellerOrganizationCode+"_N1";
				for (YFCElement eleInputOrderLine : inxml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					eleInputOrderLine.setAttribute(TelstraConstants.SHIP_NODE,
							sShipNode);
				}
			}
		}
		
		if(YFCObject.equals(sDocumentType,"0005")){
			if(!YFCObject.equals(sSellerOrganizationCode,"TELSTRA") && !YFCObject.equals(sSellerOrganizationCode,"NBN")){
				String sShipNode=sSellerOrganizationCode+"_N1";
				for (YFCElement eleInputOrderLine : inxml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					eleInputOrderLine.setAttribute(TelstraConstants.SHIP_NODE,
							sShipNode);

				}
			}
		}
				

		YFCDocument doctempOrderListinXml = YFCDocument
				.getDocumentFor("<Order EnterpriseCode='' DocumentType='' OrderHeaderKey=''><OrderLines><OrderLine Status='' OrderHeaderKey='' OrderLineKey='' PrimeLineNo=''></OrderLine></OrderLines></Order>");
		YFCDocument outxml = invokeYantraApi("createOrder", inxml,
				doctempOrderListinXml); // template
		checkforBackorder(outxml, inxml);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),
				"CreateOrder", "");
	}

	/**
	 * 
	 * @return
	 */
	private String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * 
	 * @param eleTempOrderLine
	 * @param sDocumentType
	 */
	// BackOrder for Created order
	private void backOrderLineforCreated(YFCElement eleTempOrderLine,
			String sDocumentType) {
		YFCDocument docchangeOrderinXml = YFCDocument
				.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
						+ eleTempOrderLine.getAttribute(
								TelstraConstants.ORDER_HEADER_KEY, "")
						+ "' TransactionId='SCHEDULE."
						+ sDocumentType
						+ "'><OrderLines><OrderLine BaseDropStatus='1300' ChangeForAllAvailableQty='Y' OrderLineKey='"
						+ eleTempOrderLine.getAttribute(
								TelstraConstants.ORDER_LINE_KEY, "")
						+ "'/></OrderLines></Order>");
		invokeYantraApi("changeOrderStatus", docchangeOrderinXml);
	}

	// Checking for BackOrdered Lines
	private void checkforBackorder(YFCDocument outxml, YFCDocument inxml) {
		String sOrderHeaderKey = outxml.getDocumentElement().getAttribute(
				TelstraConstants.ORDER_HEADER_KEY, "");
		String sDocumentType = outxml.getDocumentElement().getAttribute(
				TelstraConstants.DOCUMENT_TYPE, "");

		if (!YFCObject.isVoid(sOrderHeaderKey)) {
			YFCElement eleOrderOut = outxml.getElementsByTagName(
					TelstraConstants.ORDER).item(0);
			if (!YFCObject.isVoid(eleOrderOut)) {
				for (YFCElement eleInputOrderLine : inxml
						.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					String sPrimeLineNo = eleInputOrderLine.getAttribute(
							TelstraConstants.PRIME_LINE_NO, "");
					YFCElement eleTempOrderLine = null;
					for (YFCElement eleOrderListLine : eleOrderOut
							.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
						String sOrderListPrimeLineNo = eleOrderListLine
								.getAttribute(TelstraConstants.PRIME_LINE_NO,
										"");
						if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
							eleTempOrderLine = eleOrderListLine;
							break;
						}
					}
					if (!YFCObject.isNull(eleTempOrderLine)) {
						String sOrderLineStatus = eleInputOrderLine
								.getAttribute(TelstraConstants.CHANGE_STATUS,
										"");
						if (!YFCObject.isNull(sOrderLineStatus)) {
							String sBackorderstatus = getProperty(
									"ChangeStatusBackorder", true); // "B"
							if (sOrderLineStatus
									.equalsIgnoreCase(sBackorderstatus)) {
								backOrderLineforCreated(eleTempOrderLine,
										sDocumentType);
							}
						}

					}
				}
			}
		}
	}
}