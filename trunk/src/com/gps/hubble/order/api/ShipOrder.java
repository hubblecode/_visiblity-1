package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for WMS status update for Ship (Integral+ and Vector Orders)
 * 
 * @author Karthikeyan Suruliappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class ShipOrder extends AbstractCustomApi {
  private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
  private boolean soFlag = true;
  private boolean callApiFlg = false;
  private String orderHeaderKey = "";
  private String shipmentKey = "";
  private boolean backOrdFlg = false;
  private boolean vOrdFlg = false;
  private Set < String > ordLineRKSet = null;
  private HashMap < String, ArrayList < String >> ordRelsKeyToOrdLineNoMap = null;
  private HashMap < String, YFCDocument > shipMap = null;
  private ArrayList < String > eligibleOrdRelKeyList = null;
  private YFCDocument orderReleaseDoc = null;
  private HashMap < String,String > confirmShipAdditionalAtrMap = null;
  

  public YFCDocument invoke(YFCDocument inXml) throws YFSException {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    YFCElement inEle = inXml.getDocumentElement();
    validateInputXml(inEle);
    YFCDocument outputGetOrdListForComQry = getOrderList(inEle);
    uniqueOrderValidation(outputGetOrdListForComQry,inEle);
    formRelKeySet(outputGetOrdListForComQry, inXml);
    if (backOrdFlg) {
      scheduleOrder();
      outputGetOrdListForComQry = getOrderList(inEle);
      mapOrdReleaseKeyAndOrdLineNo(outputGetOrdListForComQry);
      callChangeRelease(inXml);
      LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
      return inXml;
    }
    getShipmentLineList(inXml);
    if (vOrdFlg) {
      scheduleOrder();
      outputGetOrdListForComQry = getOrderList(inEle);
      mapOrdReleaseKeyAndOrdLineNo(outputGetOrdListForComQry);
      callChangeRelease(inXml);
      createShipment();
      confirmShipment();
    } else {
      confirmShipment();
    }
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    return inXml;
  }

  /*This method is to validate the input xml for required attributies.*/
  private void validateInputXml(YFCElement inEle) {
    String ordName = inEle.getAttribute("OrderName", "");
    boolean validInputFlg = true;
    YFCNodeList < YFCElement > yfcLineList = inEle.getElementsByTagName(getProperty("LineName"));
    for (YFCElement yfcElement: yfcLineList) {
      String plNo = yfcElement.getAttribute("PrimeLineNo", "");
      String itemID = yfcElement.getAttribute("ItemID", "");
      String action = yfcElement.getAttribute("Action", "");
      String sQty = yfcElement.getAttribute(TelstraConstants.STATUS_QUANTITY, "");
      if (XmlUtils.isVoid(ordName) || XmlUtils.isVoid(plNo)) {
        validInputFlg = false;
        break;
      }
      if(XmlUtils.isVoid(itemID) || XmlUtils.isVoid(action) || XmlUtils.isVoid(sQty)){
        validInputFlg = false;
        break;
      }
      if(sQty.indexOf('.') >0){
        String[] wholeSQty = sQty.split(".");
        yfcElement.setAttribute("StatusQuantity", wholeSQty[0]);
      }
    }
    if (!validInputFlg || (yfcLineList.getLength() == 0)) {
      LoggerUtil.verboseLog("CEV message validation failure", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("CevMsgValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    
    formAdditionalConfirmShipAtrMap(inEle);
  }

  /*This method is to form a map for additional attributes that are part of input CEV message */
  private void formAdditionalConfirmShipAtrMap(YFCElement inEle) {
    confirmShipAdditionalAtrMap = new HashMap<>();
    String actualShipDate = inEle.getAttribute("ActualShipmentDate");
    if(!XmlUtils.isVoid(actualShipDate)){
      confirmShipAdditionalAtrMap.put("ActualShipmentDate", actualShipDate);
    }    
    String carrierServiceCode = inEle.getAttribute("CarrierServiceCode");
    if(!XmlUtils.isVoid(carrierServiceCode)){
      confirmShipAdditionalAtrMap.put("CarrierServiceCode", carrierServiceCode);
    }
    String scac = inEle.getAttribute("SCAC");
    if(!XmlUtils.isVoid(scac)){
      confirmShipAdditionalAtrMap.put("SCAC", scac);
    }
    String trackingNo = inEle.getAttribute("TrackingNo");
    if(!XmlUtils.isVoid(trackingNo)){
      confirmShipAdditionalAtrMap.put("TrackingNo", trackingNo);
    }
    
  }

  /*This method is to get shipment line list for the picked shipments as this gives associated Order release keys as well setting the vector order flag if not shipments there for an order.*/
  private void getShipmentLineList(YFCDocument inXml) {
    YFCDocument inApiDoc = YFCDocument.getDocumentFor("<ShipmentLine OrderHeaderKey='" + getOrderHeaderKey() + "'><Shipment Status='1200.10000'/></ShipmentLine>");
    YFCDocument outputShipLnLst = invokeYantraApi("getShipmentLineList", inApiDoc);
    formShipMap(outputShipLnLst);
    if (vOrdFlg) {
      return;
    }
    findCorrectShipment(inXml);
  }

  /*This method  is to determine the existing shipment according to the input CEV message, based on the unique Order release key set.*/
  @SuppressWarnings("rawtypes")
  private void findCorrectShipment(YFCDocument inXml) {
    if (shipMap != null) {
      Set entrySet = shipMap.entrySet();
      Iterator it = entrySet.iterator();
      YFCNodeList < YFCNode > shipEleListInMsg = XPathUtil.getXpathNodeList(inXml, "//ShipmentLine[@StatusQuantity!='0']");
      int lineSizeInMsg = shipEleListInMsg.getLength();
      while (it.hasNext()) {
        Map.Entry curMapEntry = (Map.Entry) it.next();
        YFCDocument shipDoc = (YFCDocument) curMapEntry.getValue();
        YFCElement shipEle = shipDoc.getDocumentElement();
        String shipKey = (String) curMapEntry.getKey();
        YFCNodeList < YFCElement > shipLineList = shipEle.getElementsByTagName("ShipmentLine");
        int lineSizeInShip = shipLineList.getLength();
        if (lineSizeInShip == lineSizeInMsg) {
          Set < YFCElement > uniqueEleSet = null;
          for (YFCElement yfcElement: shipLineList) {
            if (uniqueEleSet == null) {
              uniqueEleSet = new HashSet < YFCElement > ();
            }
            for (YFCNode yfcNode: shipEleListInMsg) {
              YFCElement yfcEle = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
              YFCElement tEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcElement.toString()), "//ShipmentLine[@ItemID='" + yfcEle.getAttribute("ItemID") + "' and @Quantity='" + yfcEle.getAttribute("StatusQuantity", "0") + ".00']");
              if (!XmlUtils.isVoid(tEle)) {
                uniqueEleSet.add(tEle);
              }
            }
            if (uniqueEleSet.size() == lineSizeInMsg && isShipmentReleaseKeysMatching(shipLineList)) {
              setShipmentKey(shipKey);
              return;
            }
          }
        }
      }
    }
  }


  /*This method is to determine shipment line order release key is mapping with incoming order release key or not.*/
  private boolean isShipmentReleaseKeysMatching(YFCNodeList < YFCElement > shipEle) {
    int i = 0;
    for (YFCElement yfcElement: shipEle) {
      String sOrdRelKey = yfcElement.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
      if (ordLineRKSet.contains(sOrdRelKey)) {
        i++;
      }
    }
    if (shipEle.getLength() == i) {
      return true;
    }
    return false;
  }

  /*get shipment line list output will be grouped according the shipment key.*/
  private void formShipMap(YFCDocument outputShipLnLst) {
    YFCNodeList < YFCElement > shipLineEleList = outputShipLnLst.getDocumentElement().getElementsByTagName("ShipmentLine");
    if (shipLineEleList.getLength() == 0) {
      vOrdFlg = true;
      return;
    }
    for (YFCElement yfcElement: shipLineEleList) {
      String sKey = yfcElement.getAttribute("ShipmentKey");

      if (shipMap == null) {
        shipMap = new HashMap < String, YFCDocument > ();
      }
      YFCDocument tD = shipMap.get(sKey);
      if (XmlUtils.isVoid(tD)) {
        tD = YFCDocument.getDocumentFor("<Shipment/>");
      }

      YFCDocument tO = tD.getDocumentElement().getOwnerDocument();
      YFCElement sl = tO.importNode(yfcElement, true);
      tD.getDocumentElement().appendChild(sl);
      if (shipMap.get(sKey) == null) {
        shipMap.put(sKey, tD);
      }
    }
  }

  /*This method is to determine the unique release keys of order lines according to the input xml passed. As well we are determining , need to back order or not.*/
  private void formRelKeySet(YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
    YFCNodeList < YFCNode > ordLineNodeList = XPathUtil.getXpathNodeList(inXml, "//ShipmentLine[@StatusQuantity!='0']");
    if (ordLineNodeList.getLength() == 0) {
      backOrdFlg = true;
      return;
    }
    for (YFCNode yfcNode: ordLineNodeList) {
      YFCElement yfcElement = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
      String plNo = yfcElement.getAttribute("PrimeLineNo");
      String slNo = yfcElement.getAttribute("SubLineNo", "1");
      YFCElement matchingEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + plNo + "' and @SubLineNo='" + slNo + "']");
      if (!XmlUtils.isVoid(matchingEle)) {
        String relKey = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(matchingEle.toString()), "//OrderStatuses/OrderStatus[@Status='" + getProperty("OrderPickedStatus") + "' and @StatusQty='" + yfcElement.getAttribute("StatusQuantity") + ".00']/@OrderReleaseKey");
        if (!XmlUtils.isVoid(relKey)) {
          if (ordLineRKSet == null) {
            ordLineRKSet = new HashSet < String > ();
          }
          ordLineRKSet.add(relKey);
        }
      }
    }
  }

  /*Change released will be called for the input lines according to the Order release key map*/
  @SuppressWarnings({
    "unchecked",
    "rawtypes"
  })
  void callChangeRelease(YFCDocument inXml) {
    if (ordRelsKeyToOrdLineNoMap != null) {
      Set entrySet = ordRelsKeyToOrdLineNoMap.entrySet();
      Iterator it = entrySet.iterator();
      while (it.hasNext()) {
        Map.Entry curMapEntry = (Map.Entry) it.next();
        ArrayList < String > lnList = (ArrayList < String > ) curMapEntry.getValue();
        String relsKey = (String) curMapEntry.getKey();
        orderReleaseDoc = YFCDocument.getDocumentFor("<OrderRelease><OrderLines/></OrderRelease>");
        YFCElement ordRelsEle = orderReleaseDoc.getDocumentElement();
        ordRelsEle.setAttribute(TelstraConstants.ORDER_RELEASE_KEY, relsKey);
        YFCElement ordLnsEle = ordRelsEle.getChildElement("OrderLines");
        for (String string: lnList) {
          YFCElement matchingOrdLineEle = XPathUtil.getXPathElement(inXml, "//ShipmentLines/ShipmentLine[@PrimeLineNo='" + string + "']");
          if (!XmlUtils.isVoid(matchingOrdLineEle)) {
            YFCElement tmpEle = YFCDocument.getDocumentFor("<OrderLine/>").getDocumentElement();
            Map < String, String > shipLineAtrMap = matchingOrdLineEle.getAttributes();
            tmpEle.setAttributes(shipLineAtrMap);
            YFCDocument oRelDoc = ordLnsEle.getOwnerDocument();
            YFCElement cevOrdLineEleTemp = oRelDoc.importNode(tmpEle, true);
            ordLnsEle.appendChild(cevOrdLineEleTemp);
            callApiFlg = true;
          }
        }
        if (callApiFlg) {
          if (XmlUtils.isVoid(eligibleOrdRelKeyList)) {
            eligibleOrdRelKeyList = new ArrayList();
          }
          eligibleOrdRelKeyList.add(relsKey);
          releaseChange();
        }
      }
    }
  }

  /*Order lines associated to an order release key map is formed here.*/
  void mapOrdReleaseKeyAndOrdLineNo(YFCDocument outputGetOrdListForComQry) {
    if (ordRelsKeyToOrdLineNoMap == null) {
      ordRelsKeyToOrdLineNoMap = new HashMap < String, ArrayList < String >> ();
    }
    YFCNodeList < YFCElement > ordLineList = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine");
    for (YFCElement yfcElement: ordLineList) {
      YFCElement ordStsEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcElement.toString()), "//OrderStatuses/OrderStatus[@Status='" + getProperty("OrderReleaseStatus") + "' and @StatusQty!='0']");
      if (!XmlUtils.isVoid(ordStsEle)) {
        String ordRelsKey = ordStsEle.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
        String ordLnNo = yfcElement.getAttribute("PrimeLineNo");
        if (ordRelsKeyToOrdLineNoMap.containsKey(ordRelsKey)) {
          ArrayList < String > tempArrLst = ordRelsKeyToOrdLineNoMap.get(ordRelsKey);
          tempArrLst.add(ordLnNo);
        } else {
          ArrayList < String > tempArrLst = new ArrayList < String > ();
          tempArrLst.add(ordLnNo);
          ordRelsKeyToOrdLineNoMap.put(ordRelsKey, tempArrLst);
        }
      }
    }
  }

  /* Shipment will be confirmed else exception for already confirmed shipment will be thrown */
  private void confirmShipment() {
    if(XmlUtils.isVoid(getShipmentKey())){
      LoggerUtil.verboseLog("Shipment validation failure ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("ShipmentNotFound");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    YFCDocument inputApiDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + getShipmentKey() + "' />");
    if(confirmShipAdditionalAtrMap.size() != 0){
    addtionalDetails(inputApiDoc);
    }
    YFCDocument outputGetShipmentList = invokeYantraApi("getShipmentList", inputApiDoc,YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status='' /></Shipments>"));
    if(getProperty("ShipmentStatus") == XPathUtil.getXpathAttribute(outputGetShipmentList, "//Shipment/@Status")){
      LoggerUtil.verboseLog("Shipment confirm validation failure ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("ConfirmShipmentStatus");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    invokeYantraApi("confirmShipment", inputApiDoc);
  }

  /*set the additional shipment info(s) which are got form input CEV message to the confirm shipment input*/
  private void addtionalDetails(YFCDocument inputApiDoc) {
  inputApiDoc.getDocumentElement().setAttributes(confirmShipAdditionalAtrMap);    
  }

  /*This method to create shipment for the input lines according to the order release key set.*/
  private void createShipment() {
    YFCDocument inDocCreateShipment = YFCDocument.getDocumentFor("<Shipment  OrderHeaderKey='"+getOrderHeaderKey()+"'>" +
        "<OrderReleases/>" +
        "</Shipment> ");
    YFCElement docEle = inDocCreateShipment.getDocumentElement().getChildElement("OrderReleases");
    for (String ordRelKey: eligibleOrdRelKeyList) {
      docEle.createChild("OrderRelease").setAttribute("OrderReleaseKey", ordRelKey);
    }
    YFCDocument outputApiDoc = invokeYantraApi("createShipment", inDocCreateShipment, "<Shipment ShipmentKey='' DocumentType='' />");
    setShipmentKey(XPathUtil.getXpathAttribute(outputApiDoc, "//@ShipmentKey"));
  }

  YFCDocument getOrderList(YFCElement inEle) {
    YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order>" + 
        "<ComplexQuery Operator='OR'><And><Or>" + 
        "<Exp Name='DocumentType' QryType='EQ' Value='0001'/>" + 
        "<Exp Name='DocumentType' Value='0006' QryType='EQ'/></Or>" +
        "<Exp Name='OrderName' Value='" + inEle.getAttribute("OrderName") + "' QryType='EQ'/>" + "</And>" + "</ComplexQuery>" + 
        "</Order>");
    YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
        "<Order DocumentType='' OrderHeaderKey='' HoldFlag='' >" +
        "<OrderLines>" +
        "<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' > " +
        "<OrderStatuses>" +
        "<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
        "</OrderStatuses> " +
        "</OrderLine>" +
        "</OrderLines>" +
        "</Order>" +
        "</OrderList>");
    YFCDocument outputGetOrdListForComQry = invokeYantraApi("getOrderList", inDocgetOrdList, templateForGetOrdList);
    if ("Y".equals(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@HoldFlag"))) {
      LoggerUtil.verboseLog("Order Hold Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("OrderHoldValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    return outputGetOrdListForComQry;
  }

  /*Change release for the input lines according to the release key map generated*/
  void releaseChange() {
    YFCDocument outTempForChangeRelease = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='' DocumentType='' EnterpriseCode='' OrderReleaseKey='' ReleaseNo=''/>");
    invokeYantraApi("changeRelease", orderReleaseDoc, outTempForChangeRelease);
  }

  /*Schedule the order with ScheduleAndRelease 'Y' and CheckInventory 'N'  set*/
  private void scheduleOrder() {
    String ordHdrKey = getOrderHeaderKey();
    YFCDocument inDocForSchOrd = YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' CheckInventory='N' OrderHeaderKey='" + ordHdrKey + "' />");
    invokeYantraApi("scheduleOrder", inDocForSchOrd);
  }

  /*This method to ensure whether orderNo is unique across SO and TO */
  void uniqueOrderValidation(YFCDocument outputGetOrdListForComQry,YFCElement inEle) {
    String totalOrderList = outputGetOrdListForComQry.getDocumentElement().getAttribute("TotalOrderList", "0");
    String docType = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@DocumentType");
    setOrderHeaderKey(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey"));
    int ordLineCount = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine").getLength();
    int inMsgNoOfLines = inEle.getElementsByTagName("ShipmentLine").getLength();
    if(ordLineCount!=inMsgNoOfLines){
      LoggerUtil.verboseLog("Number of orderlines between existing order and CEV message are not matching.  ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("OrderLineValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    if ("0006".equals(docType)) {
      setSOFlag(false);
    }
    if ("1".equals(totalOrderList)) {
      return;
    }
    LoggerUtil.verboseLog("Order Name Duplicate Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
    String strErrorCode = getProperty("DuplicateOrderName");
    YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
    throw new YFSException(erroDoc.toString());
  }

  public boolean isSOFlag() {
    return soFlag;
  }

  public void setSOFlag(boolean sOFlag) {
    soFlag = sOFlag;
  }

  public String getOrderHeaderKey() {
    return orderHeaderKey;
  }

  public void setOrderHeaderKey(String orderHeaderKEY) {
    orderHeaderKey = orderHeaderKEY;
  }

  public String getShipmentKey() {
    return shipmentKey;
  }

  public void setShipmentKey(String shipmentKey) {
    this.shipmentKey = shipmentKey;
  }
}