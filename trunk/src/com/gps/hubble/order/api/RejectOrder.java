package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;


/**
 * Custom API for WMS status update for rejection of an order (Integral Orders Only)
 * 
 * @author Karthikeyan Suruliappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class RejectOrder extends AbstractCustomApi  {
  private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
  PickOrder pObj = new PickOrder();
  @Override
  public YFCDocument invoke(YFCDocument inXml) throws YFSException {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);   
    YFCElement inEle = inXml.getDocumentElement();
    YFCDocument outputGetOrdListForComQry = pObj.getOrderList(inEle);
    pObj.validateInputXml(inEle);
    pObj.uniqueOrderValidation(outputGetOrdListForComQry,inEle);     
    if(!pObj.isSOFlag()){
      pObj.setBaseDropStatus("9000.10000");
      pObj.setTranID("GPS_TO_REJ_BY_DC.0006.ex");
    }else{
      pObj.setBaseDropStatus("9000.10000");
      pObj.setTranID("GPS_REL_TO_REJ.0001.ex");
    }
    pObj.mapOrdReleaseKeyAndOrdLineNo(outputGetOrdListForComQry);
    pObj.callChangeRelease(inXml);   
    rejectOrder(outputGetOrdListForComQry);
    return inXml;
  }

  private void rejectOrder(YFCDocument outputGetOrdListForComQry) {
    String ordHdrKey = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey");
    YFCDocument apiInDoc = YFCDocument.getDocumentFor("<OrderStatusChange OrderHeaderKey='"+ordHdrKey+"' ChangeForAllAvailableQty='Y' TransactionId='"+pObj.getTranID()+"' " +
        "BaseDropStatus='"+pObj.getBaseDropStatus()+"'/>");
    invokeYantraApi("changeOrderStatus", apiInDoc);
  }
}
