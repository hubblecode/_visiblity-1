package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for WMS status update for Pick (Integral Orders Only)
 * 
 * @author Karthikeyan Suruliappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class PickOrder extends AbstractCustomApi  {
  private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
  private boolean soFlag=true;
  private boolean callApiFlg = false;
  private String baseDropStatus="";
  private String tranID="";
  private String orderHeaderKey = "";
  private String shipmentKey = "";
  private HashMap<String, ArrayList<String>> ordRelsKeyToOrdLineNoMap=null; 
  private ArrayList<String> eligibleOrdRelKeyList = null;
  private YFCDocument orderReleaseDoc = null;

  public YFCDocument invoke(YFCDocument inXml) throws YFSException{
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    YFCElement inEle = inXml.getDocumentElement();    
    validateInputXml(inEle);
    YFCDocument outputGetOrdListForComQry = getOrderList(inEle);
    uniqueOrderValidation(outputGetOrdListForComQry,inEle);
    if(!isSOFlag()){
      setBaseDropStatus(getProperty("TOPickBaseDropStatus"));
      setTranID(getProperty("TOPickTranID"));
    }else{
      setBaseDropStatus(getProperty("SOPickBaseDropStatus"));
      setTranID(getProperty("SOPickTranID"));
    }
    boolean bkOrdFlg = backOrder(inXml);
    scheduleOrder();
    outputGetOrdListForComQry = getOrderList(inEle);
    mapOrdReleaseKeyAndOrdLineNo(outputGetOrdListForComQry);
    callChangeRelease(inXml); 
    if(callApiFlg && !bkOrdFlg){
      createShipment();
      changeShimentStatus();
    }
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    return inXml;
  }

  /*This method is to validate the input xml for required attributies.*/
  void validateInputXml(YFCElement inEle) {
    String ordName = inEle.getAttribute("OrderName","");
    boolean validInputFlg = true;
    YFCNodeList<YFCElement> yfcLineList = inEle.getElementsByTagName(TelstraConstants.ORDER_LINE);    
    for (YFCElement yfcElement : yfcLineList) {
      String plNo = yfcElement.getAttribute("PrimeLineNo","");
      String itemID = yfcElement.getAttribute("ItemID","");
      String action = yfcElement.getAttribute("Action","");
      String sQty = yfcElement.getAttribute("StatusQuantity","");
      if(XmlUtils.isVoid(ordName) || XmlUtils.isVoid(plNo)){
        validInputFlg = false;
        break;
      }
      if(XmlUtils.isVoid(itemID) || XmlUtils.isVoid(action) || XmlUtils.isVoid(sQty)){
        validInputFlg = false;
        break;
      }
      if(sQty.indexOf('.')>0){
        String[] wholeSQty = sQty.split(".");
        yfcElement.setAttribute("StatusQuantity", wholeSQty[0]);
      }
    }    
    if(!validInputFlg || (yfcLineList.getLength() == 0)){
      LoggerUtil.verboseLog("CEV message validation failure ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("CevMsgValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
  }

  /*If all the CEV lines having zero qty ,Order needs to be back ordered.*/
  private boolean backOrder(YFCDocument inXml) {
    YFCNodeList<YFCNode> ordLineNodeList = XPathUtil.getXpathNodeList(inXml, "//OrderLine[@StatusQuantity!='0']");
    if(ordLineNodeList.getLength()==0){
      return true;
    }
    return false;
  }


  /*Change released will be called for the input lines according to the Order release key map*/
  @SuppressWarnings({"unchecked", "rawtypes"})
  void callChangeRelease(YFCDocument inXml) {
    if(ordRelsKeyToOrdLineNoMap != null){
      Set entrySet = ordRelsKeyToOrdLineNoMap.entrySet();
      Iterator it = entrySet.iterator();      
      while(it.hasNext()){
        orderReleaseDoc=YFCDocument.getDocumentFor("<OrderRelease><OrderLines/></OrderRelease>");
        Map.Entry curMapEntry = (Map.Entry)it.next();
        ArrayList<String> lnList = (ArrayList<String>) curMapEntry.getValue();
        String relsKey = (String) curMapEntry.getKey();
        YFCElement ordRelsEle = orderReleaseDoc.getDocumentElement();
        ordRelsEle.setAttribute("OrderReleaseKey",relsKey);
        YFCElement ordLnsEle = ordRelsEle.getChildElement("OrderLines");        
        for (String string : lnList) { 
          YFCElement matchingOrdLineEle = XPathUtil.getXPathElement(inXml, "//OrderLines/OrderLine[@PrimeLineNo='"+string+"']");          
          if(!XmlUtils.isVoid(matchingOrdLineEle)){
            YFCDocument oRelDoc = ordLnsEle.getOwnerDocument();
            YFCElement cevOrdLineEleTemp = oRelDoc.importNode(matchingOrdLineEle,true);
            ordLnsEle.appendChild(cevOrdLineEleTemp);
            callApiFlg = true;
          }
        }
        if(callApiFlg)
        {
          if(XmlUtils.isVoid(eligibleOrdRelKeyList))
          {
            eligibleOrdRelKeyList = new ArrayList();
          }
          eligibleOrdRelKeyList.add(relsKey);
          releaseChange();
        }
      }
    }
  }

  /*Input lines associated order line number list will be formed and the same will be mapped to order release key*/
  void mapOrdReleaseKeyAndOrdLineNo(YFCDocument outputGetOrdListForComQry) {
    if(ordRelsKeyToOrdLineNoMap == null){
      ordRelsKeyToOrdLineNoMap = new HashMap<String, ArrayList<String>>();
    }
    YFCNodeList<YFCElement> ordLineList = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine");
    for (YFCElement yfcElement : ordLineList) {
      YFCElement ordStsEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcElement.toString()),"//OrderStatuses/OrderStatus[@Status='" + getProperty("OrderReleaseStatus") + "' and @StatusQty!='0']");
      if(!XmlUtils.isVoid(ordStsEle)){
        String ordRelsKey = ordStsEle.getAttribute("OrderReleaseKey");
        String ordLnNo = yfcElement.getAttribute("PrimeLineNo");
        if(ordRelsKeyToOrdLineNoMap.containsKey(ordRelsKey)){
          ArrayList<String> tempArrLst = ordRelsKeyToOrdLineNoMap.get(ordRelsKey);
          tempArrLst.add(ordLnNo);
        }else{
          ArrayList<String> tempArrLst = new ArrayList<String>();
          tempArrLst.add(ordLnNo);
          ordRelsKeyToOrdLineNoMap.put(ordRelsKey, tempArrLst);
        }      
      }
    }
  }
  
  /*This method to drop the shipment to picked status.*/
  private void changeShimentStatus() {
    YFCDocument inputApiDoc = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='"+getBaseDropStatus()+"' " +
        "ShipmentKey='"+getShipmentKey()+"' " +
        "TransactionId='"+getTranID()+"' />");
    invokeYantraApi("changeShipmentStatus", inputApiDoc);
  }

  /*This method to create shipment for the input lines according to the order release key set.*/
  private void createShipment() {
    YFCDocument inDocCreateShipment = YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+getOrderHeaderKey()+"'>" +
        "<OrderReleases/>" +
        "</Shipment> ");    
    YFCElement docEle = inDocCreateShipment.getDocumentElement().getChildElement("OrderReleases");
    for (String ordRelKey : eligibleOrdRelKeyList) {
      docEle.createChild("OrderRelease").setAttribute("OrderReleaseKey", ordRelKey);
    }
    YFCDocument outputApiDoc = invokeYantraApi("createShipment", inDocCreateShipment, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' DocumentType='' />"));
    setShipmentKey(XPathUtil.getXpathAttribute(outputApiDoc, "//@ShipmentKey"));
  }

  /*Get the order list for the input message order name*/
  YFCDocument getOrderList(YFCElement inEle) {
    YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order>"
        + "<ComplexQuery Operator='OR'><And><Or>"
        + "<Exp Name='DocumentType' QryType='EQ' Value='0001'/>"
        + "<Exp Name='DocumentType' Value='0006' QryType='EQ'/></Or>"
        + "<Exp Name='OrderName' Value='"+inEle.getAttribute("OrderName")+"' QryType='EQ'/>"
        + "</And>"
        + "</ComplexQuery>"
        + "</Order>");
    YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
        "<Order DocumentType='' OrderHeaderKey='' HoldFlag='' >" +
        "<OrderLines>" +
        "<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' > " +
        "<OrderStatuses>" +
        "<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
        "</OrderStatuses> " +
        "</OrderLine>" +
        "</OrderLines>" +
        "</Order>" +
        "</OrderList>");
    YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);    
    if("Y".equals(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@HoldFlag"))){
      LoggerUtil.verboseLog("Order Hold Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("OrderHoldValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString()); 
    }
    return outputGetOrdListForComQry;
  }

  /*Change release for the input lines according to the release key map generated*/
  void releaseChange() {
    YFCDocument outTempForChangeRelease = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='' DocumentType='' EnterpriseCode='' OrderReleaseKey='' ReleaseNo=''/>");
    invokeYantraApi("changeRelease",orderReleaseDoc,outTempForChangeRelease);
  }

  /*Schedule the order with ScheduleAndRelease 'Y' and CheckInventory 'N'  set*/
  private void scheduleOrder() {
    String ordHdrKey = getOrderHeaderKey();
    YFCDocument inDocForSchOrd = YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' CheckInventory='N' OrderHeaderKey='"+ordHdrKey+"' />");
    invokeYantraApi("scheduleOrder", inDocForSchOrd);
  }

  /* This method to ensure whether orderNo is unique across SO and TO */
  void uniqueOrderValidation(YFCDocument outputGetOrdListForComQry,YFCElement inEle) {
    String totalOrderList=outputGetOrdListForComQry.getDocumentElement().getAttribute("TotalOrderList","0");
    String docType =  XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@DocumentType");
    setOrderHeaderKey(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey"));
    int ordLineCount = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine").getLength();
    int inMsgNoOfLines = inEle.getElementsByTagName("OrderLine").getLength();
    if(ordLineCount!=inMsgNoOfLines){
      LoggerUtil.verboseLog("Number of orderlines between existing order and CEV message are not matching.  ", logger, TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("OrderLineValidation");
      YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
      throw new YFSException(erroDoc.toString());
    }
    if("0006".equals(docType)){
      setSOFlag(false);
    }
    if("1".equals(totalOrderList)){
      return;
    }
    LoggerUtil.verboseLog("Order Name Duplicate Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
    String strErrorCode = getProperty("DuplicateOrderName");
    YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
    throw new YFSException(erroDoc.toString());
  }

  public boolean isSOFlag() {
    return soFlag;
  }

  public void setSOFlag(boolean sOFlag) {
    soFlag = sOFlag;
  }

  public String getBaseDropStatus() {
    return baseDropStatus;
  }

  public void setBaseDropStatus(String sOBaseDropStatus) {
    baseDropStatus = sOBaseDropStatus;
  }

  public String getTranID() {
    return tranID;
  }

  public void setTranID(String tRanID) {
    tranID = tRanID;
  }

  public String getOrderHeaderKey() {
    return orderHeaderKey;
  }

  public void setOrderHeaderKey(String orderHeaderKEY) {
    orderHeaderKey = orderHeaderKEY;
  }

  public String getShipmentKey() {
    return shipmentKey;
  }

  public void setShipmentKey(String shipmentKey) {
    this.shipmentKey = shipmentKey;
  }
}
