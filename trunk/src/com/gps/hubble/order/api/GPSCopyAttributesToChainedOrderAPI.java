package com.gps.hubble.order.api;

import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * This is a Javadoc comment
 */

public class GPSCopyAttributesToChainedOrderAPI extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(GPSCopyAttributesToChainedOrderAPI.class);
		
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {


		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyRequiredAttributes\n");	
		
		logger.debug("\nGPSCopyAttributesToChainedOrderAPI :: copyRequiredAttributes :: inputDoc\n" + inDoc);
		YFCElement eleInDocRoot = inDoc.getDocumentElement();
		if (!YFCCommon.isVoid(eleInDocRoot)) {
			YFCDocument docGetOrderListOp = callGetOrderListApi(inDoc);
			callChangeOrderApi(inDoc, docGetOrderListOp);
		}

		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyRequiredAttributes\n");
		return inDoc;
	
	}
	
	/**
	 * 
	 * @param inDoc
	 * @param docGetOrderListOp
	 * @param inDoc
	 * @throws Exception
	 */

	private void callChangeOrderApi(YFCDocument inDoc, YFCDocument docGetOrderListOp) {

		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: callChangeOrderApi\n");

		YFCDocument docChangeOrderIp = YFCDocument.createDocument(TelstraConstants.ORDER);
		YFCElement eleRootOrder = docChangeOrderIp.getDocumentElement();

		YFCElement eleChainedOrder = inDoc.getElementsByTagName(TelstraConstants.CHAINED_ORDER_LIST).item(0)
				.getChildElement(TelstraConstants.ORDER);

		eleRootOrder.setAttribute(TelstraConstants.ORDER_HEADER_KEY,
				eleChainedOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		eleRootOrder.setAttribute(TelstraConstants.BILL_TO_ID, docGetOrderListOp
				.getElementsByTagName(TelstraConstants.ORDER).item(0).getAttribute(TelstraConstants.BILL_TO_ID));
		eleRootOrder.setAttribute(TelstraConstants.SHIP_TO_ID, docGetOrderListOp
				.getElementsByTagName(TelstraConstants.ORDER).item(0).getAttribute(TelstraConstants.SHIP_TO_ID));
		eleRootOrder.setAttribute(TelstraConstants.OVERRIDE, TelstraConstants.YES);
		//
		YFCElement elePersonInfoShipToOrderList = docGetOrderListOp
				.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
		if (!YFCCommon.isVoid(elePersonInfoShipToOrderList)) {
			YFCElement elePersonInfoShipTo = docChangeOrderIp.importNode(elePersonInfoShipToOrderList, true);
			eleRootOrder.appendChild(elePersonInfoShipTo);
		}
		//
		YFCElement elePersonInfoBillToOrderList = docGetOrderListOp
				.getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);

		if (!YFCCommon.isVoid(elePersonInfoBillToOrderList)) {
			YFCElement elePersonInfoBillTo = docChangeOrderIp.importNode(elePersonInfoBillToOrderList, true);
			eleRootOrder.appendChild(elePersonInfoBillTo);			
		}
		//
		YFCElement eleAdditionalAddressesOrderList =  
				docGetOrderListOp.getElementsByTagName(TelstraConstants.ADDITIONAL_ADDRESSES).item(0);
		if(!YFCCommon.isVoid(eleAdditionalAddressesOrderList)){
			YFCElement eleAdditionalAddresses = docChangeOrderIp.importNode(eleAdditionalAddressesOrderList, true);
			eleRootOrder.appendChild(eleAdditionalAddresses);
		}
		copyShipNodeInOrderLine(inDoc,docChangeOrderIp, docGetOrderListOp);

		logger.debug("\nGPSCopyAttributesToChainedOrderAPI :: callChangeOrderApi :: docChangeOrderIp :- \n"
				+ docChangeOrderIp);

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, docChangeOrderIp);
		
		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: callChangeOrderApi\n");

	}

	/**
	 * This method copy ship node from sales order to chained PO.
	 * @param inDoc 
	 * 
	 * @param docChangeOrderIp
	 * @param docGetOrderListOp
	 */
	private void copyShipNodeInOrderLine(YFCDocument inDoc, YFCDocument docChangeOrderIp, YFCDocument docGetOrderListOp) {

		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyShipNodeInOrderLine\n");

		YFCElement eleOrderLinesChangeOrder = docChangeOrderIp.createElement(TelstraConstants.ORDER_LINES);
		docChangeOrderIp.getDocumentElement().appendChild(eleOrderLinesChangeOrder);

		YFCNodeList<YFCElement> nlOrderLinesGetOrderList = docGetOrderListOp
				.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement eleOrderLineGetOrderList : nlOrderLinesGetOrderList) {

			String sSOOrderLineKey = eleOrderLineGetOrderList.getAttribute(TelstraConstants.ORDER_LINE_KEY);
			Document domInDoc = inDoc.getDocument();
			Element eleOrderLinePO;
			try {
				eleOrderLinePO = (Element) XPathAPI.selectSingleNode(domInDoc, "//ChainedOrderList/Order/OrderLines/OrderLine[@ChainedFromOrderLineKey='"+sSOOrderLineKey+"']");
				if(!YFCCommon.isVoid(eleOrderLinePO)){					
					YFCElement eleOrderLineChangeOrder = docChangeOrderIp.createElement(TelstraConstants.ORDER_LINE);
					eleOrderLinesChangeOrder.appendChild(eleOrderLineChangeOrder);
					eleOrderLineChangeOrder.setAttribute(TelstraConstants.ORDER_LINE_KEY, eleOrderLinePO.getAttribute(TelstraConstants.ORDER_LINE_KEY));
					eleOrderLineChangeOrder.setAttribute(TelstraConstants.SHIP_NODE, eleOrderLineGetOrderList.getAttribute(TelstraConstants.SHIP_NODE));
				}
			} catch (TransformerException e) {
				logger.error("TransformerException :",e);
				YFCException ex = new YFCException(e.getMessage());
				ex.setStackTrace(e.getStackTrace());
				throw ex;
			}
		}
		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyShipNodeInOrderLine\n");
	}

	/**
	 * 
	 * @param inDoc
	 * @return
	 * @throws Exception
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private YFCDocument callGetOrderListApi(YFCDocument inDoc) {

		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: callGetOrderListApi\n");

		// get order list ip
		YFCDocument docGetOrderListIp = YFCDocument.createDocument(TelstraConstants.ORDER);
		String sSOOrderHeaderKey = inDoc.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0)
				.getAttribute(TelstraConstants.CHAINED_FROM_ORDER_HEADER_KEY);
		docGetOrderListIp.getDocumentElement().setAttribute(TelstraConstants.ORDER_HEADER_KEY, sSOOrderHeaderKey);
		logger.debug("\nGPSCopyAttributesToChainedOrderAPI :: callGetOrderListApi :: docGetOrderListIp :- \n"
				+ docGetOrderListIp);

		// get order list op
		YFCDocument docGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, docGetOrderListIp,
				TelstraConstants.GET_ORDER_LIST_FOR_COPY_ATTRI_API_TEMPLATE_PATH);

		logger.debug("\nGPSCopyAttributesToChainedOrderAPI :: callGetOrderListApi :: docGetOrderListOp :- \n"
				+ docGetOrderListOp);

		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: callGetOrderListApi\n");
		return docGetOrderListOp;
	}

}
