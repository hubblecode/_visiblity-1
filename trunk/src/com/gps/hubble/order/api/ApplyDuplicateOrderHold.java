package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API to create Hold for Duplicate Order
 * 
 * @author Murali Pachiappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */
public class ApplyDuplicateOrderHold extends AbstractCustomApi  {

  private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
  /**
   * Base method execution starts here
   * 
   * Method calls calls change order to put the order on hold
   * also creates alert for with order information
   */
  @Override
  public YFCDocument invoke(YFCDocument inXml) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    String orderHeaderKey = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY);
    if(!XmlUtils.isVoid(orderHeaderKey)) {
      changeOrderForHold(orderHeaderKey);
      generateAlertForHold(inXml);
    }
    return inXml;
  }
  
  /**
   * Method to create input XML for change Order and calls
   * changeOrderAPI with hold details
   * 
   * @param orderHeaderKey
   */
  
  private void changeOrderForHold(String  orderHeaderKey) {
    YFCDocument changeOrderXml = YFCDocument.getDocumentFor("<Order OrderHeaderKey=''><OrderHoldTypes>"
        + "<OrderHoldType HoldType='GPS_DUP_ORD_HOLD' ReasonText='Duplicate Order Check  Applied' Status='1100' />"
        + "</OrderHoldTypes></Order>");
    changeOrderXml.getDocumentElement().setAttribute(TelstraConstants.ORDER_HEADER_KEY, orderHeaderKey);
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeOrderForHold", changeOrderXml);
    invokeYantraApi("changeOrder", changeOrderXml);
  }

  /**
   * Method creates exception/alert so that hold on order
   * is notified by call in createException API
   * 
   * @param inXml
   */
  private void generateAlertForHold(YFCDocument inXml){
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "generateAlertForHold", inXml);
    YFCDocument inboxXml = YFCDocument.getDocumentFor("<Inbox ActiveFlag='Y' AutoResolvedFlag='N' Description='Duplicate Order Alert'    "
        + "DetailDescription='Duplicate Order Alert, as the order with same details exist' "
        + "ExceptionType='GPS_Duplicate_Order_Alert' FlowName='GpsDuplicateOrderApplyHold' "
        + "InboxType='GPD_Duplicate_Order_Alert ' OrderHeaderKey='' OrderNo='' QueueId=''>"
        + "<InboxReferencesList><InboxReferences Name='OriginalOrderNo' ReferenceType='TEXT' Value=''/></InboxReferencesList>"
        + "<Order DocumentType='0001' EnterpriseCode='' OrderHeaderKey='' OrderNo=''/></Inbox>");
    
    assignValueForDocument(inXml.getDocumentElement(),inboxXml.getDocumentElement());
    invokeYantraApi("createException", inboxXml);
    
  }
  
  /**
   * Method to assign order related information to input XML for alert
   * 
   * @param orderEle
   * @param inboxEle
   */
  private void assignValueForDocument(YFCElement orderEle,YFCElement inboxEle) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "assignValueForDocument", inboxEle);
    inboxEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY, orderEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
    inboxEle.setAttribute(TelstraConstants.ORDER_NO, orderEle.getAttribute(TelstraConstants.ORDER_NO));
    inboxEle.setAttribute(TelstraConstants.QUEUE_ID, "DEFAULT");
    String value = orderEle.getAttribute(TelstraConstants.GPS_ORIGINAL_ORDER_NO);
    inboxEle.getChildElement("InboxReferencesList").getChildElement("InboxReferences").setAttribute("Value", value);
    inboxEle.getChildElement(TelstraConstants.ORDER).setAttribute(TelstraConstants.DOCUMENT_TYPE, orderEle.getAttribute("DocumentType"));
    inboxEle.getChildElement(TelstraConstants.ORDER).setAttribute(TelstraConstants.ENTERPRISE_CODE, orderEle.getAttribute(TelstraConstants.ENTERPRISE_CODE));
    inboxEle.getChildElement(TelstraConstants.ORDER).setAttribute(TelstraConstants.ORDER_HEADER_KEY, 
        orderEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
    inboxEle.getChildElement(TelstraConstants.ORDER).setAttribute(TelstraConstants.ORDER_NO, 
        orderEle.getAttribute(TelstraConstants.ORDER_NO));
  }
}
