package com.gps.hubble.async.agent;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.bridge.sterling.consts.ExceptionLiterals;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.agent.server.YCPAbstractAgent;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom Agent classes for creating ItemAssociation. 
 * @author Murali Pachiappan
 * @version 1.0
 * 
 * Extends AbstractCustomBaseAgent class
 * 
 * Functionality
 * @see External methods
 */

public class AsyncRequestProcessorAgent extends YCPAbstractAgent  {
  private static YFCLogCategory logger = YFCLogCategory.instance(AsyncRequestProcessorAgent.class);
  
  /**
   * Method picks up message from queue thats was
   * dropped by getJobs and calls method changesAsyncReuest
   * method for execution of message to create ItemAssocation
   */
  @Override
  public void executeJob(YFSEnvironment env, Document arg1) {
    YFCDocument inXml = YFCDocument.getDocumentFor(arg1);
    YFCElement inEle = inXml.getDocumentElement().getChildElement("AsyncRequest");
    String queueId = inXml.getDocumentElement().getAttribute(TelstraConstants.QUEUE_ID);
    if(inEle != null) {
      try{
        changeAsyncRequest(inEle,queueId,env);
      }
      catch(Exception exp) {
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob", exp);
      }
    }
    
  }

 

    
    
  /**
   * Method to call flow/API based on the flag isFlow
   * with message from the table.
   * 
   * On success full execution call deleteAsyncRequest method
   * to deleted message from the table.
   * 
   * On Exception call updateErrorCount to update error count
   * @param inEle
   * @throws YIFClientCreationException 
   * @throws RemoteException 
   * @throws YFSException 
   * @throws Exception 
   */
  private void changeAsyncRequest(YFCElement inEle, String queueId,YFSEnvironment env) throws YFSException, RemoteException, YIFClientCreationException   {
    String message = inEle.getAttribute("Message");
    String apiFlowName = inEle.getAttribute("ServiceName");
    String isFlow = inEle.getAttribute("IsFlow");
    String asyncKey = inEle.getAttribute("AsyncronousRequestKey");
    int errorCount = inEle.getIntAttribute("ErrorCount");
    YFCDocument inXml = YFCDocument.getDocumentFor(message);
    try{
      if("Y".equalsIgnoreCase(isFlow)) {
        YIFClientFactory.getInstance().getApi()
        .executeFlow(env,apiFlowName, inXml.getDocument());

      } else if("N".equalsIgnoreCase(isFlow)) {
        YIFClientFactory.getInstance().getApi()
        .invoke(env,apiFlowName, inXml.getDocument());

      }
      deleteAsyncRequest(asyncKey,env);
    }
    catch(Exception exp) {
      updateErrorCount(asyncKey,errorCount,env);
      YFCDocument inboxXml = YFCDocument.getDocumentFor("<Inbox ExceptionType='Create Item Assocation Issue' "
          + "QueueId='"+queueId+"'></Inbox>");
      YIFClientFactory.getInstance().getApi()
      .invoke(env,"createException", inboxXml.getDocument());
      LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeAsyncRequest", exp);
    }
  }
  
  /**
   * On Exception increment error count for the 
   * message in AsyncRequest table
   * @param asyncKey
   * @param errorCount
   * @throws YIFClientCreationException 
   * @throws RemoteException 
   * @throws YFSException 
   */
  
  private void updateErrorCount(String asyncKey,int errorCount,YFSEnvironment env) throws YFSException, RemoteException, YIFClientCreationException {
    int count = errorCount + 1; 
    YFCDocument errorCountXml = YFCDocument.getDocumentFor("<AsyncRequest AsyncronousRequestKey='"+asyncKey+"'"
        + " ErrorCount='"+count+"'  />");
    YIFClientFactory.getInstance().getApi()
    .executeFlow(env,"GpsChangeAsyncRequest", errorCountXml.getDocument());
  }
  
  /**
   * Method to delete the message from the AsyncRequest 
   * table after Item association is success full.
   * 
   * @param asyncKey
   * @throws YIFClientCreationException 
   * @throws RemoteException 
   * @throws YFSException 
   */
  private void deleteAsyncRequest(String asyncKey,YFSEnvironment env) throws YFSException, RemoteException, YIFClientCreationException { 
    YFCDocument errorCountXml = YFCDocument.getDocumentFor("<AsyncRequest AsyncronousRequestKey='"+asyncKey+"' />");
    YIFClientFactory.getInstance().getApi()
    .executeFlow(env,"GpsDeleteAsyncRequest", errorCountXml.getDocument());
  }

  
  /**
   * Get Jobs method gets message from AsyncRequest table
   * and send it to queue for execution.
   */
  @Override
  public List getJobs(YFSEnvironment env, Document arg1, Document lastMessageCreated) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", arg1);
    YFCDocument docInXML = YFCDocument.getDocumentFor(arg1);
    List<Document> jobList = new ArrayList<Document>();

    try {
      
      if (lastMessageCreated != null) {
          jobList = null;
          return jobList;
        }
      if(!SCUtil.isVoid(docInXML)) {
        String interfaceNo = docInXML.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO);
        String queueId = docInXML.getDocumentElement().getAttribute(TelstraConstants.QUEUE_ID);
        YFCDocument asyncRequestInXml = YFCDocument.getDocumentFor("<AsyncRequestList InterfaceNo='"+interfaceNo+"'/>");
        YFCDocument asyncRequestOutXml = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi()
        .executeFlow(env,"GpsGetAsyncRequestList", asyncRequestInXml.getDocument()));
        YFCElement asyncRequestOutEle = asyncRequestOutXml.getDocumentElement();
        YFCIterable<YFCElement> asyncRequestListEle = asyncRequestOutEle.getChildren("AsyncRequest");
        for(YFCElement asyncRequest:asyncRequestListEle) {
          YFCDocument inXml = YFCDocument.createDocument("AsyncRequestList");
          YFCElement inEle = inXml.getDocumentElement();
          inEle.setAttribute(TelstraConstants.QUEUE_ID, queueId);
          inEle.importNode(asyncRequest);
          jobList.add(inXml.getDocument());
        }
      }
    } catch (Exception exp) {
      // ERR9990001=Error in Get Jobs
      throw ExceptionUtil.getYFSException(ExceptionLiterals.ERRORCODE_GET_JOBS, exp);
    }

    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", docInXML);
    return jobList;  
  }
  
}