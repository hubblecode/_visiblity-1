package com.gps.hubble.catalog.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
/**
 * Custom API class to create or modify ItemNodeDefn
 * 
 * @author Murali Pachiappan
 * @version 1.0
 * 
 * Class extends AbstractCustomApi utils class
 */
public class ManageItemNodeDefinition extends AbstractCustomApi {

  /**
   * Base method initial execution starts.
   * 
   * Method gets ItemNodeDefn list of the item and
   * calls createOrModifyItemNodeDefn method
   * 
   */
  @Override
  public YFCDocument invoke(YFCDocument inXml) {
    YFCDocument itemNodeDefnTemp = YFCDocument.getDocumentFor("<ItemNodeDefnList TotalNumberOfRecords='' />");
    YFCDocument getItemNodeList = prepareXml(inXml);
    YFCDocument itemNodeDefn = invokeYantraApi("getItemNodeDefnList", getItemNodeList,itemNodeDefnTemp);
    String count = itemNodeDefn.getDocumentElement().getAttribute("TotalNumberOfRecords");
    createOrModifyItemNodeDefn(inXml,count);
    return inXml;
  }

  
  /**
   * Method validate count from get ItemNodeDefnList If list 
   * count is 0 then calls createItemNodeDefn else 
   * calls modifyItemNodeDefn 
   * 
   * @param inXml
   * @param count
   */
  private void createOrModifyItemNodeDefn(YFCDocument inXml , String count) {
    if("0".equals(count)) {
      invokeYantraApi("createItemNodeDefn", inXml);
    } else {
      invokeYantraApi("modifyItemNodeDefn", inXml);
    }
  }
  
  
  /**
   * Method to create input XML for get ItemNodeDefnList
   * 
   * @param inXml
   * @return
   */
  private YFCDocument prepareXml(YFCDocument inXml) {
    YFCDocument getItemNodeList = YFCDocument.getDocumentFor("<ItemNodeDefn />");
    getItemNodeList.getDocumentElement().setAttribute(TelstraConstants.ITEM_ID,
        inXml.getDocumentElement().getAttribute(TelstraConstants.ITEM_ID));
    getItemNodeList.getDocumentElement().setAttribute("UnitOfMeasure",
        inXml.getDocumentElement().getAttribute("UnitOfMeasure"));
    getItemNodeList.getDocumentElement().setAttribute("Node",
        inXml.getDocumentElement().getAttribute("Node"));
    getItemNodeList.getDocumentElement().setAttribute("OrganizationCode",
        inXml.getDocumentElement().getAttribute("OrganizationCode"));
    
    return getItemNodeList;
  }
}