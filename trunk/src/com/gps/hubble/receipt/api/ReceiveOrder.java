/***********************************************************************************************
 * File	Name		: ReceiveOrder.java
 *
 * Description		: This class is called when we consume Receive Order Message from Queue.
 * 						1. From input xml get OrderName(OrderNo) and ReceivingNode and call 
 * 							getShipmentList to get Shipped Shipments
 * 						2. Then call receiveOrder against those Shipments.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 17,2016	  	Tushar Dhaka 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.receipt.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;
import com.gps.hubble.constants.TelstraConstants;


/**
 * Custom code to call receiveOrder
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 * Extends AbstractCustomApi class
 */
public class ReceiveOrder extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ReceiveOrder.class);
	
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		/*
		 * Input XML:-
				<ReceiptList>			
					<Receipt ReceivingNode="" OrderName="" >		
						<ReceiptLines>	
							<ReceiptLine ItemID="" PrimeLineNo="" Quantity="ReceivedQuantity" SubLineNo="1" UnitOfMeasure="">
							</ReceiptLine>
						</ReceiptLines>	
					</Receipt>		
				<ReceiptList>
		 */
		
		YFCElement eleReceipt = inXml.getElementsByTagName(TelstraConstants.RECEIPT).item(0);
		if(!YFCObject.isVoid(eleReceipt)) {
			YFCDocument docGetShipmentListInput = getShipmentListInput(inXml);
			YFCDocument docGetShipmentListOutput=null;
			if(docGetShipmentListInput.getElementsByTagName(TelstraConstants.SHIPMENT_LINE).getLength()>0) {
				docGetShipmentListOutput = invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, docGetShipmentListInput, getShipmentListTempate());
			
				YFCElement eleShipment = docGetShipmentListOutput.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
				if(!YFCObject.isVoid(eleShipment)) {
					
					YFCDocument docReceiveOrderInput = getReceiveOrderInput(eleReceipt, eleShipment);					
					invokeYantraApi(TelstraConstants.RECEIVE_ORDER_API, docReceiveOrderInput);					
				}
				else {
					//throw exception
					LoggerUtil.verboseLog("No Valid Shipment Found ", logger, " throwing exception");
					YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_1181_001","No Valid Shipment Found for receiving.");
					throw new YFSException(erroDoc.toString());	
				}
			}
		}		
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		return inXml;
	}
	
	private YFCDocument getReceiveOrderInput(YFCElement eleReceipt, YFCElement eleShipment) {
		/*
		 * Input to receiveOrder:-
			<Receipt DocumentType="0005" ReceivingNode="DC1" EnterpriseCode="XYZ-CORP">
				<Shipment ShipmentKey="S0000001" ReceivingNode="DC1"/>
				<ReceiptLines>
					<ReceiptLine ItemID="NOR-00001" Quantity="50"/>
				</ReceiptLines>
			</Receipt>
		 */
		YFCDocument docReceiveOrderInput = YFCDocument.getDocumentFor("<Receipt DocumentType='"+eleShipment.getAttribute("DocumentType","")+
				"' ReceivingNode='"+eleShipment.getAttribute("ReceivingNode","")+"' EnterpriseCode='"+eleShipment.getAttribute("EnterpriseCode","")+
				"'><Shipment ShipmentKey='"+eleShipment.getAttribute("ShipmentKey","")+"' ReceivingNode='"+eleShipment.getAttribute("ReceivingNode","")+
				"'/><ReceiptLines/></Receipt>");
		
		for(YFCElement eleReceiptLine : eleReceipt.getElementsByTagName(TelstraConstants.RECEIPT_LINE)) {
			String sItemID = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID,"");
			double dQuantity = eleReceiptLine.getDoubleAttribute(TelstraConstants.QUANTITY,0.0);
			if(!YFCObject.isNull(sItemID)) {
				YFCElement eleLine = docReceiveOrderInput.createElement(TelstraConstants.RECEIPT_LINE);
				eleLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
				eleLine.setDoubleAttribute(TelstraConstants.QUANTITY, dQuantity);
				docReceiveOrderInput.getElementsByTagName(TelstraConstants.RECEIPT_LINES).item(0).appendChild(eleLine);
			}
		}
		return docReceiveOrderInput;
	}
	
	private YFCDocument getShipmentListInput(YFCDocument inXml) {
		
		YFCElement eleReceipt = inXml.getElementsByTagName(TelstraConstants.RECEIPT).item(0);
		
		String sOrderName = eleReceipt.getAttribute(TelstraConstants.ORDER_NAME,"");
		String sReceivingNode = eleReceipt.getAttribute(TelstraConstants.RECEIVING_NODE,"");
		
		/*
		 * Input to getShipmentList:-
			<Shipment ReceivingNode="TestOrg5" StatusQryType="BETWEEN" FromStatus="1400" ToStatus="1599">
			     <ShipmentLines>
			            <ShipmentLine ItemID="testitem1" QuantityQryType="GE" Quantity="5">
			            	<Order OrderNo="3003PO10"/>
			            </ShipmentLine>
			     </ShipmentLines>
			</Shipment>
		 */
		
		YFCDocument docGetShipmentListInput = YFCDocument.getDocumentFor("<Shipment ReceivingNode='"+sReceivingNode+"' "
				+ "StatusQryType='BETWEEN' FromStatus='1400' ToStatus='1599'><ShipmentLines/></Shipment>");
		
		for(YFCElement eleReceiptLine : eleReceipt.getElementsByTagName(TelstraConstants.RECEIPT_LINE)) {
			String sItemID = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID,"");
			String sQuantity = eleReceiptLine.getAttribute(TelstraConstants.QUANTITY,"");
			String sPrimeLineNo = eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO,"");
			if(!YFCObject.isNull(sItemID) && !YFCObject.isNull(sQuantity)) {
				YFCElement eleShipmentLine = docGetShipmentListInput.createElement(TelstraConstants.SHIPMENT_LINE);
				eleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
				eleShipmentLine.setAttribute("QuantityQryType", "GE");
				eleShipmentLine.setAttribute(TelstraConstants.QUANTITY, sQuantity);
				docGetShipmentListInput.getElementsByTagName(TelstraConstants.SHIPMENT_LINES).item(0).appendChild(eleShipmentLine);
				
				YFCElement eleShipmentLineOrder = docGetShipmentListInput.createElement(TelstraConstants.ORDER);
				eleShipmentLineOrder.setAttribute(TelstraConstants.ORDER_NAME, sOrderName);
				eleShipmentLine.appendChild(eleShipmentLineOrder);
				
				if(!YFCObject.isNull(sPrimeLineNo)){
					YFCElement eleShipmentLineOrderLine = docGetShipmentListInput.createElement(TelstraConstants.ORDER_LINE);
					eleShipmentLineOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
					eleShipmentLine.appendChild(eleShipmentLineOrderLine);
				}
			}
		}
		
		return docGetShipmentListInput;
	}
	
	private YFCDocument getShipmentListTempate() {
		YFCDocument template = YFCDocument.getDocumentFor("<Shipments><Shipment EnterpriseCode='' DocumentType='' ReceivingNode='' ShipmentKey=''/></Shipments>");
		return template;
	}
}