package com.gps.hubble.constants;

/**
 * This class maintains all the error code which are not configured in the service argument.
 * @author Prateek
 *
 */
public final class TelstraErrorCodeConstants {
	//Inventory upload error code constant
	public static final String INV_UPLOAD_SHIP_NODE_MISSING_ERROR_CODE = "TEL_ERR_0644_001";
	public static final String INV_UPLOAD_ITEM_ID_MISSING_ERROR_CODE = "TEL_ERR_0644_002";
	public static final String INV_UPLOAD_QUANTITY_MISSING_ERROR_CODE = "TEL_ERR_0644_003";
	public static final String INV_UPLOAD_INVALID_SHIP_NODE_ERROR_CODE = "TEL_ERR_0644_004";
	public static final String INV_UPLOAD_INVALID_ITEM_ID_ERROR_CODE = "TEL_ERR_0644_005";
	public static final String INV_UPLOAD_INVALID_QUANTITY_ERROR_CODE = "TEL_ERR_0644_006";
	public static final String INV_UPLOAD_INVALID_SHIP_NODE_TYPE_ERROR_CODE = "TEL_ERR_0644_007";
	public static final String INV_UPLOAD_INV_NOT_TRACKABLE_ERROR_CODE = "TEL_ERR_0644_008";
	// Stock movement report upload error code constant
	public static final String STOCK_MOVEMNT_UPLOAD_SHIP_NODE_MISSING_ERROR_CODE = "TEL_ERR_0645_001";
	public static final String STOCK_MOVEMNT_UPLOAD_ITEM_ID_MISSING_ERROR_CODE = "TEL_ERR_0645_002";
	public static final String STOCK_MOVEMNT_UPLOAD_QUANTITY_MISSING_ERROR_CODE = "TEL_ERR_0645_003";
	public static final String STOCK_MOVEMNT_UPLOAD_INVALID_SHIP_NODE_ERROR_CODE = "TEL_ERR_0645_004";
	public static final String STOCK_MOVEMNT_UPLOAD_INVALID_ITEM_ID_ERROR_CODE = "TEL_ERR_0645_005";
	public static final String STOCK_MOVEMNT_UPLOAD_INVALID_QUANTITY_ERROR_CODE = "TEL_ERR_0645_006";
	public static final String STOCK_MOVEMNT_UPLOAD_INVALID_SHIP_NODE_TYPE_ERROR_CODE = "TEL_ERR_0645_007";
	public static final String STOCK_MOVEMNT_UPLOAD_INV_NOT_TRACKABLE_ERROR_CODE = "TEL_ERR_0645_008";
	//Order date calculation 
	public static final String DAC_SCHEDULE_NODE_OR_DAC_MISSING_ERROR_CODE = "TEL_ERR_1002_001";
	public static final String DAC_SCHEDULE_NODE_DC_MAPPING_MISSING_ERROR_CODE = "TEL_ERR_1002_002";
	public static final String MATERIAL_TRANSPORT_MAP_MAT_CLASS_OR_PRIORITY_MISSING_ERROR_CODE = "TEL_ERR_1002_003";
	public static final String TRANSPORT_MATRIX_NAME_OR_STATE_MISSING_ERROR_CODE = "TEL_ERR_1002_004";
	public static final String WEEK_NUMBER_MANDATORY_PARAMETER_MISSING_ERROR_CODE = "TEL_ERR_1002_005";
	public static final String MILK_RUN_ROUTE_ID_MISSING_ERROR_CODE = "TEL_ERR_1002_006";	

	private TelstraErrorCodeConstants() {

	}
}
