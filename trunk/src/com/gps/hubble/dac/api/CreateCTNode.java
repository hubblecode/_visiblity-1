package com.gps.hubble.dac.api;
//Java imports - NONE

//Project imports - NONE
import com.gps.hubble.constants.TelstraConstants;

//Sterling imports
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

//Misc imports 
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;

/**
 * This Class creates organization for Communication Technician nodes
 * @author Sambeet Mohanty
 * @version 1.0 ( 28 Jun 2016)
 *
 * Functionality
 * @see method invoke()
 *
 * Known Bugs
 * Revision History
 *
 */
public class CreateCTNode extends AbstractCustomApi {


	private static YFCLogCategory logger =YFCLogCategory.instance(CreateCTNode.class);
	private YFCDocument docManageOrgInput =null;

	/**
	 * Method name: invoke
	 * arguments: YFCDocument
	 * Base method Initial execution starts here
	 * Invokes manageOrganizationHierarchy API to create CT Nodes 
	 */
	@Override
	public YFCDocument invoke(YFCDocument docInXML) 
	{		
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", docInXML);
		String sCustID=docInXML.getDocumentElement().getAttribute("CustomerID");
		if(SCUtil.isVoid(sCustID))
		{
			throw new YFSException("Customer ID is null");
		}

		if(!SCUtil.isVoid(sCustID) && sCustID.startsWith("V"))
		{
			docManageOrgInput=populateManageOrgRequest(docInXML, sCustID);
			return invokeYantraApi("manageOrganizationHierarchy", docManageOrgInput);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docInXML);
		return docInXML;

	}

	/**
	 * Method name: getLocaleCode
	 * @param inXML
	 * @return String
	 * This methods determines the locale code of the CT node to be created based on the state code 
	 */
	public String getLocaleCode(YFCDocument inXML)
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getLocaleCode", inXML);
		String sState=inXML.getElementsByTagName("BillingPersonInfo").item(0).getAttribute("State");
		LoggerUtil.verboseLog("State code present in the input Document: "+sState, logger, inXML);
		if(SCUtil.isVoid(sState))
		{
			sState="VI" ;
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getLocaleCode", inXML);
		return getProperty(sState, true);

	}

	/**
	 * Method name: populateManageOrgRequest
	 * @param inXML
	 * @param sCustID
	 * @return docManageOrg
	 * This methods populates the input request xml for manageOrganizationHierarchy API 
	 */
	public YFCDocument populateManageOrgRequest(YFCDocument inXML, String sCustID)
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "populateManageOrgRequest", inXML);
		String sLoceleCode=getLocaleCode(inXML);

		YFCDocument docManageOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement eleInput=docManageOrg.getDocumentElement();

		eleInput.setAttribute("OrganizationKey", sCustID);
		eleInput.setAttribute("OrganizationCode", sCustID);  
		eleInput.setAttribute("OrganizationName", sCustID); 
		eleInput.setAttribute("PrimaryEnterpriseKey", TelstraConstants.ORG_TELSTRA);
		eleInput.setAttribute("ParentOrganizationCode", TelstraConstants.ORG_TELSTRA);

		eleInput.setAttribute("InventoryPublished", "N");
		eleInput.setAttribute("LocaleCode", sLoceleCode);

		YFCElement  eleNode=docManageOrg.createElement("Node");
		eleNode.setAttribute("InventoryTracked", "N");
		eleNode.setAttribute("NodeType", "CT");
		eleInput.appendChild(eleNode);

		YFCElement eleBillingAddress=docManageOrg.importNode(inXML.getElementsByTagName("BillingPersonInfo").item(0),true);
		eleInput.appendChild(eleBillingAddress);

		YFCElement eleCorpAddress=docManageOrg.createElement("CorporatePersonInfo");
		eleCorpAddress.setAttributes(eleBillingAddress.getAttributes());
		eleInput.appendChild(eleCorpAddress);
		
		YFCElement eleOrgRoleList=docManageOrg.createElement("OrgRoleList");
		eleInput.appendChild(eleOrgRoleList);
		
		YFCElement eleOrgRole=docManageOrg.createElement("OrgRole");
		eleOrgRole.setAttribute("RoleKey", "NODE");
		eleOrgRoleList.appendChild(eleOrgRole);	
		
		LoggerUtil.verboseLog("Input Document docManageOrg:", logger, docManageOrg);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "populateManageOrgRequest", inXML);

		return docManageOrg;

	}

}
