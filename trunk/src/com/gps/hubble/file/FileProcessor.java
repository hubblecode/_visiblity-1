package com.gps.hubble.file;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
/**
* Class description goes here.
*
* @version 1.0 30 June 2016
* @author VGupta
*
*/
import org.apache.poi.ss.usermodel.Workbook;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public abstract class FileProcessor {
	private ServiceInvoker serviceInvoker = null;
	private static YFCLogCategory cat = YFCLogCategory.instance(FileProcessor.class);

	public abstract void processRow(Row row);
	
	public  Workbook processWorkbook(Workbook workbook){
		Sheet sheet = workbook.getSheetAt(0);
		String errorMsg = "Error While Processing: ";
		Iterator<Row> rowIterator = sheet.iterator();
		boolean isHeader = true;
		String headerKey = "";
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String key = ExcelUtil.getCellvalue(row, 0);
			try
			{
				if(isHeader){
					isHeader = false;
					headerKey = ExcelUtil.getCellvalue(row, 0);
					ExcelUtil.moveRow(workbook, row, true,"Error Message");
					ExcelUtil.moveRow(workbook, row, false,"");
					continue;
				}
				if(null != row){
					processRow(row);
					ExcelUtil.moveRow(workbook, row, false,"");
				}

			}
			catch (YFSException yfsEx)
			{
				String strErrorDesc = yfsEx.getErrorDescription();
				if(YFCCommon.isVoid(strErrorDesc)){
					strErrorDesc = yfsEx.getMessage();
				}
				cat.error("YFSException : " + errorMsg + headerKey  + ":"  + key + ":" + strErrorDesc);
				ExcelUtil.moveRow(workbook, row, true,strErrorDesc); 
			}
			catch (Exception ex)
			{
				String strErrorDesc = ex.getMessage();
				cat.error(errorMsg + headerKey  + ":"  + key + ":" + strErrorDesc);
				ExcelUtil.moveRow(workbook, row, true,strErrorDesc); 
			}
		}
		return workbook;
	}
	
	protected void setServiceInvoker(ServiceInvoker serviceInvoker){
		this.serviceInvoker=serviceInvoker;
	}
	
	protected ServiceInvoker getServiceInvoker() {
		return serviceInvoker;
	}
	
}
