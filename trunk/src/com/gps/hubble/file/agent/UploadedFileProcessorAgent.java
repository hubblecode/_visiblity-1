package com.gps.hubble.file.agent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.FileProcessorFactory;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;


/**
* Class description goes here.
*
* @version 1.0 14 June 2016
* @author VGupta
*
*/

public class UploadedFileProcessorAgent extends AbstractCustomBaseAgent
{
	
	private static YFCLogCategory cat = YFCLogCategory.instance(UploadedFileProcessorAgent.class);

	public List<YFCDocument> getJobs(YFCDocument inXML, YFCDocument lastMessage) 
	{
		debug("inXML : " + inXML);
		List<YFCDocument> aList = new ArrayList<YFCDocument>();

		YFCElement eleMessageXML = inXML.getDocumentElement();
		String sTransactionType = eleMessageXML.getAttribute("TRANSACTION_TYPE");

		if (YFCUtils.isVoid(sTransactionType))
		{
			sTransactionType = eleMessageXML.getAttribute("TransactionType");
			if (YFCUtils.isVoid(sTransactionType))
			{
				throw new YFSException("Agent Params Configuration Missing",
					"Agent Params Configuration Missing",
					"TRANSACTION_TYPE or TransactionType configuration is required");
			}
		}

		YFCDocument yfcFileListDoc = getFilesToProcess(sTransactionType);
		YFCIterable<YFCElement> yfcFileListEle = yfcFileListDoc.getDocumentElement().getChildren();
		for(YFCElement eFileupload:yfcFileListEle){
				eFileupload.setAttribute("TransactionType", sTransactionType);
				aList.add((YFCDocument.getDocumentFor(eFileupload.getString())));
		}
		debug("getJobs size is : " + aList.size());
		return aList;
	}

	@Override
	public void executeJob(YFCDocument yfcIndoc)  {
		try 
		{
			YFCElement yfcRootEle = yfcIndoc.getDocumentElement();
			debug("Input to execute jobs : " + yfcRootEle.toString());
			String strEncodedExcel = yfcRootEle.getAttribute("FileObject");

			if (!YFCCommon.isStringVoid(strEncodedExcel))
			{
				byte[] decodedBytes = Base64.decodeBase64(strEncodedExcel.getBytes());
				String strTransactionType = yfcRootEle.getAttribute("TransactionType");
				Workbook processExcel = processExcel(decodedBytes, strTransactionType);
				
				byte[] encodedBytes = updateFileUpload(yfcRootEle, processExcel);
				notifyEmail(yfcRootEle, strTransactionType, processExcel, encodedBytes);
			}
			else
			{
				cat.info("strEncodedExcel is Null");
			}
		}
		catch (Exception exp)
		{
			cat.error(exp);
			YFCException yfcException = new YFCException(exp);
			throw yfcException;
		}
	}

	/**
	 * @param yfcRootEle
	 * @param processExcel
	 * @return
	 * @throws IOException
	 */
		private byte[] updateFileUpload(YFCElement yfcRootEle, Workbook processExcel) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		processExcel.write(out);
		byte[] processByteArray = out.toByteArray();
		out.close();
		
		byte[] encodedBytes = Base64.encodeBase64(processByteArray);
		
		cat.info("lenght of GpsChangeFileUpload is:" + encodedBytes.length);
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<UploadedFile/>");
		YFCElement yfcUploadedFileEle = yfcChangeInDoc.getDocumentElement();
		String strUploadedFileKey = yfcRootEle.getAttribute("FileUploadKey");
		yfcUploadedFileEle.setAttribute("FileUploadKey", strUploadedFileKey);
		yfcUploadedFileEle.setAttribute("FileObject", new String(encodedBytes));
		yfcUploadedFileEle.setAttribute("ProcessedFlag", "Y");
		invokeYantraService("GpsChangeFileUpload", yfcChangeInDoc);
		return encodedBytes;
	}

	/**
	 * @param yfcRootEle
	 * @param strTransactionType
	 * @param processExcel
	 * @param encodedBytes
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void notifyEmail(YFCElement yfcRootEle, String strTransactionType, Workbook processExcel,
			byte[] encodedBytes) throws MessagingException, IOException {
		String strToEmailId = yfcRootEle.getAttribute("NotifyEmail");
		if(!YFCCommon.isVoid(strToEmailId)){
			
			String strFromEmailId = YFCConfigurator.getInstance().getProperty("gps.email.from.id", TelstraConstants.EMAIL_FROM_ID);
			String strPort =		YFCConfigurator.getInstance().getProperty("gps.email.port", TelstraConstants.EMAIL_PORT);
			String strHost =		YFCConfigurator.getInstance().getProperty("gps.email.hostname", TelstraConstants.EMAIL_HOST);
			String sFileExtension = ".xls";
			if(processExcel instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
				sFileExtension = ".xlsx";
			} 
			ExcelUtil.sendExcelAttachmentEmail(encodedBytes, strToEmailId, strFromEmailId, strTransactionType, sFileExtension, strPort, strHost);
		}
	}


	protected YFCDocument getFilesToProcess(String strProcess){
		/**
		 * The method calls the extended database Get List API to get a list of
		 * all the files that are yet to be processed for a particular process
		 * passed in the argument
		 */
		YFCDocument yfcGetFileListInDoc = YFCDocument
				.getDocumentFor("<UploadedFile TransactionType=\"" + strProcess + "\" ProcessedFlag=\"N\"/>");
		debug("GpsGetFileUploadList : " + yfcGetFileListInDoc.getString());
		return invokeYantraService("GpsGetFileUploadList", yfcGetFileListInDoc);
	}

	private  Workbook processExcel(byte[] decodedBytes, String strTransactionType) throws InvalidFormatException, IOException 
	{
		InputStream file = null;
		Workbook workbook;
		try {
			file = new ByteArrayInputStream(decodedBytes);
			workbook = WorkbookFactory.create(file);
			debug("Invoking : " + strTransactionType);
			FileProcessor fileProcessor = FileProcessorFactory.create(strTransactionType , getServiceInvoker());
			workbook = fileProcessor.processWorkbook(workbook);
			
		} finally{
			if(null!=file){
				file.close();
			}
		}
		return workbook;
	}
	
	private void debug(String string) {
		cat.debug(string);
	}
}
