package com.gps.hubble.file.agent.inventory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom code to upload inventory from excel
 * 
 * @author Prateek
 * @version 1.0
 * 
 * Extends FileProcessor class
 */
public class InventoryUploadFileProcessor extends FileProcessor {

	private static YFCLogCategory logger = YFCLogCategory.instance(InventoryUploadFileProcessor.class);

	/**
	 * 
	 */
	@Override
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", row);
		uploadInventory(row);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", row);

	}

	/**
	 * 
	 * @param row
	 */
	private void uploadInventory(Row row) {

		// fetch and validate the field from the excel row
		String sShipNode = validateAndGetShipNode(row);
		String sItemId = getItemId(row);
		String sQuantity = validateAndGetQuantity(row);
		String sUnitOfMeasure = validateItemIdAndGetUnitOfMeasure(sItemId);
		String sReasonText = getResonText(row);

		// prepare and call adjustInventory api
		callAdjustInventoryApi(sShipNode, sItemId, sQuantity, sUnitOfMeasure, sReasonText);
	}

	/**
	 * This method upload the inventory in sterling data base by calling adjust
	 * inventory api
	 * 
	 * @param sShipNode
	 * @param sItemId
	 * @param sQuantity
	 * @param sUnitOfMeasure
	 * @param sReasonText
	 */
	private void callAdjustInventoryApi(String sShipNode, String sItemId, String sQuantity, String sUnitOfMeasure,
			String sReasonText) {
		YFCDocument docAdjustInventory = YFCDocument.createDocument(TelstraConstants.ITEMS);
		YFCElement eleItems = docAdjustInventory.getDocumentElement();
		YFCElement eleItem = docAdjustInventory.createElement(TelstraConstants.ITEM);
		eleItems.appendChild(eleItem);

		eleItem.setAttribute(TelstraConstants.ADJUSTMENT_TYPE, TelstraConstants.ABSOLUTE);
		eleItem.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		eleItem.setAttribute(TelstraConstants.QUANTITY, sQuantity);
		eleItem.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		eleItem.setAttribute(TelstraConstants.SUPPLY_TYPE, TelstraConstants.ONHAND);
		eleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
		eleItem.setAttribute(TelstraConstants.AVAILABILITY, TelstraConstants.TRACK);
		if (StringUtils.isNotBlank(sReasonText)) {
			eleItem.setAttribute(TelstraConstants.REASON_CODE, TelstraConstants.EXCEL_UPLOAD_INV_REASON_CODE);
			eleItem.setAttribute(TelstraConstants.REASON_TEXT, sReasonText);
		}
		LoggerUtil.verboseLog("InventoryUploadFileProcessor :: uploadInventory :: adjustInventory input doc\n", logger,
				docAdjustInventory);
		getServiceInvoker().invokeYantraApi(TelstraConstants.API_ADJUST_INVENTORY, docAdjustInventory);

	}

	/**
	 * This method sub string the text to 255 character if it is more than this
	 * 
	 * @param row
	 * @return
	 */
	private String getResonText(Row row) {
		String sReasonText = ExcelUtil.getCellvalue(row, 3);
		if (sReasonText.length() > 255) {
			sReasonText = sReasonText.substring(0, 255);
		}
		return sReasonText;
	}

	/**
	 * This method fetches and validate quantity field
	 * 
	 * @param row
	 * @return
	 */
	private String validateAndGetQuantity(Row row) {

		String sQuantity = ExcelUtil.getCellvalue(row, 2);
		if (YFCCommon.isVoid(sQuantity)) {
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_QUANTITY_MISSING_ERROR_CODE);
			LoggerUtil.verboseLog("Inventory quantity missing", logger, " throwing exception");
			throw new YFSException(erroDoc.toString());
		} 
		try {
			Double.parseDouble(sQuantity);	           
		} catch (NumberFormatException e) {
			LoggerUtil.verboseLog("Inventory quantity invalid", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INVALID_QUANTITY_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sQuantity;
	}

	/**
	 * This method retrieves item id from the excel row
	 * 
	 * @param row
	 * @return
	 */
	private String getItemId(Row row) {
		String sItemId = ExcelUtil.getCellvalue(row, 1);
		if (YFCCommon.isVoid(sItemId)) {
			LoggerUtil.verboseLog("Item ID missing", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_ITEM_ID_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sItemId;
	}

	/**
	 * This method validates whether ship node is valid for inventory upload or
	 * not.
	 * 
	 * @param row
	 * @return
	 */
	private String validateAndGetShipNode(Row row) {

		String sShipNode = ExcelUtil.getCellvalue(row, 0);
		if (YFCCommon.isVoid(sShipNode)) {
			LoggerUtil.verboseLog("Ship node missing", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_SHIP_NODE_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		boolean isShipNodeValid = validateShipNode(sShipNode);
		if (!isShipNodeValid) {
			LoggerUtil.verboseLog("Invalid ship node type", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_INVALID_SHIP_NODE_TYPE_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sShipNode;
	}

	/**
	 * This method validates whether item id passed in the input is valid or
	 * not. If it is valid it returns UOM
	 * 
	 * @param sItemId
	 * @return
	 */
	private String validateItemIdAndGetUnitOfMeasure(String sItemId) {

		YFCDocument docGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='" + sItemId + "'/>");
		LoggerUtil.verboseLog(
				"InventoryUploadFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList input doc\n", logger,
				docGetItemListIp);
		YFCDocument docGetItemListTemp = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\" /></ItemList>");
		LoggerUtil.verboseLog(
				"InventoryUploadFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList template doc\n",
				logger, docGetItemListTemp);
		YFCDocument docGetItemListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				docGetItemListIp, docGetItemListTemp);
		LoggerUtil.verboseLog(
				"InventoryUploadFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList output doc\n", logger,
				docGetItemListOp);

		YFCElement eleItem = docGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if (YFCCommon.isVoid(eleItem)) {
			LoggerUtil.verboseLog("Invalid item id type", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_INVALID_ITEM_ID_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
	}

	/**
	 * This method checks whether ship node exist in Sterling or not. If it
	 * exist then is node type is valid for inventory upload or not
	 * 
	 * @param sShipNode
	 * @return boolean
	 */
	private boolean validateShipNode(String sShipNode) {

		boolean bReturn = false;

		YFCDocument docGetShipNodeListIp = YFCDocument.getDocumentFor("<ShipNode ShipNode='" + sShipNode + "'/>");
		LoggerUtil.verboseLog("InventoryUploadFileProcessor :: validateShipNode :: getShipNodeList input doc\n", logger,
				docGetShipNodeListIp);
		YFCDocument docGetShipNodeListTemp = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode ShipNode=\"\" NodeType=\"\" InventoryTracked=\"\"/></ShipNodeList>");
		LoggerUtil.verboseLog("InventoryUploadFileProcessor :: validateShipNode :: getShipNodeList template doc\n",
				logger, docGetShipNodeListTemp);
		YFCDocument docGetShipNodeListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_SHIP_NODE_LIST,
				docGetShipNodeListIp, docGetShipNodeListTemp);
		LoggerUtil.verboseLog("InventoryUploadFileProcessor :: validateShipNode :: getShipNodeList output doc\n",
				logger, docGetShipNodeListOp);

		YFCElement eleShipNode = docGetShipNodeListOp.getElementsByTagName(TelstraConstants.SHIP_NODE).item(0);
		if (YFCCommon.isVoid(eleShipNode)) {
			LoggerUtil.verboseLog("Invalid ship node", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_INVALID_SHIP_NODE_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}else {
			String sTrackedInventory = eleShipNode.getAttribute(TelstraConstants.INVENTORY_TRACKED);			
			if(TelstraConstants.NO.equalsIgnoreCase(sTrackedInventory)){
				LoggerUtil.verboseLog("Inventory is not tracked in this ship node:- "+sShipNode, logger, " throwing exception");
				YFCDocument erroDoc = ExceptionUtil
						.getYFSExceptionDocument(TelstraErrorCodeConstants.INV_UPLOAD_INV_NOT_TRACKABLE_ERROR_CODE);
				throw new YFSException(erroDoc.toString());
			}
		}
		String sNodeType = eleShipNode.getAttribute(TelstraConstants.NODE_TYPE);
		List<String> lCodeValue = getCommonCodeCodeValueList(TelstraConstants.INV_UPLOAD_NODE_TYPE);
		if (lCodeValue.contains(sNodeType)) {
			bReturn = true;
		}

		return bReturn;
	}

	/**
	 * This method calls get common code list api and return the list of code
	 * value.
	 * 
	 * @param sCodeType
	 * @return codeValueList
	 */
	private List<String> getCommonCodeCodeValueList(String sCodeType) {

		YFCDocument docGetCommonCodeListIp = YFCDocument.getDocumentFor("<CommonCode CodeType='" + sCodeType + "'/>");
		LoggerUtil.verboseLog("InventoryUploadFileProcessor :: getCommonCodeCodeValueList :: getCommonCode input doc\n",
				logger, docGetCommonCodeListIp);
		YFCDocument docGetCommonCodeListTemp = YFCDocument.getDocumentFor(
				"<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" CodeShortDescription=\"\"/></CommonCodeList>");
		LoggerUtil.verboseLog(
				"InventoryUploadFileProcessor :: getCommonCodeCodeValueList :: getCommonCode template doc\n", logger,
				docGetCommonCodeListTemp);
		YFCDocument docGetCommonCodeListOp = getServiceInvoker().invokeYantraApi(
				TelstraConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListIp, docGetCommonCodeListTemp);
		LoggerUtil.verboseLog(
				"InventoryUploadFileProcessor :: getCommonCodeCodeValueList :: getCommonCode output doc\n", logger,
				docGetCommonCodeListOp);

		List<String> lCodeValue = new ArrayList<String>();

		YFCNodeList<YFCElement> nlCommonCode = docGetCommonCodeListOp
				.getElementsByTagName(TelstraConstants.COMMON_CODE);
		for (int i = 0; i < nlCommonCode.getLength(); i++) {
			YFCElement eleCommonCode = nlCommonCode.item(i);
			String sCodeValue = eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE);
			lCodeValue.add(sCodeValue);
		}

		return lCodeValue;
	}

}
