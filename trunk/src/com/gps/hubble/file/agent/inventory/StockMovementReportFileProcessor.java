package com.gps.hubble.file.agent.inventory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.inventory.api.FinancialAdjustmentMessage;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom code to upload stock movement report from excel
 * 
 * @author Prateek
 * @version 1.0
 * 
 * Extends FileProcessor class
 */
public class StockMovementReportFileProcessor extends FileProcessor{

	private static YFCLogCategory logger = YFCLogCategory.instance(StockMovementReportFileProcessor.class);

	@Override
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", row);
		uploadStockMovementReport(row);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", row);

	}
	
	/**
	 * 
	 * @param row
	 */
	private void uploadStockMovementReport(Row row) {

		// fetch and validate the field from the excel row
		String sShipNode = validateAndGetShipNode(row);
		String sItemId = getItemId(row);
		String sQuantity = validateAndGetQuantity(row);
		String sUnitOfMeasure = validateItemIdAndGetUnitOfMeasure(sItemId);

		// prepare and call adjustInventory api
		callAdjustInventoryApi(sShipNode, sItemId, sQuantity, sUnitOfMeasure);

	}

	/**
	 * 
	 * @param sShipNode
	 * @param sItemId
	 * @param sQuantity
	 * @param sUnitOfMeasure
	 */
	private void callAdjustInventoryApi(String sShipNode, String sItemId, String sQuantity, String sUnitOfMeasure) {
		YFCDocument docAdjustInventory = YFCDocument.createDocument(TelstraConstants.ITEMS);
		YFCElement eleItems = docAdjustInventory.getDocumentElement();
		YFCElement eleItem = docAdjustInventory.createElement(TelstraConstants.ITEM);
		eleItems.appendChild(eleItem);

		eleItem.setAttribute(TelstraConstants.ADJUSTMENT_TYPE, TelstraConstants.ADJUSTMENT);
		eleItem.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		eleItem.setAttribute(TelstraConstants.QUANTITY, sQuantity);
		eleItem.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		eleItem.setAttribute(TelstraConstants.SUPPLY_TYPE, TelstraConstants.ONHAND);
		eleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
		eleItem.setAttribute(TelstraConstants.AVAILABILITY, TelstraConstants.TRACK);
		
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: uploadInventory :: adjustInventory input doc\n", logger,
				docAdjustInventory);
		getServiceInvoker().invokeYantraApi(TelstraConstants.API_ADJUST_INVENTORY, docAdjustInventory);

	}

	/**
	 * 
	 * @param sItemId
	 * @return
	 */
	private String validateItemIdAndGetUnitOfMeasure(String sItemId) {

		YFCDocument docGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='" + sItemId + "'/>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList input doc\n", logger,
				docGetItemListIp);
		YFCDocument docGetItemListTemp = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\" /></ItemList>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList template doc\n",
				logger, docGetItemListTemp);
		YFCDocument docGetItemListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				docGetItemListIp, docGetItemListTemp);
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList output doc\n", logger,
				docGetItemListOp);

		YFCElement eleItem = docGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if (YFCCommon.isVoid(eleItem)) {
			LoggerUtil.verboseLog("Invalid item id type", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INVALID_ITEM_ID_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private String validateAndGetQuantity(Row row) {

		String sQuantity = ExcelUtil.getCellvalue(row, 2);
		if (YFCCommon.isVoid(sQuantity)) {
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_QUANTITY_MISSING_ERROR_CODE);
			LoggerUtil.verboseLog("Inventory quantity missing", logger, " throwing exception");
			throw new YFSException(erroDoc.toString());
		} 
		try {
			Double.parseDouble(sQuantity);	           
		} catch (NumberFormatException e) {
			LoggerUtil.verboseLog("Inventory quantity invalid", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INVALID_QUANTITY_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sQuantity;
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private String getItemId(Row row) {
		String sItemId = ExcelUtil.getCellvalue(row, 1);
		if (YFCCommon.isVoid(sItemId)) {
			LoggerUtil.verboseLog("Item ID missing", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_ITEM_ID_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sItemId;
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private String validateAndGetShipNode(Row row) {
		String sShipNode = ExcelUtil.getCellvalue(row, 0);
		if (YFCCommon.isVoid(sShipNode)) {
			LoggerUtil.verboseLog("Ship node missing", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_SHIP_NODE_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		boolean isShipNodeValid = validateShipNode(sShipNode);
		if (!isShipNodeValid) {
			LoggerUtil.verboseLog("Invalid ship node type", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INVALID_SHIP_NODE_TYPE_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		return sShipNode;
	}

	/**
	 * 
	 * @param sShipNode
	 * @return
	 */
	private boolean validateShipNode(String sShipNode) {
		boolean bReturn = false;

		YFCDocument docGetShipNodeListIp = YFCDocument.getDocumentFor("<ShipNode ShipNode='" + sShipNode + "'/>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList input doc\n", logger,
				docGetShipNodeListIp);
		YFCDocument docGetShipNodeListTemp = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode ShipNode=\"\" NodeType=\"\" InventoryTracked=\"\"/></ShipNodeList>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList template doc\n",
				logger, docGetShipNodeListTemp);
		YFCDocument docGetShipNodeListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_SHIP_NODE_LIST,
				docGetShipNodeListIp, docGetShipNodeListTemp);
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList output doc\n",
				logger, docGetShipNodeListOp);

		YFCElement eleShipNode = docGetShipNodeListOp.getElementsByTagName(TelstraConstants.SHIP_NODE).item(0);
		if (YFCCommon.isVoid(eleShipNode)) {
			LoggerUtil.verboseLog("Invalid ship node", logger, " throwing exception");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INVALID_SHIP_NODE_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}else{
			String sTrackedInventory = eleShipNode.getAttribute(TelstraConstants.INVENTORY_TRACKED);			
			if(TelstraConstants.NO.equalsIgnoreCase(sTrackedInventory)){
				LoggerUtil.verboseLog("Inventory is not tracked in this ship node:- "+sShipNode, logger, " throwing exception");
				YFCDocument erroDoc = ExceptionUtil
						.getYFSExceptionDocument(TelstraErrorCodeConstants.STOCK_MOVEMNT_UPLOAD_INV_NOT_TRACKABLE_ERROR_CODE);
				throw new YFSException(erroDoc.toString());
			}
		}
		String sNodeType = eleShipNode.getAttribute(TelstraConstants.NODE_TYPE);
		List<String> lCodeValue = getCommonCodeCodeValueList(TelstraConstants.STOCK_MOVEMNT_UPLOAD_NODE_TYPE);
		if (lCodeValue.contains(sNodeType)) {
			bReturn = true;
		}

		return bReturn;
	}

	/**
	 * 
	 * @param sCodeType
	 * @return
	 */
	private List<String> getCommonCodeCodeValueList(String sCodeType) {

		YFCDocument docGetCommonCodeListIp = YFCDocument.getDocumentFor("<CommonCode CodeType='" + sCodeType + "'/>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode input doc\n",
				logger, docGetCommonCodeListIp);
		YFCDocument docGetCommonCodeListTemp = YFCDocument.getDocumentFor(
				"<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" CodeShortDescription=\"\"/></CommonCodeList>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode template doc\n", logger,
				docGetCommonCodeListTemp);
		YFCDocument docGetCommonCodeListOp = getServiceInvoker().invokeYantraApi(
				TelstraConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListIp, docGetCommonCodeListTemp);
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode output doc\n", logger,
				docGetCommonCodeListOp);

		List<String> lCodeValue = new ArrayList<String>();

		YFCNodeList<YFCElement> nlCommonCode = docGetCommonCodeListOp
				.getElementsByTagName(TelstraConstants.COMMON_CODE);
		for (int i = 0; i < nlCommonCode.getLength(); i++) {
			YFCElement eleCommonCode = nlCommonCode.item(i);
			String sCodeValue = eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE);
			lCodeValue.add(sCodeValue);
		}

		return lCodeValue;
	}

	/**
	 * 
	 */
	@Override
	public  Workbook processWorkbook(Workbook workbook){
		Sheet sheet = workbook.getSheetAt(0);
		String errorMsg = "Error While Processing: ";
		Iterator<Row> rowIterator = sheet.iterator();
		boolean isHeader = true;
		String headerKey = "";
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String key = ExcelUtil.getCellvalue(row, 0);
			try
			{
				if(isHeader){
					isHeader = false;
					headerKey = ExcelUtil.getCellvalue(row, 0);
					ExcelUtil.moveRow(workbook, row, true,"Error Message");
					ExcelUtil.moveRow(workbook, row, false,"");
					continue;
				}
				if(null != row){
					processRow(row);
					ExcelUtil.moveRow(workbook, row, false,"");
				}

			}
			catch (YFSException yfsEx)
			{
				String strErrorDesc = yfsEx.getErrorDescription();
				if(YFCCommon.isVoid(strErrorDesc)){
					strErrorDesc = yfsEx.getMessage();
				}
				logger.error("YFSException : " + errorMsg + headerKey  + ":"  + key + ":" + strErrorDesc);
				ExcelUtil.moveRow(workbook, row, true,strErrorDesc); 
			}
			catch (Exception ex)
			{
				String strErrorDesc = ex.getMessage();
				logger.error(errorMsg + headerKey  + ":"  + key + ":" + strErrorDesc);
				ExcelUtil.moveRow(workbook, row, true,strErrorDesc); 
			}
		}
		
		// prepare financial adjustment message document and send it to Integral Plus
		FinancialAdjustmentMessage obj = new FinancialAdjustmentMessage();
		obj.prepareFinancialAdjustmentMessage(workbook, getServiceInvoker());
		
		return workbook;
	}

}
