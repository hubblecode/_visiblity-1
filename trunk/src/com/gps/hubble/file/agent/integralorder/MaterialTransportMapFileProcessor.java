package com.gps.hubble.file.agent.integralorder;

import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * 
 *
 */
public class MaterialTransportMapFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(MaterialTransportMapFileProcessor.class);

/**
 * 
 */
	public void processRow(Row row){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String materialClass = ExcelUtil.getCellvalue(row, 0);
		String priority = ExcelUtil.getCellvalue(row, 1);
		String matrixName = ExcelUtil.getCellvalue(row, 2);
		if(YFCObject.isVoid(materialClass) || YFCObject.isVoid(priority)){
			LoggerUtil.verboseLog("Either material class or priority missing", logger, " .This row will be moved to error");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.MATERIAL_TRANSPORT_MAP_MAT_CLASS_OR_PRIORITY_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		if(!YFCObject.isVoid(matrixName)){
			matrixName = matrixName.toUpperCase();
		}
		YFCDocument materialTransportMap = YFCDocument.createDocument("MaterialToTransportMap");
		YFCElement materialTransportMapElem = materialTransportMap.getDocumentElement();
	    materialTransportMapElem.setAttribute("MaterialClass", materialClass);	
	    materialTransportMapElem.setAttribute(TelstraConstants.PRIORITY, priority);
	    YFCDocument output = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_MATERIAL_TO_TRANSPORT_MAP, materialTransportMap);
	    materialTransportMapElem.setAttribute(TelstraConstants.MATRIX_NAME, matrixName);
	    if(isRecordExist(output)){
	    	getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_MATERIAL_TO_TRANSPORT_MAP, materialTransportMap);
	    }else{
	    	getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_MATERIAL_TO_TRANSPORT_MAP, materialTransportMap);
	    }
	    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}
	
	
	
}
