package com.gps.hubble.file.agent.integralorder;

import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;
/**
 * 
 * @author Prateek
 *
 */
public class MilkRunFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(MilkRunFileProcessor.class);

	/**
	 * 
	 */
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String routeId = ExcelUtil.getCellvalue(row, 0);
		if(YFCObject.isVoid(routeId)){
			LoggerUtil.verboseLog("Mandatory parameter for milk run is missing", logger, " .This row will be moved to error");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.MILK_RUN_ROUTE_ID_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		YFCDocument milkRunSchedule = YFCDocument.createDocument("MilkRunSchedule");
		YFCElement milkRunScheduleElem = milkRunSchedule.getDocumentElement();
		milkRunScheduleElem.setAttribute(TelstraConstants.ROUTE_ID, routeId);
		YFCDocument milkRunScheduleOutput = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_MILK_RUN_SCHEDULE, milkRunSchedule);
		String milkRunName = ExcelUtil.getCellvalue(row, 1);
		milkRunName = milkRunName.toUpperCase();
		String weekDay = ExcelUtil.getCellvalue(row, 2);
		String frequency = ExcelUtil.getCellvalue(row, 3);
		String comments = ExcelUtil.getCellvalue(row, 4);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
		String effectiveFromStr = ExcelUtil.getCellvalue(row, 5,sdf);
		String effectiveFrom = null;
		if(!YFCObject.isVoid(effectiveFromStr)){
			effectiveFrom = convertToDateString(effectiveFromStr);
		}
		String effectiveToStr = ExcelUtil.getCellvalue(row, 6,sdf);
		String effectiveTo = null;
		if(!YFCObject.isVoid(effectiveToStr)){
			effectiveTo = convertToDateString(effectiveToStr);
		}
		milkRunScheduleElem.setAttribute("MilkRunName", milkRunName);
		milkRunScheduleElem.setAttribute("Weekday", weekDay);
		milkRunScheduleElem.setAttribute("Frequency", frequency);
		milkRunScheduleElem.setAttribute("Comments", comments);
		if(!YFCObject.isVoid(effectiveFrom)){
			milkRunScheduleElem.setAttribute("EffectiveFrom", effectiveFrom);
		}
		if(!YFCObject.isVoid(effectiveTo)){
			milkRunScheduleElem.setAttribute("EffectiveTo", effectiveTo);
		}
		if(isRecordExist(milkRunScheduleOutput)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_MILK_RUN_SCHEDULE, milkRunSchedule);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_MILK_RUN_SCHEDULE, milkRunSchedule);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	private static String convertToDateString(String dateTimeString){
		YDate ydate = new YDate(dateTimeString, "dd/MM/yyyy HH:mm:SS", true);
		return ydate.getString("yyyy-MM-dd");
	}
	
	private boolean isRecordExist(YFCDocument milkRunScheduleOutput){
		if(milkRunScheduleOutput == null || milkRunScheduleOutput.getDocumentElement() == null || milkRunScheduleOutput.getDocumentElement().getAttributes() == null || milkRunScheduleOutput.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}

}
