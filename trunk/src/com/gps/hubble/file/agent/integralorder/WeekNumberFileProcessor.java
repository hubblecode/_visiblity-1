package com.gps.hubble.file.agent.integralorder;

import java.text.SimpleDateFormat;

import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class WeekNumberFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(WeekNumberFileProcessor.class);

/**
 * 	
 */
	public void processRow(Row row){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String weekNumber = ExcelUtil.getCellvalue(row, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
		String fromDateStrWithTime = ExcelUtil.getCellvalue(row, 1, sdf);
		String toDateStrWithTime = ExcelUtil.getCellvalue(row, 2,sdf);
		
		if(YFCObject.isVoid(fromDateStrWithTime) || YFCObject.isVoid(toDateStrWithTime) || YFCObject.isVoid(weekNumber)){
			LoggerUtil.verboseLog("Mandatory parameter for week number is missing", logger, " .This row will be moved to error");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.WEEK_NUMBER_MANDATORY_PARAMETER_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		String fromDateStr = convertToDateString(fromDateStrWithTime);
		String toDateStr = convertToDateString(toDateStrWithTime);
		YFCDocument weekNumberInput = YFCDocument.createDocument(TelstraConstants.WEEK_NUMBER);
		YFCElement weekNumberInputElem = weekNumberInput.getDocumentElement();
		weekNumberInputElem.setAttribute("FromDate", fromDateStr);
		weekNumberInputElem.setAttribute("ToDate", toDateStr);
		YFCDocument getWeekNumberOutput = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_WEEK_NUMBER, weekNumberInput);
		weekNumberInputElem.setAttribute(TelstraConstants.WEEK_NUMBER, weekNumber);
		if(isRecordExist(getWeekNumberOutput)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_WEEK_NUMBER, weekNumberInput);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_WEEK_NUMBER, weekNumberInput);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	private static String convertToDateString(String dateTimeString){
		YDate ydate = new YDate(dateTimeString, "dd/MM/yyyy HH:mm:SS", true);
		return ydate.getString("yyyy-MM-dd");
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}

}
