package com.gps.hubble.file.agent.integralorder;

import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class TransportMatrixFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(TransportMatrixFileProcessor.class);


	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String matrixName = ExcelUtil.getCellvalue(row, 0);
		String fromState = ExcelUtil.getCellvalue(row, 1);
		String toPostCode = ExcelUtil.getCellvalue(row, 2);
		String toSuburb = ExcelUtil.getCellvalue(row, 3);
		if(YFCObject.isVoid(matrixName) || YFCObject.isVoid(fromState) || YFCObject.isVoid(toPostCode) || YFCObject.isVoid(toSuburb)){
			LoggerUtil.verboseLog("Mandatory parameter for transport matrix is missing", logger, " .This row will be moved to error");
			YFCDocument erroDoc = ExceptionUtil
					.getYFSExceptionDocument(TelstraErrorCodeConstants.TRANSPORT_MATRIX_NAME_OR_STATE_MISSING_ERROR_CODE);
			throw new YFSException(erroDoc.toString());
		}
		matrixName = matrixName.toUpperCase();
		YFCDocument transportMatrix = YFCDocument.createDocument(TelstraConstants.TRANSPORT_MATRIX);
		YFCElement transportMatrixElem = transportMatrix.getDocumentElement();
		transportMatrixElem.setAttribute(TelstraConstants.MATRIX_NAME, matrixName);
		transportMatrixElem.setAttribute(TelstraConstants.FROM_STATE, fromState);
		transportMatrixElem.setAttribute(TelstraConstants.TO_POSTCODE, toPostCode);
		transportMatrixElem.setAttribute(TelstraConstants.TO_SUBURB, toSuburb);
		YFCDocument output = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_TRANSPORT_MATRIX, transportMatrix);
		
		String mondayTransitTime = ExcelUtil.getCellvalue(row, 4);
		String tuesdayTransitTime = ExcelUtil.getCellvalue(row, 5);
		String wednesdayTransitTime = ExcelUtil.getCellvalue(row, 6);
		String thursdayTransitTime = ExcelUtil.getCellvalue(row, 7);
		String fridayTransitTime = ExcelUtil.getCellvalue(row, 8);
		transportMatrixElem.setAttribute(TelstraConstants.MONDAY_TRANSIT_TIME, mondayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.TUESDAY_TRANSIT_TIME, tuesdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.WEDNESDAY_TRANSIT_TIME, wednesdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.THURSDAY_TRANSIT_TIME, thursdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.FRIDAY_TRANSIT_TIME, fridayTransitTime);
		setDateAttributes(row, transportMatrixElem);
		if(isRecordExist(output)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_TRANSPORT_MATRIX, transportMatrix);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_TRANSPORT_MATRIX, transportMatrix);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	private void setDateAttributes(Row row,YFCElement transportMatrixElem){
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
		String fromDateStr = ExcelUtil.getCellvalue(row, 9,sdf);
		String fromDate = null;
		if(!YFCObject.isVoid(fromDateStr)){
			fromDate = convertToDateString(fromDateStr);
		}
		String toDateStr = ExcelUtil.getCellvalue(row, 10,sdf);
		String toDate = null;
		if(!YFCObject.isVoid(toDateStr)){
			toDate = convertToDateString(toDateStr);
		}
		if(!YFCObject.isVoid(fromDate)){
			transportMatrixElem.setAttribute("FromDate", fromDate);
		}
		if(!YFCObject.isVoid(toDate)){
			transportMatrixElem.setAttribute("ToDate", toDate);
		}
	}
	
	private static String convertToDateString(String dateTimeString){
		YDate ydate = new YDate(dateTimeString, "dd/MM/yyyy HH:mm:SS", true);
		return ydate.getString("yyyy-MM-dd");
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}

}
