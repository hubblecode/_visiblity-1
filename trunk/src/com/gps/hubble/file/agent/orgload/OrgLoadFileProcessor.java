package com.gps.hubble.file.agent.orgload;

import org.apache.poi.ss.usermodel.Row;

import com.bridge.sterling.utils.ExceptionUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
/**
* Class description goes here.
*
* @version 1.0 30 June 2016
* @author VGupta
*
*/
public class OrgLoadFileProcessor extends FileProcessor {
	
	private static YFCLogCategory cat = YFCLogCategory.instance(OrgLoadFileProcessor.class);
	private static final String ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY = "OrgRoleList/OrgRole@RoleKey";
	@Override	
	
	public void processRow(Row row) {
		manageOrganizationHierarchy(row);
	}

	private void manageOrganizationHierarchy(Row row) {
		String sOrganizationCode = ExcelUtil.getCellvalue(row, 0);
		String sParentOrganizationCode = ExcelUtil.getCellvalue(row, 2);
		debug("Processing : " + sOrganizationCode);
		if (YFCCommon.isVoid(sOrganizationCode)) {
			throw ExceptionUtil.getYFSException("ERR9900016", "OrganizationCode is Void or null");
		}

		YFCDocument yfcDocManageOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inputEle = yfcDocManageOrg.getDocumentElement();

		setOrgHeaderAttributes(row, sOrganizationCode, sParentOrganizationCode, inputEle);

		boolean isVendor = prepareManageOrganizationHierarchyInput(row, sOrganizationCode, inputEle);

		debug("manageOrganizationHierarchy input Doc :: " + yfcDocManageOrg);
		getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", yfcDocManageOrg);

		if (isVendor) {
			applyVendorLogic(row, inputEle);
		}

		String dac = ExcelUtil.getCellvalue(row, 4);
		if (!YFCCommon.isVoid(dac)) {
			String inputManageCustomer = "<Customer CustomerKey='" + dac + "' ExternalCustomerID='" + sOrganizationCode	+ "' />";
			getServiceInvoker().invokeYantraService("GpsDropToDacQ", YFCDocument.getDocumentFor(inputManageCustomer));
		}

		cat.info("Successfully read the Organization: " + sOrganizationCode);

	}

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param isVendor
	 * @param inputEle
	 * @return
	 */
	private boolean prepareManageOrganizationHierarchyInput(Row row, String sOrganizationCode, 
			YFCElement inputEle) {
		boolean isVendor = false;
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 5))){
			setBuyerAttributes(inputEle);
			
		}
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 6))){
			setSellerAttributes(inputEle);
		}
		
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 7))){
			setEnterpriseAttributes(row, sOrganizationCode, inputEle);
			isVendor =  true;
		}
		
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 10))){
			setNodeAttributes(row, inputEle);
		}
		
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 13))){
			setAttribute(inputEle, "CARRIER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		}
		
		setAddressAttributes(row, inputEle);

		if(YFCCommon.isVoid(inputEle.getAttribute(TelstraConstants.LOCALE_CODE))){
			inputEle.setAttribute(TelstraConstants.LOCALE_CODE, TelstraConstants.LOCALE_EN_AU_ACT);
		}
		return isVendor;
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void applyVendorLogic(Row row, YFCElement inputEle) {
		String vSendsCommitment = ExcelUtil.getCellvalue(row, 8);
		String vSendsAsn = ExcelUtil.getCellvalue(row, 9);
		createDefaultVendorNode(inputEle,vSendsCommitment,vSendsAsn);
		if("Y".equalsIgnoreCase(inputEle.getAttribute(TelstraConstants.INVENTORY_PUBLISHED))){
			markConsumableOrganization(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
			createAtpRules(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
		}
	}

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param sParentOrganizationCode
	 * @param inputEle
	 */
	private void setOrgHeaderAttributes(Row row, String sOrganizationCode, String sParentOrganizationCode,
			YFCElement inputEle) {
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.ORGANIZATION_CODE);
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.ORGANIZATION_KEY);
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 1), TelstraConstants.ORGANIZATION_NAME);
		setAttribute(inputEle, sParentOrganizationCode, TelstraConstants.PARENT_ORGANIZATION_CODE);
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 3), TelstraConstants.PRIMARY_ENTERPRISE_KEY);
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void setAddressAttributes(Row row, YFCElement inputEle) {
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 14), "CorporatePersonInfo@FirstName");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 15), "CorporatePersonInfo@AddressLine1");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 16), "CorporatePersonInfo@AddressLine2");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 17), "CorporatePersonInfo@AddressLine3");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 18), "CorporatePersonInfo@City");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 19), "CorporatePersonInfo@State");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 20), "CorporatePersonInfo@ZipCode");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 21), "CorporatePersonInfo@Country");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 22), "CorporatePersonInfo@DayPhone");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 23), "CorporatePersonInfo@HttpUrl");
		
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 14), "BillingPersonInfo@FirstName");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 15), "BillingPersonInfo@AddressLine1");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 16), "BillingPersonInfo@AddressLine2");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 17), "BillingPersonInfo@AddressLine3");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 18), "BillingPersonInfo@City");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 19), "BillingPersonInfo@State");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 20), "BillingPersonInfo@ZipCode");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 21), "BillingPersonInfo@Country");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 22), "BillingPersonInfo@DayPhone");
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 23), "BillingPersonInfo@HttpUrl");
		
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 24), TelstraConstants.LOCALE_CODE);
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void setNodeAttributes(Row row, YFCElement inputEle) {
		setAttribute(inputEle, "NODE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		setAttribute(inputEle, ExcelUtil.getCellvalue(row, 11), "Node@NodeType");
		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 12))){
			setAttribute(inputEle, "Y", "Node@InventoryTracked");
			setAttribute(inputEle, "Y", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, TelstraConstants.ORG_TELSTRA, "InventoryOrganizationCode");
		}
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param inputEle
	 */
	private void setBuyerAttributes(YFCElement inputEle) {
		setAttribute(inputEle, "BUYER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param inputEle
	 */
	private void setSellerAttributes(YFCElement inputEle) {
		setAttribute(inputEle, "SELLER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param inputEle
	 * @return
	 */
	private void setEnterpriseAttributes(Row row, String sOrganizationCode, YFCElement inputEle) {
		setAttribute(inputEle, "ENTERPRISE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		setAttribute(inputEle, "Y", "IsLegalEntity");
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.PRIMARY_ENTERPRISE_KEY);

		if("Y".equalsIgnoreCase(ExcelUtil.getCellvalue(row, 12))){
			setAttribute(inputEle, "Y", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, "Y", "HasOwnInventory");
			setAttribute(inputEle, sOrganizationCode, "InventoryOrganizationCode");
		}else {
			setAttribute(inputEle, "N", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, "N", "HasOwnInventory");
		}
		
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CUSTOMER_MASTER_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.INHERIT_CONFIG_FROM_ENTERPRISE);
	}

	private void createAtpRules(String orgCode) {
		YFCDocument createAtpRulesInputDoc = YFCDocument.getDocumentFor("<AtpRules />");
		YFCElement createAtpRulesInputEle = createAtpRulesInputDoc.getDocumentElement();
		
		String defaultAtpDays = "730";
		String orgATPPostFix = "_ATP";
		
		createAtpRulesInputEle.setAttribute("AccumulationDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("AdvanceNotificationTime", "0");
		createAtpRulesInputEle.setAttribute("AtpRule", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("AtpRuleKey", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("AtpRuleName", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("BackwardConsumptionDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("CallingOrganizationCode", orgCode);
		createAtpRulesInputEle.setAttribute("ConsiderPoForAlloc", "N");
		createAtpRulesInputEle.setAttribute("ForwardConsumptionDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("MaxInventoryHoldDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("ModifyFlag", "Y");
		createAtpRulesInputEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgCode);
		createAtpRulesInputEle.setAttribute("PastDueDemandDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("PastDueSupplyDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("ProcessingTime", "0");
		
		debug("createAtpRulesInputDoc : " + createAtpRulesInputDoc);
		getServiceInvoker().invokeEntityApi("manageAtpRules", createAtpRulesInputDoc, null);	
		
		manageRuleForDefaultATP(orgCode,orgCode+orgATPPostFix);
		
	}

	private void manageRuleForDefaultATP(String orgCode, String atpRuleName) {
		YFCDocument manageRulesDoc = YFCDocument.getDocumentFor("<Rules />");
		YFCElement eRules = manageRulesDoc.getDocumentElement();

		eRules.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgCode);
		eRules.setAttribute("RuleSetFieldDescription", orgCode+"_ATP_RULE");
		eRules.setAttribute("RuleSetFieldName", "ATP_RULE");
		eRules.setAttribute("RuleSetValue", atpRuleName);
		
		debug("manageRule API input : " + manageRulesDoc);
		getServiceInvoker().invokeYantraApi("manageRule", manageRulesDoc);
		
	}

	private void markConsumableOrganization(String sOrganizationKey) {
		YFCDocument inputForMakeConsumableInventoryOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inputForMakeConsumableInventoryOrgEle = inputForMakeConsumableInventoryOrg.getDocumentElement();
		inputForMakeConsumableInventoryOrgEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, TelstraConstants.ORG_TELSTRA);
		setAttribute(inputForMakeConsumableInventoryOrgEle,  sOrganizationKey, "ConsumableOrganizationList/InventoryOrgRelationship@ConsumableInventoryOrg");
		if(!isConsumableInventoryOrgForTELSTRA(sOrganizationKey)){
			debug("inputForMakeConsumableInventoryOrg: " + inputForMakeConsumableInventoryOrgEle);
			getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", inputForMakeConsumableInventoryOrg);
		}
		
	}

	private boolean isConsumableInventoryOrgForTELSTRA(String sOrganizationKey) {
		String inputStringForGetOrganizationList = ""
				+ "<Organization OrganizationCode='"+TelstraConstants.ORG_TELSTRA+"'>"
				+ "		<ConsumableOrganizationList>"
				+ "			<InventoryOrgRelationship ConsumableInventoryOrg='"+sOrganizationKey+"'/>"
				+ "		</ConsumableOrganizationList>"
				+ "</Organization>";
		
		String templateForgetOrgList=""
				+ "<OrganizationList>"
				+ "		<Organization OrganizationCode=''>"
				+ "			<ConsumableOrganizationList>"
				+ "				<InventoryOrgRelationship ConsumableInventoryOrg=''/>"
				+ "			</ConsumableOrganizationList>"
				+ "		</Organization>"
				+ "</OrganizationList>";

		YFCDocument inDocGetOrganizationList = YFCDocument.getDocumentFor(inputStringForGetOrganizationList);
		YFCDocument outputGetOrganizationList = getServiceInvoker().invokeYantraApi("getOrganizationList", inDocGetOrganizationList, YFCDocument.getDocumentFor(templateForgetOrgList));
		YFCElement sOrganization = outputGetOrganizationList.getDocumentElement().getChildElement("Organization");
		if(!YFCElement.isNull(sOrganization)){
			return true;
		}
		return false;
	}


	private void createDefaultVendorNode(YFCElement inputEle, String vSendsCommitment, String vSendsAsn) {
		createVendorOrg(inputEle);
		manageVendor(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE),vSendsCommitment,vSendsAsn);
	}

	private void createVendorOrg(YFCElement orgInputEle) {
		cat.beginTimer("createVendorOrg");
		YFCDocument inDocForCreateVendorOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inEleForCreateVendorOrg = inDocForCreateVendorOrg.getDocumentElement();
		
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_KEY,  orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_KEY)+"_N1");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE)+"_N1");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_NAME, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_NAME)+"_N1 Default Node");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.LOCALE_CODE, 		 orgInputEle.getAttribute(TelstraConstants.LOCALE_CODE));
		
		setAttribute(inEleForCreateVendorOrg, "NODE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inEleForCreateVendorOrg, "VN", "Node@NodeType");	
		setAttribute(inEleForCreateVendorOrg, "N", "Node@InventoryTracked");
		setAttribute(inEleForCreateVendorOrg, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE), TelstraConstants.PARENT_ORGANIZATION_CODE);
		setAttribute(inEleForCreateVendorOrg, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_KEY), TelstraConstants.PRIMARY_ENTERPRISE_KEY);
		
		inEleForCreateVendorOrg.importNode(orgInputEle.getChildElement("BillingPersonInfo",true).cloneNode(true));
		inEleForCreateVendorOrg.importNode(orgInputEle.getChildElement("CorporatePersonInfo").cloneNode(true));
		
		debug("inEleForCreateVendorOrg : \n " + inEleForCreateVendorOrg);
		getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", inDocForCreateVendorOrg);	
		cat.endTimer("createVendorOrg");
	}

	private void manageVendor(String sVendorID, String vSendsCommitment, String vSendsAsn) {
		cat.beginTimer("manageVendor");
		YFCDocument manageVendorInputDoc = YFCDocument.getDocumentFor("<Vendor />");
		YFCElement manageVendorInputEle = manageVendorInputDoc.getDocumentElement();
		manageVendorInputEle.setAttribute("CallingOrganizationCode", TelstraConstants.ORG_TELSTRA_SCLSA);
		manageVendorInputEle.setAttribute("CommitmentTime", "24");
		manageVendorInputEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, TelstraConstants.ORG_TELSTRA_SCLSA);
		manageVendorInputEle.setAttribute("SellerOrganization", "Y");
		manageVendorInputEle.setAttribute("SellerOrganizationCode", sVendorID);
		manageVendorInputEle.setAttribute("SendsCommitment", vSendsCommitment);
		manageVendorInputEle.setAttribute("SendsAsn", vSendsAsn);
		manageVendorInputEle.setAttribute("SendsFuncAck", "N");
		manageVendorInputEle.setAttribute("VendorID", sVendorID);	
		
		debug("manageVendorInput : \n " + manageVendorInputEle);
		getServiceInvoker().invokeYantraApi("manageVendor", manageVendorInputDoc);
		cat.endTimer("manageVendor");
	}

	private  void setAttribute(YFCElement inputEle, String cellvalue,String xpath) {
		setAttribute( inputEle,  cellvalue, xpath,false);
	}
	
	private  void setAttribute(YFCElement inputEle, String cellvalue,String xpath,boolean alwaysCreateNewElement) {
		
		if(xpath.contains("/")){
			int firstEleIndex = xpath.indexOf('/');
			String firstEle=xpath.substring(0, firstEleIndex);
			YFCElement childElement = inputEle.getChildElement(firstEle, true);
			setAttribute(childElement, cellvalue, xpath.substring(firstEleIndex+1),alwaysCreateNewElement);
			
		}else if (xpath.contains("@"))
		{ 
			String[] xpathlast = xpath.split("@");
			String ele = xpathlast[0];			
			String atribute = xpathlast[1];
			YFCElement childElement = null;
			if(alwaysCreateNewElement){
				childElement = inputEle.createChild(ele);
			}
			else{
				childElement = inputEle.getChildElement(ele, true);
			}
			
			childElement.setAttribute(atribute, cellvalue);
		}
		else
		{
			inputEle.setAttribute(xpath, cellvalue);
		}
	}
	
	private void debug(String string) {
		cat.debug(string);
	}

}
