package com.gps.hubble.file;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.agent.integralorder.DacScheduleFileProcessor;
import com.gps.hubble.file.agent.integralorder.MaterialTransportMapFileProcessor;
import com.gps.hubble.file.agent.integralorder.MilkRunFileProcessor;
import com.gps.hubble.file.agent.integralorder.TransportMatrixFileProcessor;
import com.gps.hubble.file.agent.integralorder.WeekNumberFileProcessor;
import com.gps.hubble.file.agent.inventory.InventoryUploadFileProcessor;
import com.gps.hubble.file.agent.inventory.StockMovementReportFileProcessor;
//import com.gps.hubble.file.agent.inventory.InventoryUploadFileProcessor;
import com.gps.hubble.file.agent.orgload.OrgLoadFileProcessor;
import com.yantra.yfs.japi.YFSException;
/**
 * Class description goes here.
 *
 * @version 1.0 30 June 2016
 * @author VGupta
 *
 */
public final class FileProcessorFactory {
	private FileProcessorFactory(){

	}

	/**
	 * 
	 * @param sTransactionType
	 * @param serviceInvoker
	 * @return
	 */
	public static FileProcessor create(String sTransactionType, ServiceInvoker serviceInvoker) {
		FileProcessor fileProcessor = null ;

		if("ORG_LOAD".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new OrgLoadFileProcessor();
		} else  if(TelstraConstants.EXCEL_INVENTORY_UPLOAD_TRANSACTION_TYPE.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new InventoryUploadFileProcessor();
		}	
		else  if(TelstraConstants.EXCEL_STOCK_MOVEMENT_REPORT_UPLOAD_TRANSACTION_TYPE.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new StockMovementReportFileProcessor();
		}else  if("WEEK_NUMBER".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new WeekNumberFileProcessor();
		} else if("MATERIAL_TO_TRANSPORT_MAP".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new MaterialTransportMapFileProcessor();
		} else if("TRANSPORT_MATRIX".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new TransportMatrixFileProcessor();
		} else if("DAC_SCHEDULE".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new DacScheduleFileProcessor();
		} else if("MILK_RUN_SCHEDULE".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new MilkRunFileProcessor();
		}	
		else  if("".equalsIgnoreCase(sTransactionType)){
			fileProcessor = null;
		}

		if(null!= fileProcessor){
			fileProcessor.setServiceInvoker(serviceInvoker);
			return fileProcessor;
		}

		throw new YFSException("FileProcessor Parameter Missing", "FileProcessor Parameter Missing",
				"TRANSACTION_TYPE or TransactionType configuration is required");
	} 
}
