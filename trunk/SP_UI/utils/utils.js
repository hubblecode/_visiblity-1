	var BridgeUtils = new Object();

	var displayNumberFormat =  Intl.NumberFormat('en-AU', { minimumFractionDigits: 2, maximumFractionDigits: 3 })
	var displayQuantityFormat =  Intl.NumberFormat('en-AU', { minimumFractionDigits: 0, maximumFractionDigits: 0 })
	var displayIntFormat =  Intl.NumberFormat('en-AU', { minimumFractionDigits: 0, maximumFractionDigits: 0 })

	BridgeUtils.formatNumberForDisplay = function(serverNumber, usage) {
		var displayFormat
		if ("number" === usage) {
			displayFormat = displayNumberFormat
		} else if("int" == usage) {
			displayFormat = displayIntFormat
		} else {
			displayFormat = displayQuantityFormat
		}
		try {
			var number = Number(serverNumber)
			return displayFormat.format(number)
		} catch(error) {
			console.log(error)
			return serverNumber
		}
	}
    
    BridgeUtils.formatDateForServer = function(date) {
	if (date) {
    	return date.toJSON();
	}
	return "";
	}

	BridgeUtils.formatNumberForServer = function(userNumber) {
		try {
			var cleanNumber = userNumber.replace(/,/g, '')
			return Number.parseFloat(cleanNumber) + ""
		} catch(error) {
			console.log(error)
			return userNumber
		}
	}

	BridgeUtils.getNumber = function(userNumber) {
		if(!userNumber) {
			return
		}
		try {
			var cleanNumber = userNumber.replace(/,/g, '')
			return Number.parseFloat(cleanNumber)
		} catch(error) {
			console.log(error)
			return userNumber
		}
	}

	BridgeUtils.formatForDisplay = function(serverDate) {
		if(serverDate) {
			var date = new Date(serverDate);
			return $.datepicker.formatDate('dd/mm/yy', date);
		}
		return "";
	}

	BridgeUtils.formatForServer = function(displayDate, format) {
		if (displayDate) {
			var date = $.datepicker.parseDate('dd/mm/yy', displayDate);
			if (format === "filter") {
				// For filter we want to get the date as selected by user, ignoring the timezone
				date.setHours(12)
				return date.toJSON().slice(0, 10);
			}
			return date.toJSON();
		}
		return "";
	}

	BridgeUtils.invokeMashup = function(mashupId, success, jsonData) {
		var timeout = function() {
			window.location.href = window.AppInfo.contextroot+"/"+window.AppInfo.loginpage;
		}

		var apiError = function(data) {
			alert(JSON.stringify(data.errors));
		}
		
		var successWrapper = function (data) {
			console.log("Output of call to mashup: %s", mashupId);
			console.log(data);
			var errorcode = data.errorcode;
			if (errorcode) {
				if ( (400 == errorcode) || (401 == errorcode) ) {
					timeout();
				} else {
					apiError(data);
				}
			} else {
				success(data);
			}
		}
		var url = window.AppInfo.contextroot + "/api/" + mashupId
		var ajaxInput = {url: url, success: successWrapper, method: 'POST', dataType: 'json'}
		if (jsonData) {
			var inputData = {}
			inputData[mashupId] = JSON.stringify(jsonData);
			ajaxInput['data'] = inputData;
		}
		$.ajax(ajaxInput);
	}

	BridgeUtils.getValue = function (model, binding) {
		var paths = binding.split('.')
		var displayValue;
		if (paths.length > 1) {
			var currentObj = model
			var bindingAttr = paths.pop()
			paths.forEach( function(path) {
				if (currentObj[path] == undefined) {
					return
				}
				currentObj = currentObj[path]
			})
			displayValue = currentObj[bindingAttr]
		} else {
			displayValue = model[binding]
		}
		return displayValue
	}

	BridgeUtils.setValue = function (model, binding, value) {
		var paths = binding.split('.')
		if (paths.length > 1) {
			var currentObj = model
			var bindingAttr = paths.pop()
			paths.forEach( function(path) {
				if (currentObj[path] == undefined) {
					return
				}
				currentObj = currentObj[path]
			})
			if(value) {
				currentObj[bindingAttr] = value
			} else {
				delete currentObj[bindingAttr]
			}
		} else {
			if(value) {
				model[binding] = value
			} else {
				delete model[binding]
			}
		}
	}

	module.exports = BridgeUtils

