var BridgeDateUtils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.directive('datepicker', {
  twoWay: true,
  params: ['usage'],
  bind: function () {
    var self = this
    if(!this.params.usage) {
      this.params.usage = "datetime"
    }
    $(this.el).datepicker({
    	dateFormat: 'dd/mm/yy',
    	onSelect: function (date) {
        	$(this).change()
          this.focus()
      	}
    }).on("change", function() {
      self.set(BridgeDateUtils.formatForServer(this.value, self.params.usage))
    });

  },
  update: function (val) {
    $(this.el).datepicker('setDate', BridgeDateUtils.formatForDisplay(val))
  },
  unbind: function () {
    $(this.el).off().remove()
  }
});
