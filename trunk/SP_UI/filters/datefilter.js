var BridgeDateUtils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('datefilter', {
  // model -> view
  // formats the value when updating the input element.
  read: function(val) {
    return BridgeDateUtils.formatForDisplay(val)
  },
  // view -> model
  // formats the value when writing to the data.
  write: function(val, oldVal) {
    var valInserverFormat = "";
    try {
      valInserverFormat =  BridgeDateUtils.formatForServer(val)
      BridgeDateUtils.formatForDisplay(valInserverFormat)
    } catch(error) {
      valInserverFormat =  BridgeDateUtils.formatForServer(oldVal)
    }
    return valInserverFormat
  }
})