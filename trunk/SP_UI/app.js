
var Vue = require('vue')
Vue.config.debug = true
var VueRouter = require('vue-router')
Vue.use(VueRouter)

var NotFound = Vue.extend({
    template: '<p>This page is not found!</p>'
})
Vue.component('notfound',NotFound)



var Index = require('./components/Index.vue')
Vue.component('Index',Index)


var GridPagination = require('./components/GridPagination.vue')
Vue.component('gridpagination',GridPagination)

var Vendor = require('./components/VendorList.vue')
Vue.component('vendorlist',Vendor)

var VendorDetails = require('./components/VendorDetails.vue')
Vue.component('vendordetails',VendorDetails )

var OrderList = require('./components/OrderList.vue')
Vue.component('orderlist',OrderList )

var OrderDetails = require('./components/OrderDetails.vue')
Vue.component('orderdetails',OrderDetails )

var Alert = require('./components/AlertList.vue')
Vue.component('alertlist',Alert)

var AlertDetails = require('./components/AlertDetails.vue')
Vue.component('alertdetails',AlertDetails )

var VendorContact = require('./components/VendorContact.vue')
Vue.component('vendorcontact',VendorContact )

var VendorAddressDetails = require('./components/VendorAddressDetails.vue')
Vue.component('vendoraddressdetails',VendorAddressDetails)

var AssignAlertToUser = require('./components/AssignAlertToUser.vue')
Vue.component('assignalerttouser',AssignAlertToUser)

var VendorDeleteAdrContact = require('./components/DelContactConfirm.vue')
Vue.component('vendordeleteadrcontact',VendorDeleteAdrContact)

var Inventory=require('./components/InventoryList.vue')
Vue.component('inventoryList',Inventory )

var FileUpload=require('./components/FileUploadUtil.vue')
Vue.component('fileuploads',FileUpload )

var UploadOthers= require('./components/OtherUploads.vue')
Vue.component ('otherupload',UploadOthers)


require('./directives/datepicker.js')
require('./filters/datefilter.js')
require('./filters/gridfilter.js')
require('./filters/numberfilter.js')
require('./filters/integerfilter.js')
require('./filters/quantityfilter.js')
require('./filters/gridsorter.js')

var App = Vue.extend({})
var router = new VueRouter()

router.map({

	'/home': {
    	component: Index
    },
    '/alertlist': {
    	component:Alert
    },
    '/alertdetails': {
    	component:AlertDetails
    },
    '/vendorlist': {
    	component:Vendor
    },
    '/vendordetails': {
    	component:VendorDetails
    },
    '/inventoryList':{
	component:Inventory
    },
    '/fileuploads':{
	component:FileUpload
	},    
	 '/otherupload': {
		component:UploadOthers  
    },
    '/orderlist': {
    	component:OrderList
    },
     '/orderdetails': {
    	component:OrderDetails
    },	
       '/*any': {
    	component: NotFound
    }
})

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
  jqXHR.setRequestHeader('bridgecsrf', window.AppInfo.bridgecsrftoken);
});

router.start(App, '#app')
