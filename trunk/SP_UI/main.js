// main.js
require('./bootstrap/css/bootstrap.min.css')
// require('exports?window.jquery!./jquery-ui/external/jquery/jquery.js')
// var $ = require('./jquery-ui/external/jquery/jquery.js')
// var jquery = $
// var jQuery = $
// require('imports?jquery=jquery!./jquery-ui/jquery-ui.js')
// require('./jquery-ui/jquery-ui.js')
var $ = require('jquery');
require('jquery-ui-js');
require('jquery-ui-css/jquery-ui.css');
require('jquery-ui-css/jquery.ui.theme.css');
require('chartist/dist/chartist.min.css');

require('./css/app.css')

var Vue = require('vue')
Vue.config.debug = true
var VueRouter = require('vue-router')
Vue.use(VueRouter)

var NotFound = Vue.extend({
    template: '<p>This page is not found!</p>'
})
Vue.component('notfound',NotFound)

var SearchList = require('./components/SearchList.vue')
Vue.component('searchlist',SearchList)

var OrderList = require('./components/OrderList.vue')
Vue.component('orderlist',OrderList)

var ChartList = require('./components/ChartList.vue')
Vue.component('chartlist',ChartList)

var DemoGrid = require('./components/DemoGrid.vue')
Vue.component('demogrid',DemoGrid)

var Barchart = require('./components/Barchart.vue')
Vue.component('barchart',Barchart)

var AlertList = require('./components/AlertList.vue')
Vue.component('alertlist',AlertList)

var Linechart = require('./components/Linechart.vue')
Vue.component('linechart',Linechart)

var Piechart = require('./components/Piechart.vue')
Vue.component('piechart',Piechart)

require('./directives/datepicker.js')
require('./filters/datefilter.js')

var App = Vue.extend({})
var router = new VueRouter()

router.map({
    '/searchlist': {
        component: SearchList
    },
    '/orderlist': {
        component: OrderList
    },
    '/chartlist': {
    	component: ChartList
    },
	 '/alertlist': {
    	component: AlertList
    },
    '/*any': {
    	component: NotFound
    }
})

router.start(App, '#app')
