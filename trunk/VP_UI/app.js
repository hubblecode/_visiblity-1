	
var Vue = require('vue')
Vue.config.debug = true
var VueRouter = require('vue-router')
Vue.use(VueRouter)

var NotFound = Vue.extend({
    template: '<p>This page is not found!</p>'
})
Vue.component('notfound',NotFound)



var Index = require('./components/Index.vue')
Vue.component('Index',Index)


var GridPagination = require('./components/GridPagination.vue')
Vue.component('gridpagination',GridPagination)


var OrderList = require('./components/OrderList.vue')
Vue.component('orderlist',OrderList )

var OrderDetails = require('./components/OrderDetails.vue')
Vue.component('orderdetails',OrderDetails )

var Schedule= require('./components/Schedule.vue')
Vue.component('schedule',Schedule )

var FileUpload=require('./components/FileUploadUtil.vue')
Vue.component('fileuploads',FileUpload )



require('./directives/datepicker.js')
require('./filters/datefilter.js')
require('./filters/gridfilter.js')
require('./filters/numberfilter.js')
require('./filters/integerfilter.js')
require('./filters/quantityfilter.js')
require('./filters/gridsorter.js')

var App = Vue.extend({})
var router = new VueRouter()

router.map({

	'/home': {
    	component: Index
    },

    '/orderlist': {
    	component:OrderList
    },
     '/orderdetails': {
    	component:OrderDetails
    },	
	'/fileuploads':{
	component:FileUpload
	}, 
       '/*any': {
    	component: NotFound
    }
})

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
  jqXHR.setRequestHeader('bridgecsrf', window.AppInfo.bridgecsrftoken);
});

router.start(App, '#app')
