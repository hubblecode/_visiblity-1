var GridUtils = new Object();
var Utils = require("./../utils/utils.js")

GridUtils.PaginationCount = 5
GridUtils.PaginationRowsPerPage = 10

GridUtils.populateGridMeta = function(gmeta, columns, editable) {
	var sortOrders = {}
	columns.forEach(function (key) {
		sortOrders[key] = 1
	})
	gmeta['filter'] = {}
	gmeta['filter']['columns'] = columns
	updateObjectForFilter(gmeta.filter, columns)
	gmeta['sort'] = {}
	gmeta['sort']['sortorder'] = sortOrders
	gmeta['sort']['key'] = ''
	gmeta['sort']['order'] = '1'
	gmeta['sort']['numeric'] = false
	gmeta['pagination'] = {}
	gmeta['pagination']['itemsPerPage'] = GridUtils.PaginationRowsPerPage
	gmeta['pagination']['resultCount'] = 0
	gmeta['pagination']['currentPage'] = 0
	gmeta['pagination']['minimumPageDisplayed'] = 0
	gmeta.pagination.PaginationCount = GridUtils.PaginationCount
	
	if(editable) {
		gmeta['edit'] = {}
		gmeta['edit']['erow'] = {}
		gmeta['edit']['origrow'] = {}
		gmeta['edit']['editing'] = false
	}
}

function updateObjectForFilter(filter, columns) {
	columns.forEach(function (binding) {
	var paths = binding.split('.')
	if (paths.length > 1) {
	  var currentObj = filter
	  var bindingAttr = paths.pop()
	  paths.forEach( function(path) {
	      if (currentObj[path] == undefined) {
	        currentObj[path] = {}
	      }
	      currentObj = currentObj[path]
	  })
	}
	})
}

GridUtils.setPage = function(gmeta, pageNumber, totalPages) {
	if(pageNumber < 0) {
		return
	}
	gmeta.pagination.currentPage = pageNumber
	if (pageNumber < gmeta.pagination.minimumPageDisplayed) {
		gmeta.pagination.minimumPageDisplayed = gmeta.pagination.minimumPageDisplayed -  gmeta.pagination.PaginationCount
	} else if(pageNumber === (totalPages - 1) ) {
		gmeta.pagination.minimumPageDisplayed = pageNumber - (pageNumber % gmeta.pagination.PaginationCount)
	} else if(pageNumber >= (gmeta.pagination.minimumPageDisplayed + gmeta.pagination.PaginationCount) ) {
		gmeta.pagination.minimumPageDisplayed = gmeta.pagination.minimumPageDisplayed + gmeta.pagination.PaginationCount
	}
}

GridUtils.startEdit = function(gmeta, row) {
	gmeta.edit.erow = row
	gmeta.edit.origrow = JSON.parse(JSON.stringify(row))
	gmeta.edit.editing = true
}

GridUtils.doneEdit = function(gmeta, row) {
	gmeta.edit.erow = null
	row.framework.edited = true
}

GridUtils.cancelEdit = function(gmeta, row, columns) {
	columns.forEach(function (key) {
        var origValue = Utils.getValue(gmeta.edit.origrow, key)
        Utils.setValue(row, key, origValue)
//		row[key] = gmeta.edit.origrow[key]
	})
	gmeta.edit.erow = null
	gmeta.edit.origrow = null
}

GridUtils.prepareForEdit = function(gmeta, list) {
	if(!list) {
		return
	}
	list.forEach(function(row) {
		row.framework = {}
	})
}

GridUtils.resetEdit = function(gmeta, list) {
	GridUtils.prepareForEdit(gmeta, list)
}

GridUtils.getModifiedRows = function(gmeta, list) {
	if(!list) {
		return
	}
	var modifiedList = []
	list.forEach(function(row) {
		if(row.framework.edited) {
			modifiedList.push(row)
		}
	})
	return modifiedList
}

GridUtils.isRowBeingEdited = function(gmeta, row) {
	return gmeta.edit.erow == row
}

GridUtils.sortBy = function(gmeta, key, numeric) {
	if(!numeric) {
		numeric = false
	}
	gmeta['sort']['key'] = key
	gmeta['sort']['sortorder'][key] = gmeta['sort']['sortorder'][key] * -1
	gmeta['sort']['order'] = gmeta['sort']['sortorder'][key] 
	gmeta['sort']['numeric'] = numeric
}

GridUtils.paginate = function(gmeta, list, totalPages) {
    if(!list || list.length == 0) {
        gmeta['pagination']['resultCount'] = 0
        gmeta['pagination']['currentPage'] = 0
        gmeta['pagination']['minimumPageDisplayed'] = 0
        return list
    }
	gmeta['pagination']['resultCount'] = list.length
	if (gmeta['pagination']['currentPage'] >= totalPages) {
		if (totalPages == 0) {
			gmeta['pagination']['currentPage'] = 0
			gmeta['pagination']['minimumPageDisplayed'] = 0
		} else {
			var pageNumber = totalPages - 1
			gmeta['pagination']['currentPage'] = pageNumber
			gmeta['pagination']['minimumPageDisplayed'] = pageNumber - (pageNumber % gmeta.pagination.PaginationCount)
		}
	}
	var index = gmeta['pagination']['currentPage'] * gmeta['pagination']['itemsPerPage']
	var filteredList = list.slice(index, index + gmeta['pagination']['itemsPerPage'])
	return filteredList
}

module.exports = GridUtils
