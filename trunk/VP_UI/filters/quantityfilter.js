var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('quantityfilter', {
  // model -> view
  // formats the value when updating the input element.
  read: function(val) {
    if(!val) {
      return
    }
    return Utils.formatNumberForDisplay(val, 'quantity')
  },
  // view -> model
  // formats the value when writing to the data.
  write: function(val, oldVal) {
    if(!val) {
      return
    }
    return Utils.formatNumberForServer(val)
  }
})