package com.gps.hubble.ui.mashup;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.mashup.Mashup;

public class OrderListMashup extends Mashup {

	@Override
	public JSONObject massageOutput(JSONObject output) {
		JSONObject massagedOutput = output;
		String loginId= super.getUIContext().getLoginid();
		
		JSONArray orderList= (JSONArray)massagedOutput.get("Order");
	    //       System.out.print(orderList);
		if(orderList!=null && orderList.size()>0) {
			JSONObject orderObj=null;
			for(Iterator<JSONObject>itr=orderList.iterator();itr.hasNext();){
				 orderObj=itr.next();
		     // System.out.println(orderObj);
				JSONObject extnObj= (JSONObject)orderObj.get("Extn");
				
				if(extnObj !=null) {
				//	  System.out.println(extnObj);
					JSONObject watchedOrders=(JSONObject)extnObj.get("WatchedOrderList");
				//	  System.out.println(watchedOrders);
					if(watchedOrders!=null) {
						JSONArray watchedOrderList=(JSONArray)watchedOrders.get("WatchedOrder");
						JSONObject watchedOrder=null;
						boolean found=false;
						if(watchedOrderList!=null && watchedOrderList.size()>0) {
							for(Iterator<JSONObject>itr1=watchedOrderList.iterator();itr1.hasNext();) {
								watchedOrder=itr1.next();
								String orderlogin=(String)watchedOrder.get("Loginid");
								if(orderlogin!=null && loginId.equals(orderlogin)) {
									orderObj.put("IsWatchedByUser","Y");
									//System.out.println("Y");
									found=true;
									break;
								}
								
							}
						}
						if(!found) {
							orderObj.put("IsWatchedByUser","N");
						//	System.out.println("N");
						}
					}
				}
				
				
			
			}	
		}
			
			
		
			
		
		
		return super.massageOutput(massagedOutput);
		
}

}
