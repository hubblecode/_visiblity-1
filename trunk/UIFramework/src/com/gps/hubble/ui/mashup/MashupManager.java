package com.gps.hubble.ui.mashup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gps.hubble.ui.utils.UIContext;
import com.gps.hubble.ui.utils.XMLUtils;

public class MashupManager {

	private static final String MASHUP_ID_MANAGE_TEMPLATE = "manageApiTemplate";
	private static final String MASHUP_ELEM_NAME = "mashup";
	private static int counter = 0;
	private static Map<Integer, String> registeredFiles = new HashMap<Integer, String>();
	private static Map<String, MashupDef> mashups = new HashMap<String, MashupDef>();
	private static final Logger logger = LogManager.getLogger();
	public static final String appshortcode = "bridgescv";
	public static final String defaultOrg = "DEFAULT";
	
	public static void registerMashupFile(String mashupFile) {
		registeredFiles.put(counter, mashupFile);
		counter++;
	}
	
	public static void loadMashups() {
		for (int i = 0; i < counter; i++) {
			loadMashup(registeredFiles.get(i));
		}
		logger.info("Total Mashups loaded: {}", mashups.size());
	}

	private static void loadMashup(String file) {
		Document mashupsDoc = XMLUtils.createDocumentFromFile(file);
		List<Element> mashupDefList = XMLUtils.getChildElements(MASHUP_ELEM_NAME, mashupsDoc.getDocumentElement());
		for (Element mashupElem : mashupDefList) {
			try {
				MashupDef mashupDef = new MashupDef(mashupElem);
				mashupDef.createMashupImpl();
				mashups.put(mashupDef.getId(), mashupDef);
			} catch (Exception e) {
				logger.catching(e);
			}
		}
	}

	public static MashupDef getMashupDef(String mashupId) {
		return mashups.get(mashupId);
	}

	public static void syncMashupTemplates(UIContext context) {
		Set<String> mashupIdSet = mashups.keySet();
		for (String mashupId : mashupIdSet) {
			MashupDef mashupDef = getMashupDef(mashupId);
			if (mashupDef.isAggregator() || MASHUP_ID_MANAGE_TEMPLATE.equals(mashupDef.getId())) {
				continue;
			}
			
			if (mashupDef.getTemplateId() == null) {
				continue;
			}

			Document template = mashupDef.getTemplate();
			MashupDef manageTemplateMashupDef = getMashupDef(MASHUP_ID_MANAGE_TEMPLATE);
			Document manageTemplateInput = XMLUtils.clone(manageTemplateMashupDef.getInput());
			manageTemplateInput.getDocumentElement().setAttribute("TemplateId", mashupDef.getTemplateId());
			manageTemplateInput.getDocumentElement().setAttribute("ApiName", mashupDef.getAPIName());
			XMLUtils.copyElement(XMLUtils.getChildElement("TemplateData", manageTemplateInput.getDocumentElement()), template.getDocumentElement());
			APIInvoker.invoke(context, null, XMLUtils.getString(manageTemplateInput), manageTemplateMashupDef.getAPIName(), manageTemplateMashupDef.isService(), false);
		}
	}
}
