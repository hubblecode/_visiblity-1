package com.gps.hubble.ui.mashup;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.Constants;
import com.gps.hubble.ui.utils.UIContext;

public class APIInvoker {

//	private static final String scheme = "http";
	// Shravan
//	private static final String restUrl = scheme+"://192.168.10.237:7001/smcfs/restapi";
	// Vinay
//	private static final String restUrl = scheme+"://192.168.10.185:8100/smcfs/restapi";
	private static final String restUrl = AppInfo.getRemoteserverurl();
	private static final String restApiUrl = restUrl+"/invoke";
	private static final String loginApiUrl = restUrl+"/invoke/login";
	private static final String restServiceUrl = restUrl+"/executeFlow";
	private static final int CONNECT_TIMEOUT = 2000;
	private static final int REQUEST_TIMEOUT = 10000;
	private static final String MAX_CONNECTIONS = "10";
	private static final Logger logger = LogManager.getLogger();

	static {
		System.setProperty("http.maxConnections", MAX_CONNECTIONS);
	}

	public static String invoke(UIContext context, MashupDef mashupDef, String input, String apiName, boolean isService, boolean useJson) {
		HttpURLConnection connection = getURLConnection(context, mashupDef, apiName, isService, useJson);
		postData(connection, input, context);
		if (context.isRemoteServerUnreachable()) {
			return null;
		}
		String output = readData(connection, context);
		return output;
	}

	private static String readData(HttpURLConnection connection, UIContext context) {
		int respCode = -1;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String decodedString;
			StringBuilder outputBuilder = new StringBuilder();
			while ((decodedString = in.readLine()) != null) {
				outputBuilder.append(decodedString);
			}
			in.close();
			String output = outputBuilder.toString();
			return output;
		} catch (IOException e) {
			try {
				respCode = connection.getResponseCode();
				context.setRemoteServerErrorStatusCode(respCode);
				InputStream es = connection.getErrorStream();
				// read the response body
				byte[] buf = new byte[1028];
				while (es.read(buf) > 0) {
					logger.error(new String(buf));
				}
				es.close();
			} catch(IOException ex) {
			}
		}
		return null;
	}

	private static void postData(HttpURLConnection connection, String input, UIContext context) {
		try {
			BufferedOutputStream oStream = new BufferedOutputStream(connection.getOutputStream());
			oStream.write(input.getBytes("UTF-8"));
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			logger.catching(e);
			context.setRemoteServerUnreachable(true);
		}
	}

	private static HttpURLConnection getURLConnection(UIContext context, MashupDef mashupDef, String apiName, boolean isService, boolean useJson) {
		try {
			HttpURLConnection connection = null;
			URL url = null;
			if ("login".equalsIgnoreCase(apiName)) {
				url = new URL(loginApiUrl);
			} else if (isService) {
				String finalUrl = createUrl(restServiceUrl, mashupDef, apiName, context);
				url = new URL(finalUrl);
			} else {
				String finalUrl = createUrl(restApiUrl, mashupDef, apiName, context);
				url = new URL(finalUrl);
			}
			connection = (HttpURLConnection) url.openConnection();
			setupConnection(connection, context, useJson);
			return connection;
		} catch(Exception e) {
			logger.catching(e);
		}
		return null;
	}

	private static void setupConnection(HttpURLConnection connection, UIContext context, boolean useJson) throws Exception {
		connection.setConnectTimeout(CONNECT_TIMEOUT);
		connection.setReadTimeout(REQUEST_TIMEOUT);
		if (useJson) {
			connection.addRequestProperty("Accept", Constants.jsonContentType);
			connection.addRequestProperty("Content-Type", Constants.jsonContentType);
		} else {
			connection.addRequestProperty("Accept", Constants.xmlContentType);
			connection.addRequestProperty("Content-Type", Constants.xmlContentType);
		}
		connection.setRequestMethod("POST");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
	}

	private static String createUrl(String url, MashupDef mashupDef, String apiName, UIContext context) throws UnsupportedEncodingException {
		String token = context.getToken();
		String loginid = context.getLoginid();
		StringBuilder builder = new StringBuilder(url);
		builder.append("/");
		builder.append(apiName);
		builder.append("?_loginid=");
		builder.append(loginid);
		builder.append("&_token=");
		builder.append(token);
		if (mashupDef != null && mashupDef.getTemplateId() != null) {
			builder.append("&_templateId=");
			builder.append(URLEncoder.encode(mashupDef.getTemplateId(), "UTF-8"));
		}
		return builder.toString();
	}

}
