package com.gps.hubble.ui.mashup;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.w3c.dom.Element;

import com.gps.hubble.ui.utils.MashupUtils;
import com.gps.hubble.ui.utils.UIContext;

public class AggregatorMashup implements IMashup {

	private static final String ATTR_REF_ID = "id";
	private MashupDef mashupDef = null;
	private UIContext uiContext = null;
	private static final Logger logger = LogManager.getLogger();

	@Override
	public JSONObject massageInput(JSONObject input) {
		return input;
	}

	@Override
	public JSONObject massageOutput(JSONObject output) {
		return output;
	}

	@SuppressWarnings("unchecked")
	@Override
	public final JSONObject execute(JSONObject input) {
		if (uiContext.isTracing()) {
			logger.info("Aggregator: Invoking aggregate mashup: {}", mashupDef.getId());
		}
		ArrayList<Element> mashupRefs = mashupDef.getMashupRefs();
		JSONObject output = new JSONObject();
		for (Iterator<Element> iterator = mashupRefs.iterator(); iterator.hasNext();) {
			Element ref = iterator.next();
			String refMashupId = ref.getAttribute(ATTR_REF_ID);
			if (uiContext.isTracing()) {
				logger.info("Aggregator: Invoking mashup ref: {}", refMashupId);
			}
			JSONObject refInput = (JSONObject) input.get(refMashupId);
			JSONObject refOutput = MashupUtils.invokeMashup(refMashupId, refInput, uiContext);
			if (refOutput != null) {
				output.put(refMashupId, refOutput);
			}
		}
		
		return output;
	}

	@Override
	public MashupDef getMashupDef() {
		return mashupDef;
	}

	@Override
	public void setMashupDef(MashupDef mashupDef) {
		this.mashupDef = mashupDef;
	}

	@Override
	public void setUIContext(UIContext context) {
		this.uiContext = context;
	}

	@Override
	public UIContext getUIContext() {
		return uiContext;
	}

	
}
