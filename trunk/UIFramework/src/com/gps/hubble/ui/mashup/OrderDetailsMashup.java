package com.gps.hubble.ui.mashup;                

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.mashup.Mashup;

public class OrderDetailsMashup extends Mashup {

	@Override
	public JSONObject massageOutput(JSONObject output) {
		JSONObject massagedOutput = output;
		String loginId= super.getUIContext().getLoginid();
		JSONObject extnObj= (JSONObject)massagedOutput.get("Extn");
		
		if(extnObj !=null) {
			JSONObject watchedOrders=(JSONObject)extnObj.get("WatchedOrderList");
			if(watchedOrders!=null) {
				JSONArray watchedOrderList=(JSONArray)watchedOrders.get("WatchedOrder");
				JSONObject watchedOrder=null;
				boolean found=false;
				if(watchedOrderList!=null && watchedOrderList.size()>0) {
					for(Iterator<JSONObject>itr=watchedOrderList.iterator();itr.hasNext();) {
						watchedOrder=itr.next();
						String orderlogin=(String)watchedOrder.get("Loginid");
						if(orderlogin!=null && loginId.equals(orderlogin)) {
							massagedOutput.put("IsWatchedByUser","Y");
							found=true;
							break;
						}
						
					}
				}
				if(!found) {
					massagedOutput.put("IsWatchedByUser","N");
				}
			}
		}
		
		return super.massageOutput(massagedOutput);
		
	}

}
