package com.gps.hubble.ui.mashup;

import org.json.simple.JSONObject;

import com.gps.hubble.ui.utils.UIContext;

public interface IMashup {
	
	public JSONObject massageInput(JSONObject input);
	
	public JSONObject massageOutput(JSONObject output);
	
	public JSONObject execute(JSONObject input);
	
	public MashupDef getMashupDef();
	
	public void setMashupDef(MashupDef mashupDef);

	public void setUIContext(UIContext context);
	
	public UIContext getUIContext();
}
