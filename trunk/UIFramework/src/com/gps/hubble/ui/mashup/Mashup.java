package com.gps.hubble.ui.mashup;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.Cookie;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utils.UIContext;

public class Mashup implements IMashup {

	private MashupDef mashupDef = null;
	private UIContext uiContext = null;
	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public JSONObject massageInput(JSONObject input) {
		return input;
	}

	@Override
	public JSONObject massageOutput(JSONObject output) {
		return output;
	}

	@Override
	public final JSONObject execute(JSONObject jsonInput) {
		if (jsonInput == null) {
			jsonInput = new JSONObject();
		}
		if (!mashupDef.isAggregator()) {
			updateWithInputDataDef(jsonInput);
			updateWithHeaderDataDef(jsonInput);
			updateWithConstDataDef(jsonInput);
		}

		if (uiContext.isTracing()) {
			String input = jsonInput.toJSONString();
			logger.info("API Input after munging mashup: {} input: {}", mashupDef.getId(), input);
		}

		JSONObject massagedInput = massageInput(jsonInput);
		if (mashupDef.isRemoveEmptyAttributes()) {
			removeEmptyAttributesAndElements(massagedInput);
		}
		JSONObject output = executeInternal(massagedInput);
		if (output != null) {
			JSONObject massagedOutput = massageOutput(output);
			if (uiContext.isTracing()) {
				String outputStr = massagedOutput.toJSONString();
				logger.info("API Output after munging mashup: {} output: {}", mashupDef.getId(), outputStr);
			}
			return massagedOutput;
		}
		return null;
	}
	
	private boolean removeEmptyAttributesAndElements(JSONObject input) {
		boolean childrenFound = false;
		Object[] keys = input.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			String key = (String) keys[i];
			Object value = input.get(key);
			if(value instanceof JSONObject) {
				childrenFound = removeEmptyAttributesAndElements((JSONObject)value);
				if (!childrenFound) {
					input.remove(key);
				}
			} else if (value instanceof JSONArray) {
				childrenFound = removeEmptyAttributesAndElements((JSONArray)value);
				if (!childrenFound) {
					input.remove(key);
				}
			} else if (value instanceof String) {
				if("".equalsIgnoreCase((String)value)) {
					input.remove(key);
				} else {
					childrenFound = true;
				}
			}
		}
		return childrenFound;
	}

	private boolean removeEmptyAttributesAndElements(JSONArray value) {
		for (Iterator iterator = value.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if(object instanceof JSONObject) {
				removeEmptyAttributesAndElements((JSONObject) object);
			}
		}
		return true;
	}

	private void updateWithConstDataDef(JSONObject jsonInput) {
		HashMap<String, String> constdataDef = mashupDef.getConstDataDef();
		if (constdataDef == null) {
			return;
		}
		for (String path : constdataDef.keySet()) {
			String data = constdataDef.get(path);
			updateJsonObject(jsonInput, path, data);
		}
	}
	
	private void updateWithHeaderDataDef(JSONObject jsonInput) {
		HashMap<String, String> dataDef = mashupDef.getHeaderDataDef();
		if (dataDef == null) {
			return;
		}
		for (String path : dataDef.keySet()) {
			String dataKey = dataDef.get(path);
			String data = getUserDataUsingHeaderDef(dataKey);
			updateJsonObject(jsonInput, path, data);
		}
	}

	private void updateWithInputDataDef(JSONObject jsonInput) {
		HashMap<String, String> dataDef = mashupDef.getInputDataDef();
		if (dataDef == null) {
			return;
		}
		for (String path : dataDef.keySet()) {
			String dataKey = dataDef.get(path);
			String data = getUserDataUsingDataDef(dataKey);
			updateJsonObject(jsonInput, path, data);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateJsonObject(JSONObject jsonInput, String path, String data) {
		if (uiContext.isTracing()) {
			logger.info("Updating input json using path: {} data: {}", path, data);
		}
		
		String[] paths = path.split("\\.");
		if (paths == null || paths.length == 1) {
			if (!jsonInput.containsKey(path)) {
				jsonInput.put(path, data);
			}
			return;
		}
		JSONObject currentObject = jsonInput;
		int i = 0;
		for (i = 0; i < paths.length - 1; i++) {
			JSONObject pathObject = (JSONObject) currentObject.get(paths[i]);
			if (pathObject == null) {
				pathObject = new JSONObject();
				currentObject.put(paths[i], pathObject);
			}
			currentObject = pathObject;
		}
		if (!currentObject.containsKey(paths[paths.length - 1])) {
			currentObject.put(paths[paths.length - 1], data);
		}
	}

	private String getUserDataUsingDataDef(String dataDefKey) {
		return uiContext.getRequest().getParameter(dataDefKey);
	}

	private String getUserDataUsingHeaderDef(String dataDefKey) {
		String data = uiContext.getRequest().getHeader(dataDefKey);
		if (data != null) {
			return data;
		}
		Cookie[] cookies = uiContext.getRequest().getCookies();
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if(dataDefKey.equals(cookie.getName())) {
				data = cookie.getValue();
			}
		}
		return data;
	}

	private JSONObject executeInternal(JSONObject input) {
		if (mashupDef.isPrototype()) {
			logger.error("Invoking mashup {} in prototype mode.", mashupDef.getId());
			String fileName = "/prototype/"+mashupDef.getId()+".json";
			InputStream fileStream = Mashup.class.getResourceAsStream(fileName);
			JSONParser parser = new JSONParser();
			try {
				Object output = parser.parse(new InputStreamReader(fileStream, "UTF-8"));
				JSONObject jsonOutput = (JSONObject) output;
				return jsonOutput;
			} catch (Exception e) {
				logger.catching(e);
			}
			return null;
		}
		String output =  APIInvoker.invoke(uiContext, mashupDef, input.toJSONString(), mashupDef.getAPIName(), mashupDef.isService(), true);
		if (output == null) {
			return null;
		}
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonOutput = (JSONObject) parser.parse(output);
			return jsonOutput;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public MashupDef getMashupDef() {
		return mashupDef;
	}

	@Override
	public void setMashupDef(MashupDef mashupDef) {
		this.mashupDef = mashupDef;
	}

	@Override
	public void setUIContext(UIContext context) {
		this.uiContext = context;
	}

	@Override
	public UIContext getUIContext() {
		return uiContext;
	}

}
