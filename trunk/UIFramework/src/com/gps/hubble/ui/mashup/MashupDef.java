package com.gps.hubble.ui.mashup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.gps.hubble.ui.utils.Utils;
import com.gps.hubble.ui.utils.XMLUtils;

public class MashupDef {

	private static final String MUNGE_CONST_DATA = "const:";
	private static final String MUNGE_DYNAMIC_DATA = "data:";
	private static final String MUNGE_HEADER_DATA = "header:";
	private static final String ATTR_SEQUENCE = "sequence";
	private static final String PROTOTYPE_ATTR = "prototype";
	private static final String INTERNAL_ATTR = "internal";
	private static final String AGGREGATOR_ATTR = "aggregator";
	private static final String ID_ATTR = "id";
	private static final String INPUT_ELEM_NAME = "Input";
	private static final String ISSERVICE_ATTR = "service";
	private static final String TEMPLATE_ELEM_NAME = "Template";
	private static final String API_NAME_ATTR = "name";
	private static final String API_ELEM_NAME = "API";
	private static final String CLASS_NAME_ATTR = API_NAME_ATTR;
	private static final String REMOVE_EMPTY_ATTR = "removeEmptyAttributes";
	private static final String CLASS_INFORMATION_ELEM_NAME = "classInformation";
	
	private String id = null;
	private String className = null;
	private boolean isService = false;
	private String apiName = null;
	private Document input = null;
	private HashMap<String, String> inputDataDef = null;
	private HashMap<String, String> constDataDef = null;
	private HashMap<String, String> headerDataDef = null;
	private Document template = null;
	private IMashup mashup = null;
	private ArrayList<Element> mashupRefs = null;
	private boolean aggregator = false;
	private String templateId = null;
	private boolean isPrototype = false;
	private boolean isInternal = false;
	private boolean removeEmptyAttributes = false;
	
	private static final Logger logger = LogManager.getLogger();
	
	public	MashupDef(Element mashup) {
		//mashup Id
		this.id = mashup.getAttribute(ID_ATTR);
		logger.info("Parsing Mashup def for id: {}", this.id);
		this.aggregator = Utils.getBooleanValue(mashup.getAttribute(AGGREGATOR_ATTR));
		this.isPrototype = XMLUtils.getBooleanAttribute(mashup, PROTOTYPE_ATTR, false);
		this.isInternal = XMLUtils.getBooleanAttribute(mashup, INTERNAL_ATTR, false);
		this.removeEmptyAttributes = XMLUtils.getBooleanAttribute(mashup, REMOVE_EMPTY_ATTR, false);

		//Class information
		Element classInfo = XMLUtils.getChildElement(CLASS_INFORMATION_ELEM_NAME, mashup);
		
		if (classInfo == null) {
			logger.error("Class Info not found for Mashup {}.", id);
		}
		this.className = classInfo.getAttribute(CLASS_NAME_ATTR);
		
		if (aggregator) {
			readAggregatorMashupDef(mashup);
		} else {
			readMashupDef(mashup);
		}
	}

	public boolean isRemoveEmptyAttributes() {
		return removeEmptyAttributes;
	}

	private void readAggregatorMashupDef(Element mashup) {
		ArrayList<Element> refList = XMLUtils.getChildElements("ref", mashup);
		Collections.sort(refList, new Comparator<Element>() {
			@Override
			public int compare(Element ref1, Element ref2) {
				int sequence1 = XMLUtils.getIntAttribute(ref1, ATTR_SEQUENCE, -1);
				int sequence2 = XMLUtils.getIntAttribute(ref2, ATTR_SEQUENCE, -1);
				if (sequence1 < sequence2) {
					return -1;
				} else if (sequence1 == sequence2) {
					return 0;
				}
				return 1;
			}
		});
		mashupRefs = refList;
	}

	private void readMashupDef(Element mashup) {
		Element api = XMLUtils.getChildElement(API_ELEM_NAME, mashup);
		
		if (api == null) {
			logger.error("API Element not found for Mashup {}.", id);
		}
		
		this.apiName = api.getAttribute(API_NAME_ATTR) ;
		this.isService = XMLUtils.getBooleanAttribute(api, ISSERVICE_ATTR, false);
		
		Element inputElem = XMLUtils.getChildElement(INPUT_ELEM_NAME, api);
		if (inputElem != null) {
			List<Element> childElemList = XMLUtils.getChildElements(inputElem);
			if (childElemList != null && childElemList.size() > 0) {
				this.input = XMLUtils.createDocumentFromElement(childElemList.get(0));
				parseInputForData(this.input.getDocumentElement(), null);
			}
		}
		
		Element templateElem = XMLUtils.getChildElement(TEMPLATE_ELEM_NAME, api);
		if (templateElem == null) {
			logger.error("Template not defined for Mashup {}.", id);
		} else {
			List<Element> childElemList = XMLUtils.getChildElements(templateElem);
			if (childElemList != null && childElemList.size() > 0) {
				this.templateId = MashupManager.appshortcode+this.id;
				this.template = XMLUtils.createDocumentFromElement(childElemList.get(0));
			}
		}
	}

	private void parseInputForData(Element element, String path) {
		NamedNodeMap attributesMap = element.getAttributes();
		int attrbutesCount = attributesMap.getLength();
		for (int i = 0; i < attrbutesCount; i++) {
			Node node = attributesMap.item(i);
			String value = node.getNodeValue();
			if (value != null && value != "") {
				int index = value.indexOf(MUNGE_DYNAMIC_DATA);
				if (index != -1) {
					String attrPath = node.getNodeName();
					if (path != null) {
						attrPath = path+"."+attrPath;
					}
					String dataKey = value.substring(MUNGE_DYNAMIC_DATA.length());
					if (inputDataDef == null) {
						inputDataDef = new HashMap<String, String>();
					}
					inputDataDef.put(attrPath, dataKey);
					continue;
				}
				index = value.indexOf(MUNGE_CONST_DATA);
				if (index != -1) {
					String attrPath = node.getNodeName();
					if (path != null) {
						attrPath = path+"."+attrPath;
					}
					String dataKey = value.substring(MUNGE_CONST_DATA.length());
					if (constDataDef == null) {
						constDataDef = new HashMap<String, String>();
					}
					constDataDef.put(attrPath, dataKey);
					continue;
				}
				index = value.indexOf(MUNGE_HEADER_DATA);
				if (index != -1) {
					String attrPath = node.getNodeName();
					if (path != null) {
						attrPath = path+"."+attrPath;
					}
					String dataKey = value.substring(MUNGE_HEADER_DATA.length());
					if (headerDataDef == null) {
						headerDataDef = new HashMap<String, String>();
					}
					headerDataDef.put(attrPath, dataKey);
					continue;
				}
			}
		}
		List<Element> childElemList = XMLUtils.getChildElements(element);
		if (childElemList != null) {
			for (Iterator<Element> iterator = childElemList.iterator(); iterator.hasNext();) {
				Element childElem = iterator.next();
				String childElemPath = childElem.getTagName();
				if (path != null) {
					childElemPath = path + "." + childElemPath;
				}
				parseInputForData(childElem, childElemPath);
			}
		}
	}

	public void createMashupImpl() {
		try {
			Class<?> implClass = Class.forName(className);
			mashup = (IMashup) implClass.newInstance();
			mashup.setMashupDef(this);
		} catch (Exception e) {
			logger.error("Exception while creating mahup class: Mashup: {} classname: {}.", id, className);
			logger.catching(e);
		}
	}

	public IMashup getMashup() {
		return mashup;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Document getTemplate() {
		return template;
	}

	public Document getInput() {
		return input;
	}
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public boolean isService() {
		return isService;
	}

	public boolean isPrototype() {
		return isPrototype;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public String getAPIName() {
		return apiName;
	}

	public HashMap<String, String> getInputDataDef() {
		return inputDataDef;
	}

	public HashMap<String, String> getConstDataDef() {
		return constDataDef;
	}

	public HashMap<String, String> getHeaderDataDef() {
		return headerDataDef;
	}

	public boolean isAggregator() {
		return aggregator;
	}

	public ArrayList<Element> getMashupRefs() {
		return mashupRefs;
	}

	public String getTemplateId() {
		return templateId;
	}
	
}
