package com.gps.hubble.ui;

public final class Constants {

	public static final String jsonContentType = "application/json";
	public static final String xmlContentType = "application/xml";
	public static final String INIT_SERVLETS_MAPPING = "/init/";
	public static final String TOKEN_COOKIE_NAME = "bridgetoken";
	public static final String LOGIN_COOKIE_NAME = "bridgeloginid";
	public static final String TRACE_COOKIE_NAME = "bridgetrace";
	public final static String CSRF_COOKIE_NAME = "bridgecsrf";
	public final static String ORG_NAME = "currentorg";
}
