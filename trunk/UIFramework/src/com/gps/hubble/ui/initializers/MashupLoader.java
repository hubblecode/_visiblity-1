package com.gps.hubble.ui.initializers;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.mashup.MashupManager;

/**
 * Servlet implementation class MashupLoader
 */
@WebServlet(urlPatterns = {"/init/MashupLoader"}, loadOnStartup = 100)
public class MashupLoader extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MashupLoader() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("Mashup loading started.");
		MashupManager.loadMashups();
	}
}
