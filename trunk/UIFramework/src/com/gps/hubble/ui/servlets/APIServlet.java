package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.mashup.MashupDef;
import com.gps.hubble.ui.mashup.MashupManager;
import com.gps.hubble.ui.utils.MashupUtils;
import com.gps.hubble.ui.utils.UIContext;

/**
 * Servlet implementation class APIServlet
 */
@WebServlet("/api/*")
public class APIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	private static final String jsonContentType = "application/json";
	
    /**
     * Default constructor. 
     */
    public APIServlet() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO remove doGet once in production
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UIContext context = UIContext.getUIContext(request, response);
		boolean tracing = context.isTracing();
		String mashupId = MashupUtils.getMashupId(context);
		
		if (tracing) {
			logger.info("Mashup being invoked: {}.",  mashupId);
		}
		MashupDef mashupDef = MashupManager.getMashupDef(mashupId);
		if (mashupDef != null && mashupDef.isInternal()) {
			logger.error("Internal mashup {} being invoked from the API Servlet by loginid {}", mashupDef.getId(), context.getLoginid());
			context.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
			response.setStatus(context.getRemoteServerErrorStatusCode());
			return;
		}
		
		String jsonInput = MashupUtils.getMashupInput(context, mashupId);
		if (tracing) {
			logger.info("Mashup input: {}", jsonInput);
		}
		JSONObject output = MashupUtils.invokeMashup(mashupId, MashupUtils.getJSONObject(jsonInput), context);
		if (context.isRemoteServerUnreachable()) {
			if (tracing) {
				logger.info("Remote server unreachable");
			}
			// TODO handle errors
			response.setStatus(0);
		} else if (context.getRemoteServerErrorStatusCode() != -1) {
			if (tracing) {
				logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
			}
			// TODO handle errors
			response.setStatus(context.getRemoteServerErrorStatusCode());
		} else {
		    
			String outputStr = "";
			if(output !=null) {
			  outputStr= output.toJSONString();
			} else {
			  output = new JSONObject();
			  output.put("Result", "ApiSuccess");
			  outputStr= output.toJSONString();
			}
			if (tracing) {
				logger.info("Output of Mashup: {}", outputStr);
			}
			response.setContentType(jsonContentType);
			response.setCharacterEncoding("UTF-8");
			response.getWriter().append(outputStr);
		}
	}

}
