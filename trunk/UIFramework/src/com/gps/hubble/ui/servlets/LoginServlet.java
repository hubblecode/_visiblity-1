package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.utils.MashupUtils;
import com.gps.hubble.ui.utils.UIContext;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	@Override
	public void init() throws ServletException {
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served by login servlet: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UIContext context = UIContext.getUIContext(request, response);
		boolean tracing = context.isTracing();
		JSONObject loginInput = new JSONObject();
//		loginInput.put("LoginID", request.getParameter("LoginID"));
//		loginInput.put("Password", request.getParameter("Password"));
		JSONObject output = MashupUtils.invokeMashup("login", loginInput, context);
		
		if (context.getRemoteServerErrorStatusCode() != -1) {
			if(tracing) {
				logger.error("Invalid user password combination.");
			}
			response.getWriter().append("Invalid user password combination.");
			return;
		} else if (context.isRemoteServerUnreachable()) {
			if(tracing) {
				logger.error("Remote server Unreachable.");
			}
			response.getWriter().append("Remote server Unreachable.");
			return;
		}
		
		context.setLoginid((String) output.get("LoginID"));
		context.setToken((String) output.get("UserToken"));
		context.setUserOrgCode((String)output.get("OrganizationCode"));
		
		context.createCSRFToken();
//		response.getWriter().append("Served by login servlet: ").append(request.getContextPath());
//		response.getWriter().append("CSRF Token is: ").append(context.getCsrfToken());
		response.setStatus(HttpURLConnection.HTTP_MOVED_TEMP);
		String url = context.getRedirectUrl(AppInfo.getHomepage());
		url = url.concat(AppInfo.getIndex()) ;
		response.sendRedirect(url);
	}

}
