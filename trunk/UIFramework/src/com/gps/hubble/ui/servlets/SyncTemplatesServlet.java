package com.gps.hubble.ui.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.mashup.MashupManager;
import com.gps.hubble.ui.utils.UIContext;

/**
 * Servlet implementation class SyncTemplatesServlet
 */
@WebServlet("/api/synctemplates")
public class SyncTemplatesServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyncTemplatesServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("Template sync called.");
		UIContext context = UIContext.getUIContext(request, response);
		MashupManager.syncMashupTemplates(context);
		response.getWriter().append("Template sync completed.");
	}

}
