package com.gps.hubble.ui.servlets.filehandler;

import java.io.IOException;

import java.io.InputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.simple.JSONObject;
import org.xml.sax.SAXException;

import com.gps.hubble.ui.utils.MashupUtils;
import com.gps.hubble.ui.utils.UIContext;

/**
 * Servlet class implementation for ExcelUploadServlet
 * @author Sambeet Mohanty
 * @version 1.0 ( 21 Jul 2016)
 */
@WebServlet("/api/exceluploadservlet")
public class ExcelUploadServlet extends HttpServlet  {


	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	private static final String jsonContentType = "application/json";
	private static final String strUploadProcess="process";
	private static final String strReceiverMailId="emailnotify";
	private static final String strProcessType="uploadType";
	private static final String mashupId = "fileUploadUtil";  


	/**
	 * Upon receiving file upload submission, parses the request to read upload
	 * data and saves the file in DB as in base64 String format.
	 * Method name: doPost
	 * @param request
	 * @return void
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String strProcess = null; 
		String strMailId = null;
		String strProcessTyp = null;
		String encodedString = null;

		UIContext context = UIContext.getUIContext(request, response);	
		boolean tracing = context.isTracing();

		if (tracing) {
			logger.info("upload servlet triggered");
		}

		try {
			if (!ServletFileUpload.isMultipartContent(request)) {
				return;
			}
			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iter = upload.getItemIterator(request);

			while (iter.hasNext()) {

				FileItemStream item = iter.next();
				String strFieldName = item.getFieldName();
				if (item.isFormField()) 
				{
					if(strFieldName.equalsIgnoreCase(strUploadProcess))
					{

						InputStream stream = item.openStream();
						strProcess = Streams.asString(stream);

						if (tracing) {
							logger.info("upload process type is: "+ strProcess);
						}
						stream.close();

					}
					else if(strFieldName.equalsIgnoreCase(strReceiverMailId))
					{
						InputStream stream = item.openStream();
						strMailId=Streams.asString(stream);

						if (tracing) {
							logger.info("mail id value is: "+ strMailId);
						}						
						stream.close();
					}
					else if(strFieldName.equalsIgnoreCase(strProcessType))
					{
						InputStream stream = item.openStream();
						strProcessTyp=Streams.asString(item.openStream());
			//			System.out.println(strFieldName); 
					//	System.out.println(Streams.asString(item.openStream()));
						if (tracing) {
							logger.info("Other upload process value is: "+ strProcessTyp);
						}						
						stream.close();
					}

				} else if (!item.isFormField() && !("".equals(item.getName()))) {

					InputStream is = item.openStream();
					byte[] encodedBytes = Base64.encodeBase64(IOUtils
							.toByteArray(is));
					encodedString = new String(encodedBytes);
					is.close();
				}
			}
			
			if(strProcessTyp!=null)
			{
				updateExcelInDB(encodedString, strProcessTyp,strMailId,context, response);
			}
			else
			{
				updateExcelInDB(encodedString, strProcess,strMailId,context, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("File Upload failed for "+strProcess+ "with error "+e.getMessage() );
		} 

	}

	private void updateExcelInDB(String encodedString, String strProcess, String strMailId, UIContext context, HttpServletResponse response )
					throws ParserConfigurationException, SAXException, IOException, TransformerException {

		JSONObject inputObj = new JSONObject();
		boolean tracing = context.isTracing();

		inputObj.put("NotifyEmail", strMailId);
		inputObj.put("FileObject", encodedString);
		inputObj.put("TransactionType",strProcess);

		if (tracing) {
			logger.info("Mashup input: {}", inputObj);
		} 

		JSONObject output = MashupUtils.invokeMashup(mashupId, inputObj, context);
		if (context.isRemoteServerUnreachable()) {
			if (tracing) {
				logger.info("Remote server unreachable");
			}

			response.setStatus(0);
		} else if (context.getRemoteServerErrorStatusCode() != -1) {
			if (tracing) {
				logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
			}

			response.setStatus(context.getRemoteServerErrorStatusCode());
		} else {

			String outputStr = "";

			output = new JSONObject();
			output.put("Result", "ApiSuccess");
			outputStr= output.toJSONString();

			if (tracing) {
				logger.info("Output of Mashup: {}", outputStr);
			}
			response.setContentType(jsonContentType);

			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(output);  

		}		
		
		logger.info("File Upload completed");	 

	}



}

