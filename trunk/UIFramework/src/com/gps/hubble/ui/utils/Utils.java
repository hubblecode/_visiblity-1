package com.gps.hubble.ui.utils;

public class Utils {

	public static final boolean getBooleanValue(String value) {
		if (value == null) {
			return false;
		}
		String lowerCaseValue = value.toLowerCase();
		if ( "y".equals(lowerCaseValue) || "yes".equals(lowerCaseValue) || "true".equals(lowerCaseValue) ) {
			return true;
		}
		return false;
	}

	public static final int getIntValue(String value, int defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return new Integer(value);
	}
}
