package com.gps.hubble.ui.utils;

import java.net.HttpURLConnection;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.Constants;

public class UIContext {

	private static final String UICONTEXT_ATTR_NAME = "uicontext";
	private final static String DEFAULT_PRNG = "SHA1PRNG"; //algorithm to generate key
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String loginid = null;
	private String token = null;
	private String userOrgCode=null;
	
	private boolean remoteServerUnreachable = false;
	private int remoteServerErrorStatusCode = -1;
	private boolean tracing = false;
	private String csrfToken;

	private static final Logger logger = LogManager.getLogger();
	
	public static UIContext getUIContext(HttpServletRequest request, HttpServletResponse response) {
		Object context = request.getAttribute(UICONTEXT_ATTR_NAME);
		if (context != null) {
			return (UIContext) context;
		}
		UIContext newContext = new UIContext(request, response);
		request.setAttribute(UICONTEXT_ATTR_NAME, newContext);
		return newContext;
	}

	private static String generateCSRFToken() {
		SecureRandom sr;
		try {
			sr = SecureRandom.getInstance(DEFAULT_PRNG);
			return "" + sr.nextLong();
		} catch (NoSuchAlgorithmException e) {
			logger.catching(e);
		}
		return "" + System.currentTimeMillis();
	}
	
	private UIContext(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.setupContext();
	}

	private void setupContext() {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return;
		}
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			String cookieName = cookie.getName();
			if (Constants.TOKEN_COOKIE_NAME.equals(cookieName)) {
				this.token = cookie.getValue();
			} else if (Constants.LOGIN_COOKIE_NAME.equals(cookieName)) {
				this.loginid = cookie.getValue();
			} else if (Constants.TRACE_COOKIE_NAME.equals(cookieName)) {
				this.tracing = Utils.getBooleanValue(cookie.getValue());
			} else if (Constants.CSRF_COOKIE_NAME.equals(cookieName)) {
				this.csrfToken = cookie.getValue();
			}
		}
	}

	public String getToken() {
		return token;
	}

	public String getLoginid() {
		return loginid;
	}

	public String getCsrfToken() {
		return csrfToken;
	}

	public void setToken(String token) {
		this.token = token;
		Cookie cookie = new Cookie(Constants.TOKEN_COOKIE_NAME, token);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
		Cookie cookie = new Cookie(Constants.LOGIN_COOKIE_NAME, loginid);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public void setUserOrgCode(String userOrgCode) {
		this.userOrgCode = userOrgCode;
		Cookie cookie = new Cookie(Constants.ORG_NAME,userOrgCode );
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		response.addCookie(cookie);
	}
	
	
	
	
	public void createCSRFToken() {
		csrfToken = generateCSRFToken();
		Cookie cookie = new Cookie(Constants.CSRF_COOKIE_NAME, csrfToken);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public boolean isValidRequest() {
		if (getLoginid() == null || getToken() == null || getCsrfToken() == null) {
			return false;
		}

		String reqCSRFToken = request.getHeader(Constants.CSRF_COOKIE_NAME);
		if (reqCSRFToken == null) {
			reqCSRFToken = request.getParameter(Constants.CSRF_COOKIE_NAME);
		}
		
		if (reqCSRFToken == null) {
			logger.error("CSRF Token is invalid for loginid {}", getLoginid());
			return false;
		}
		
		if (!reqCSRFToken.equals(csrfToken)) {
			logger.error("CSRF Token is invalid for loginid {}", getLoginid());
			return false;
		}

		return true;
	}

	public boolean isRemoteServerUnreachable() {
		return remoteServerUnreachable;
	}

	public void setRemoteServerUnreachable(boolean remoteServerUnreachable) {
		this.remoteServerUnreachable = remoteServerUnreachable;
	}
	
	public int getRemoteServerErrorStatusCode() {
		return remoteServerErrorStatusCode;
	}

	public void setRemoteServerErrorStatusCode(int errorStatusCode) {
		this.remoteServerErrorStatusCode = errorStatusCode;

		if (errorStatusCode != HttpURLConnection.HTTP_BAD_REQUEST) {
			return;
		}
		
		// TODO handle invalid 400 errors from remote server
		
//		Cookie tokenCookie = new Cookie(TOKEN_COOKIE_NAME, "");
//		tokenCookie.setMaxAge(0);
//		tokenCookie.setValue("");
//		tokenCookie.setPath("/");
//		response.addCookie(tokenCookie);
//		
//		Cookie loginCookie = new Cookie(LOGIN_COOKIE_NAME, "");
//		loginCookie.setMaxAge(0);
//		loginCookie.setValue("");
//		loginCookie.setPath("/");
//		response.addCookie(loginCookie);
	}

	public boolean isTracing() {
		return tracing;
	}

	public void setTracing(boolean trace) {
		if (tracing == trace) {
			return;
		}
		tracing = trace;
		Cookie cookie = null;
		if (tracing) {
			cookie = new Cookie(Constants.TRACE_COOKIE_NAME, "y");
			// Enable tracing for 10 minutes
			cookie.setMaxAge(600);
		} else {
			cookie = new Cookie(Constants.TRACE_COOKIE_NAME, "n");
			cookie.setMaxAge(0);
		}
		response.addCookie(cookie);
	}

	public String getRedirectUrl(String url) {
		String urlWithCSRFToken = null; 
		if (url.contains("?")) {
			urlWithCSRFToken = url + "&" + Constants.CSRF_COOKIE_NAME + "=" + getCsrfToken();
		} else {
			urlWithCSRFToken = url + "?" + Constants.CSRF_COOKIE_NAME + "=" + getCsrfToken();
		}
		return urlWithCSRFToken;
	}

	public String getUserOrgCode() {
		return userOrgCode;
	}

	

}
