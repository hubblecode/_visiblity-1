package com.gps.hubble.ui.utils;

import java.net.HttpURLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.mashup.IMashup;
import com.gps.hubble.ui.mashup.MashupDef;
import com.gps.hubble.ui.mashup.MashupManager;

public class MashupUtils {
	
	public static JSONObject invokeMashup(String mashupId, JSONObject input, UIContext context) {
		IMashup mashup = getMashup(mashupId, context);
		if (mashup == null) {
			context.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
			return null;
		}
		JSONObject output = mashup.execute(input);
		return output;
	}

	private static IMashup getMashup(String mashupId, UIContext context) {
		MashupDef mashupDef = getMashupDef(mashupId, context);
		if (mashupDef == null) {
			return null;
		}
		IMashup mashup = getMashupImpl(mashupDef, context);
		mashup.setMashupDef(mashupDef);
		mashup.setUIContext(context);
		return mashup;
	}

	private static IMashup getMashupImpl(MashupDef mashupDef, UIContext context) {
		return mashupDef.getMashup();
	}

	private static MashupDef getMashupDef(String mashupId, UIContext context) {
		return MashupManager.getMashupDef(mashupId);
	}
	
	public static JSONObject getJSONObject(String json) {
		if (json == null) {
			return new JSONObject();
		}
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonOutput = (JSONObject) parser.parse(json);
			return jsonOutput;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getMashupId(UIContext context) {
		String path = context.getRequest().getPathInfo();
		String mashupId = path.replaceFirst("/", "");
		return mashupId;
	}

	public static String getMashupInput(UIContext context, String mashupId) {
		return context.getRequest().getParameter(mashupId);
	}
}
